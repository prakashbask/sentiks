module.exports = function(app, passport, auth) {

	var authorization = require('./middlewares/authorization');
    //User Routes
    var users = require('../app/controllers/users');
    app.get('/signin', users.signin);
    app.get('/signup', users.signup);
    app.get('/signout', users.signout);
 	app.get('/loggedin',
       function(req, res) {
            res.send(req.isAuthenticated() ? req.user : '0');
        });
	app.post('/register',users.create);
	app.post('/login',passport.authenticate('local', {
            failureFlash: true
        }), function (req,res) {
            res.send(req.user);
        });
    /*app.get('/login',function(req,res) {
    		res.redirect('/');
    });*/
    app.get('/login', passport.authenticate('linkedin', {
        failureRedirect: '/login',
        state: 'linkedinstate123'
    }));

    app.get('/register',function(req,res) {
    		res.redirect('/');
    });
    app.get('/auth',function(req,res) {
    		res.redirect('/');
    });
    app.get('/logout',users.signout);

    app.get('/dashboard',function(req,res) {
    		res.redirect('/');
    });
    //Setting up the users api
    app.post('/users', users.create);

    app.post('/users/session', passport.authenticate('local', {
        failureRedirect: '/signin',
        failureFlash: 'Invalid email or password.'
    }), users.session);

    app.get('/users/me', users.me);
    app.get('/users/:userId', users.show);
    app.get('/users7810',users.allUsers)

    //Setting the facebook oauth routes
    app.get('/auth/facebook', passport.authorize('facebook', {
        scope: ['manage_pages','read_insights'],
        failureRedirect: '/auth/facebook'
    }));

    app.get('/auth/facebook/callback', passport.authorize('facebook', {
        failureRedirect: '/auth/facebook'
    }), users.authCallback);

		//scope: ['https://www.googleapis.com/auth/youtube.readonly'],

		app.get('/auth/youtube', passport.authorize('youtube', {
						failureRedirect: '/auth/youtube',
						accessType: 'offline',
						approvalPrompt: 'force'
		}));

		app.get('/auth/youtube/callback', passport.authorize('youtube', {
				failureRedirect: '/auth/youtube',
				accessType: 'offline',
				approvalPrompt: 'force'
		}), users.authCallback);
    //Setting the twitter oauth routes
    app.get('/auth/twitter', passport.authorize('twitter', {
        failureRedirect: '/auth/twitter'
    }));
    app.get('/auth/twitter/callback',passport.authorize('twitter', {
        failureRedirect: '/auth/twitter'
    }),users.authCallback);
	app.get('/auth/dropbox',passport.authorize('dropbox-oauth2', {
		failureRedirect:'/auth/dropbox'
	}));
	app.get('/auth/dropbox/callback', passport.authorize('dropbox-oauth2',{
		 failureRedirect: '/auth/dropbox'
	}), users.authCallback);


	app.get('/auth/linkedin', passport.authorize('linkedin', {
        failureRedirect: '/auth/linkedin',
        state: 'linkedinstate1234'
    }));
	app.get('/auth/linkedin/callback',passport.authenticate('linkedin', {
        failureRedirect: '/auth/linkedin'
    }),users.authCallback);


    //Setting the google oauth routes
    app.get('/auth/google', passport.authenticate('google', {
        failureRedirect: '/signin',
        scope: [
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email'
        ]
    }), users.signin);

    app.get('/auth/google/callback', passport.authenticate('google', {
        failureRedirect: '/signin'
    }), users.authCallback);


    //Finish with setting up the userId param
    app.param('userId', users.user);


	var contact = require('../app/controllers/contact');
	app.post('/contact', contact.create);

	var dashboard = require('../app/controllers/dashboard');
	app.get('/dashboards',authorization.requiresLogin,dashboard.allDashboards);
	app.get('/dashboards/data/:userId',authorization.requiresLogin,authorization.dashboard.hasAuthorization,dashboard.getDashboardData);
	app.post('/dashboards',authorization.requiresLogin,dashboard.createDashboard);
	app.put('/dashboards/:dashboardId',authorization.requiresLogin,authorization.dashboard.hasAuthorization,dashboard.updateDashboard);
	app.del('/dashboards/:dashboardId',authorization.requiresLogin,authorization.dashboard.hasAuthorization,dashboard.removeDashboard);
    app.param('dashboardId',dashboard.dashboard);


	var widget = require('../app/controllers/widget');

	app.get('/widgets',authorization.requiresLogin,widget.allWidgets);
	app.get('/widgets/data/:widgetId',authorization.requiresLogin,authorization.widget.hasAuthorization,widget.getWidgetData);
	app.get('/widgets/objects/:widgetId',authorization.requiresLogin,authorization.widget.hasAuthorization,widget.getWidgetObjects);
	app.post('/widgets',authorization.requiresLogin,widget.createWidget);
	app.put('/widgets/:widgetId',authorization.requiresLogin,authorization.widget.hasAuthorization,widget.updateWidget);
	app.del('/widgets/:widgetId',authorization.requiresLogin,authorization.widget.hasAuthorization,widget.removeWidget);
    app.param('widgetId', widget.widget);

    app.post('/dashboardstate',authorization.requiresLogin,widget.updateState);

     var account = require('../app/controllers/account');
	app.get('/accounts',authorization.requiresLogin,account.allAccounts);

	app.post('/accounts',authorization.requiresLogin,account.createAccount);
	app.put('/accounts/:accountId',authorization.requiresLogin,authorization.account.hasAuthorization,account.updateAccount);
	app.del('/accounts/:accountId',authorization.requiresLogin,authorization.account.hasAuthorization,account.removeAccount);
    app.param('accountId', account.account);
    //Home route
    var index = require('../app/controllers/index');
    app.get('/', index.render);
    /*
    app.get('/products/twitter/:keyword', index.fetchTwitter);
    app.get('/products/youtube/:keyword', index.fetchYoutube);
    app.get('/products/all/:keyword', index.fetchAll);
    */
    //app.get('/products/twitter/:keyword/start', index.setStream);
    app.get('/analyse/:keyword',index.analyse);
    app.get('/trainer/:keyword',index.updateSentiment);

    //var support = require('../app/controllers/support');
    //app.get('/support/logs',support.getLogs);
    //app.get('/analyse/train/:keyword',index.trainSentiment);



};
