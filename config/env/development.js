module.exports = {
    db: "mongodb://localhost/mean-dev",
    app: {
        name: "sentiks"
    },
    facebook: {
        clientID: "APP_ID",
        clientSecret: "APP_SECRET",
        callbackURL: "http://localhost:3000/auth/facebook/callback"
    },
    twitter: {
        clientID: "CONSUMER_KEY",
        clientSecret: "CONSUMER_SECRET",
        callbackURL: "http://localhost:3000/auth/twitter/callback"
    },
    github: {
        clientID: "APP_ID",
        clientSecret: "APP_SECRET",
        callbackURL: "http://localhost:3000/auth/github/callback"
    },
    google: {
        clientID: "APP_ID",
        clientSecret: "APP_SECRET",
        callbackURL: "http://localhost:3000/auth/google/callback"
    },
    access: {
		number: {
			numberOfTimesDay:"3",
			duration:"60",
			reason:"You have exceeded the quota for the day. Please get in touch with support if you would like to get more access",
			errstatus:"401"
		}
    }
}
