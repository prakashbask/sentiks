/**
 * Generic require login routing middleware
 */
exports.requiresLogin = function(req, res, next) {
    if (!req.isAuthenticated()) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

/**
 * User authorizations routing middleware
 */
exports.user = {
    hasAuthorization: function(req, res, next) {
        if (req.profile.id != req.user.id) {
            return res.send(401, 'User is not authorized');
        }
        next();
    }
};

/**
 
 */
exports.widget = {
    hasAuthorization: function(req, res, next) {
    //	console.log(req.widget,req.user);
        if (req.widget.user != req.user._id) {
            return res.send(401, 'User is not authorized');
        }
        next();
    }
};

exports.account = {
    hasAuthorization: function(req, res, next) {
    //	console.log(req.session,req.passport);
        if (req.account.user != req.user._id) {
            return res.send(401, 'User is not authorized');
        }
        next();
    }
};

exports.dashboard = {
    hasAuthorization: function(req, res, next) {
    //	console.log(req.session,req.dashboard,req.user);
        if (req.dashboard.user != req.user._id) {
            return res.send(401, 'User is not authorized');
        }
        next();
    }
};

