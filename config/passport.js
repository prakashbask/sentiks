var mongoose = require('mongoose'),
    LocalStrategy = require('passport-local').Strategy,
    TwitterStrategy = require('passport-twitter').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    GitHubStrategy = require('passport-github').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    DropboxStrategy = require('passport-dropbox-oauth2').Strategy,
    LinkedInStrategy = require('passport-linkedin-oauth2').Strategy,
    YoutubeStrategy = require('passport-youtube-v3').Strategy,
    User = mongoose.model('User'),
    Account = mongoose.model('Account'),
    config = require('./config');

var storageProvider = {};
storageProvider['dropbox']='storage';


module.exports = function(passport) {
    //Serialize sessions
    passport.serializeUser(function(user, done) {
    	//console.log("In Serialize",user)
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
    	//console.log("In deserialize",id)
        User.findOne({
            _id: id
        }, '-salt -hashed_password', function(err, user) {
            done(err, user);
        });
    });

    //Use local strategy
    passport.use(new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password'
        },
        function(email, password, done) {
            User.findOne({
                email: email
            }, function(err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, {
                        message: 'Unknown user'
                    });
                }
                if (!user.authenticate(password)) {
                    return done(null, false, {
                        message: 'Invalid password'
                    });
                }
                return done(null, user);
            });
        }
    ));

    var logincallback = function(req,accessToken,refreshToken, profile, done) {
    	  	process.nextTick(function() {
    	  		//console.log("Profile",profile);
    	  		User.findOne({email: profile.emails[0].value},function(err,user) {
    	  			if(err) {return done(null,false,{message: 'Error: Unable to lookup User'});}
    	  			if(user) {
    	  				 Account.findOne({ domain: profile.provider, did: profile.id,user:user._id }, function(err, account) {
				      		if (err) { return done(null,false,{message:'Error: Unable to fetch account'}); }
				      		if (account) {
				      	  			console.log("Token",accessToken,refreshToken);
				      				account.accessToken = accessToken;
				      		      	account.refreshToken = refreshToken;
				      		      	account.save();
				      		      	req.account=account;
    	  							return done(null,user);
				      		} else {

	    	  					  var account = new Account();
							      account.user = user._id;
							      account.domain = profile.provider;
							      account.did = profile.id;
							      account.name = profile.displayName;
							      //account.profile = profile;
							      account.category = getCategory(profile.provider);
							      account.accessToken = accessToken;
							      account.refreshToken = refreshToken;
							      account.save(function(err) {
							      	req.account=account;
	    	  						return done(null,user);
							      });
						     }
						  });

    	  			} else {

    	  				user = new User();
    	  				user.name = profile.displayName;
    	  				user.email = profile.emails[0].value;
    	  				user.provider = profile.provider;
    	  				user.save(function(err,user) {


    	  					  if(err) { return done(null,false,{message:'Error: Unable to create user'})}
    	  					  var account = new Account();
						      account.user = user._id;
						      account.domain = profile.provider;
						      account.did = profile.id;
						      account.name = profile.displayName;
						      //account.profile = profile;
						      account.category = getCategory(profile.provider);
						      account.accessToken = accessToken;
						      account.refreshToken = refreshToken;

						      account.save(function(err,account) {
		                        console.log("In Account Save",err);
		                        req.account = account;
		                        return done(null,user);
		                  	  });

    	  				});
    	  			}

    	  		});
    	  	});

    };
	var oauth1callback =  function(req,token, tokenSecret, profile, done) {
        	process.nextTick(function() {
        		 //console.log("Profile",profile);
	        	//if (req.user) {

	        		console.log("User",req.user);
	        		if(!profile) return done(null,false,{message:'Error: Unable to fetch profile'})
	        		 Account.findOne({ domain: profile.provider, did: profile.id,user:req.user._id }, function(err, account) {

				      if (err) { return done(null,false,{message:'Error: Unable to create account'}); }
				      if (account) {
				      	account.accessToken = token;
				      	account.accessTokenSecret = tokenSecret;
				      	account.save();
				      	req.user.account=account;
				      	return done(null, req.user);
				      }

				      var account = new Account();
				      account.user = req.user._id;
				      account.domain = profile.provider;
				      account.did = profile.id;
				      account.name = profile.username;
				      //account.profile = profile;
				      account.category = getCategory(profile.provider);
				      account.accessToken = token;
				      account.accessTokenSecret = tokenSecret;

				      account.save(function(err) {
                        if (err) return done(null,false,{message:'Error: Unable to save account information'});

                        // if successful, return the new user
                        req.user.account=account;
                        return done(null, req.user);
                  	  });

				    });

	       	 	//}
        	});
        }




	var oauth2callback = function(req,accessToken,refreshToken,profile,done) {
		process.nextTick(function() {
      	  		 console.log("Profile",profile,done);

	        		 if(!profile) return done(null,false,{message:'Error: Unable to fetch profile'});
	        		 Account.findOne({ domain: profile.provider, did: profile.id,user:req.user._id }, function(err, account) {
				      if (err) { return done(null,false,{message:'Error: Unable to create account'}); }
				      if (account) {
				      	  console.log("Token",accessToken,refreshToken);
				      	account.accessToken = accessToken;

				      	account.refreshToken = refreshToken;
				      	account.save();
				      	req.user.account = account;
				      	return done(null, req.user);
				      }

				      var account = new Account();

				      account.domain = profile.provider;
				      account.user = req.user._id;
				      account.did = profile.id;
				      var emailId = profile.emails ? profile.emails[0].value : null
				      account.name = profile.username || emailId || profile.displayName || "Unavailable";
				      //account.profile = profile;
				      console.log("Token",accessToken,refreshToken);
				      account.category = getCategory(profile.provider);
				      account.accessToken = accessToken;
				      account.refreshToken=refreshToken;
				      account.save(function(err) {
                        if (err)return done(null,false,{message:'Error: Unable to save account information'});

                        // if successful, return the new user
                        req.user.account=account;
                        return done(null, req.user);
                  	  });

				    });

	       	 	//}
        	});
	}

	var getCategory = function(provider) {

		return storageProvider[provider];
	}
	//Use twitter strategy
    passport.use(new TwitterStrategy({
            consumerKey: config.twitter.clientID,
            consumerSecret: config.twitter.clientSecret,
            callbackURL: config.twitter.callbackURL,
            passReqToCallback:true
        },oauth1callback));

    //Use facebook strategy
    passport.use(new FacebookStrategy({
            clientID: config.facebook.clientID,
            clientSecret: config.facebook.clientSecret,
            callbackURL: config.facebook.callbackURL,
            passReqToCallback:true

        },oauth2callback));

   passport.use(new LinkedInStrategy({
            clientID: config.linkedin.clientID,
            clientSecret: config.linkedin.clientSecret,
            callbackURL: config.linkedin.callbackURL,
            scope: ['r_emailaddress', 'r_basicprofile','r_fullprofile'],
            passReqToCallback:true

        },logincallback));

    //Use github strategy
    passport.use(new GitHubStrategy({
            clientID: config.github.clientID,
            clientSecret: config.github.clientSecret,
            callbackURL: config.github.callbackURL
        },
        function(accessToken, refreshToken, profile, done) {
            User.findOne({
                'github.id': profile.id
            }, function(err, user) {
                if (!user) {
                    user = new User({
                        name: profile.displayName,
                        email: profile.emails[0].value,
                        username: profile.username,
                        provider: 'github',
                        github: profile._json
                    });
                    user.save(function(err) {
                        if (err) console.log(err);
                        return done(err, user);
                    });
                } else {
                    return done(err, user);
                }
            });
        }
    ));
	passport.use(new DropboxStrategy({
	    clientID: config.dropbox.clientID,
	    clientSecret: config.dropbox.clientSecret,
	    callbackURL: config.dropbox.callbackURL,
	    passReqToCallback:true

	  },oauth2callback));


    //Use Youtube strategy
    passport.use(new GoogleStrategy({
          clientID: config.youtube.clientID,
          clientSecret: config.youtube.clientSecret,
          callbackURL: config.youtube.callbackURL,
          scope:['profile','email','https://www.googleapis.com/auth/youtube.readonly',
                'https://www.googleapis.com/auth/yt-analytics.readonly'],
          passReqToCallback:true
        },oauth2callback,'youtube'));

};
