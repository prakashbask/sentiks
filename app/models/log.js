var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var AccessSchema = new Schema({
    ipAddress: { type: String, required: true},
    keyword : { type: String, required: true},
    timeTaken: {type:Number},
    createdAt: { type: Date,default: Date.now }
   
});


// create and export our model
module.exports = mongoose.model('AccessLog', AccessSchema);
