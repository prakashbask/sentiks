var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var WidgetDefaultSchema = new Schema({
    name: {type:String,unique:true},
    title: {type:String},
    dataAttrName:{type:String},
    dataModelType:{type:String},
    dataModelOptions: {
 	params: {type:String}
    },
    editModalOptions: {
		templateUrl: {type:String}
    },
    
    type: {type:String},
    config: {
		accounts: {
			list:[], 
			selected: {type: String}
		},
		metrics: {
			list:[], 
			selected: {type: String}
		},
		chartType: {
			list:[],
			selected: {type: String}
		},
		custom: {
		}
    },
    dimensions: {
	sizeX: {type: Number},
	sizeY: {type: Number},
        row: {type: Number},
        col: {type:Number}

    }

   
});


// create and export our model
module.exports = mongoose.model('WidgetDefault', WidgetDefaultSchema);

