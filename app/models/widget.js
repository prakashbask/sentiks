var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var WidgetSchema = new Schema({
    user: { type: String},
    dashboardId: {type: String},
    id: {type: Number},
    title: {type:String},
    type: {type:String},
    config: {
		status: {type: Boolean},
		accounts: {
			selected: {type: String}
		},
		metrics: {
			list:[],
			selected: {type: String}
		},
		chartType: {
			list:[],
			selected: {type: String}
		},
		storage: {
			list:[],
			selected: {type:String}
		},
		objects: {
			list:[],
			selected: {type:String}
		},
		interval: {type: Number, default: 12},
		custom: {},
		storeoptions: {}
    },
    dimensions: {
        sizeX: {type: Number},
        sizeY: {type: Number},
        row: {type: Number},
        col: {type:Number}

    }


});
WidgetSchema.statics = {
    load: function(id, cb) {
        this.findOne({
            _id: id
        }).exec(cb);
    }
};

// create and export our model
module.exports = mongoose.model('Widget', WidgetSchema);
