var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DeniedLogSchema = new Schema({
    ipAddress: { type: String, required: true},
    keyword : { type: String, required: true},
    reason : { type:String, required: true},
    createdAt: { type: Date,default: Date.now }
   
});


// create and export our model
module.exports = mongoose.model('DeniedLog', DeniedLogSchema);
