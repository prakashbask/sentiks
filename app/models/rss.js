var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var RSSSchema = new Schema({
    title: { type: String, required: true, trim: true, index: {unique: true, dropDups: true} },
    desc: { type: String },
    link: { type: String, required: true },
    category: {type: String, required: true },
    source: {type: String, required: true },
    timeago: { type: String, required: true},
    timeseconds: { type: Number, required: true},
    date: { type: String, required: true},
    id:{type: Number,required: true},
    sentiment: {type:String, required:true},
    createdAt: { type: Date,expires: 172800, default: Date.now } //expires after 2 days
    
});



// create and export our model
module.exports = mongoose.model('RSSFeed', RSSSchema);
