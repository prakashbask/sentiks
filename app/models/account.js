/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


/**
 * User Schema
 */
var AccountSchema = new Schema({

	user:String,
	domain:String,
	did:String,
	name:String,
	accessToken:String,
	accessTokenSecret:String,
	refreshToken:String,
	category:{type:String, default:'NA'}
		
    
});

AccountSchema.statics = {
    load: function(id, cb) {
        this.findOne({
            _id: id
        }).populate('user', 'name username').exec(cb);
    }
};

mongoose.model('Account', AccountSchema);
