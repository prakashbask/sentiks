var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var WidgetDataSchema = new Schema({
    user: { type: String},
    dashboardId: {type: String},
    widgetId: {type: String},
    data: {},
    createdAt:{type:Number, default: Date.now}

   
});



// create and export our model
module.exports = mongoose.model('WidgetData', WidgetDataSchema);
