var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DashboardSchema = new Schema({
    user: { type: String},
    title: { type:String},
    createdAt: {type:Date, default: Date.now}
   
});

DashboardSchema.statics = {
    load: function(id, cb) {
        this.findOne({
            _id: id
        }).exec(cb);
    }
};

// create and export our model
module.exports = mongoose.model('Dashboard', DashboardSchema);
