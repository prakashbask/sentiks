var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ContactSchema = new Schema({
    name: { type: String},
    email : { type: String},
    phone: {type:String},
    message: { type: String }
   
});


// create and export our model
module.exports = mongoose.model('Contact', ContactSchema);
