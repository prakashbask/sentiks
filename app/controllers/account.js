/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Account = mongoose.model('Account');
     _ = require('underscore');
var TAG="Account:";
exports.account = function(req, res, next, id) {
    Account.load(id, function(err, account) {
        if (err) return next(err);
        if (!account) return next(new Error('Failed to load Account ' + id));
        req.account = account;
        next();
    });
};
exports.createAccount = function(req, res) {
    var account = new Account(req.body);

    account.save(function(err) {
        if (err) {
		         res.send('404',err);
        }else {
		        res.jsonp(account);
	}
    });
}
exports.updateAccount = function(req, res) {

    var account = req.account;
    console.log(req.account);
    if(!account.user && (account.user !== req.body.user))res.send(401, 'Account is already added for another User')

    account = _.extend(account, req.body);

    account.save(function(err) {
    	if(err) {
    	   res.send('404',err);
    	}else {
        	res.jsonp(account);
       }
    });

}

exports.removeAccount = function(req, res) {
    var account= req.account;

    account.remove(function(err) {
        if (err) {
            res.send('404',err);
        } else {
            res.jsonp(account);
        }
    });
};


exports.allAccounts = function(req, res) {
	console.log(TAG,req.user);
    Account.find({user:req.user._id}).sort('-created').exec(function(err, accounts) {
        if (err) {
            res.send('404',err);
        } else {
            res.jsonp(accounts);
        }
    });
};

exports.findAccount = function(req, res, next, id) {
    Account.load(id, function(err, account) {
        if (err) return next(err);
        if (!account) return next(new Error('Failed to load Account ' + id));
        req.account = account;
        next();
    });
};
