/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),

    Widget = mongoose.model('Widget'),
    WidgetData = mongoose.model('WidgetData'),
    WidgetCollector = require('./collectors/widget/WidgetCollector'),
    Account = mongoose.model('Account'),
    CloudDatastore = require('./lib/CloudDatastore.js'),
     _ = require('underscore');

var TAG="Widget:"

var widgetCollector = new WidgetCollector();

exports.widget = function(req, res, next, id) {
	console.log(TAG,'Load');
    Widget.load(id, function(err, widget) {
        if (err) return next(err);
        if (!widget) return next(new Error('Failed to load Widget ' + id));
        req.widget = widget;
        var tempDomain = (widget.type.split('-'))[0];

        Account.findOne({domain:tempDomain,name:widget.config.accounts.selected,user:widget.user},function(err,accountdata) {
        	if(err) return next(err);
        	if (accountdata) req.account=accountdata;
        	next();
        });

    });
};
exports.createWidget = function(req, res) {
    var widget = new Widget(req.body);
	widget.user = req.user._id;
    widget.save(function(err) {
        if (err) {
		        res.send('404',err);
        }else {
		        res.jsonp(widget);
	      }
    });
}
exports.updateWidget = function(req, res) {

    var widget = req.widget;
    var doScheduleFirst = false;
    /*console.log(req.widget,req.body);
    if((!widget.config.status  && req.body.config.status) ||
    	 ((widget.config.storage.selected !== req.body.config.storage.selected) && req.body.config.storage.selected !== 'Passthru')) {
    	//it means that widget has been configured for the first time
    	//schedule the first collection immediately. Pass the widget id.
    	//client will poll every 10 secs till it gets the first data
    	doScheduleFirst = true;
    } */
    widget = _.extend(widget, req.body);

    widget.save(function(err) {
    	/*if(doScheduleFirst && widget.config.storage.selected !== 'Passthru') widgetCollector.scheduleJob(widget,req.account,true);*/
        console.log(TAG,'Save:',err);
        if(err){
          res.send('404',err);
        }else {
          res.jsonp(widget);
        }
    });

}
exports.updateState = function(req, res) {

    var state = req.body;
    Widget.find({user:req.user._id,dashboardId:state.seldashboardId}).exec(function(err,widgets){
    	if(err){

    	}else {

    		_.each(widgets,function(widget) {
    			var newdimension = (state.widgets && state.widgets[widget._id]) ? state.widgets[widget._id].dimensions : null;
    			if(newdimension) {

    				_.extend(widget.dimensions,newdimension);
    				widget.save();
    			}
    		});
    	}


    });
    res.jsonp("success");

};

exports.removeWidget = function(req, res) {
    var widget= req.widget;

    widget.remove(function(err) {
        if (err) {
            res.send('404',err);
        } else {
        	WidgetData.remove({user:widget.user,dashboardId:widget.dashboardId,widgetId:widget._id}).exec();
            res.jsonp(widget);
        }
    });
    //TODO..remove widgetData also
};


exports.allWidgets = function(req, res) {
	var userId = req.user._id;
	var dashboardId = req.query.dashboardId;

    Widget.find({user:userId, dashboardId: dashboardId}).sort('-created').exec(function(err, widgets) {
        if (err) {
            res.send('404',err);
        } else {
            res.jsonp(widgets);
        }
    });
};

exports.findWidget = function(req, res, next, id) {
    Widget.load(id, function(err, widget) {
        if (err) return next(err);
        if (!widget) return next(new Error('Failed to load Widget ' + id));
        req.widget = widget;
        next();
    });
};

exports.getWidgetData = function(req, res) {


	try {
		var widgetId = req.param('widgetId');
		var user = req.user._id;
		var startTime = req.query.st;
		var endTime = req.query.et;

		if(!req.account) {
			console.log(TAG,'GetWidgetData:','Account is not defined');
			res.send('404','Widget Account is not selected');
			return;
	 	};
		console.log(TAG,'GetWidgetData:',user,widgetId,startTime,endTime);
		//console.log("Account",req.account);
		//TODO: Refactor the code here
		if(req.widget.config.storage.selected === 'Passthru') {
		 	//runtime retreival
		 	widgetCollector.fetchRealTime(req.widget,req.account,res);
		}else if (req.widget.config.storage.selected === 'Remote'){
		    WidgetData.find({user:user,widgetId: widgetId}).sort({'createdAt':1}).exec(function(err, widgetdata) {
		        if (err) {
              res.send('404',err);
		        } else {

		        	if(widgetdata.length == 0) {
		        		//schedule it once.
		        		widgetCollector.scheduleJob(req.widget,req.account,true);
		        	}
		            res.jsonp(widgetdata);
		        }
		    });
	    } else {
	    	//res.jsonp([]);

	    	Account.findOne({domain:'dropbox',name:req.widget.config.storage.selected,user:req.user._id}).exec(function(err, accountdata) {
		        if (err) {
              res.send('404',err);
		        } else {
		       			var clouddatastore = new CloudDatastore({accessToken:accountdata.accessToken});
		       			clouddatastore.get(req.widget,function(err,widgetdata) {
		       				if (err) {
                     res.send('404',err);
		        			} else {

		        			//clouddatastore.close();
		        			if(widgetdata.length == 0) {
		        				//schedule it once.
		        				widgetCollector.scheduleJob(req.widget,req.account,true);
		        			}
		            		res.jsonp(widgetdata);
		        			}

		       			});
		        }
	    	});

	    }
	 } catch (err) {
	 	console.log(TAG,'GetWidgetData:',err);
		res.send('404','Error in fetching Widget Data');
	 }
};
exports.getWidgetObjects = function(req, res) {


  try {
  	var widgetId = req.param('widgetId');
  	//var user = req.user._id;
  /*	var account = {accountName: req.query.accountName,
  				   accountDomain: req.query.accountDomain};
    */

    var account = _.clone(req.query);

  	widgetCollector.fetchObjects(req.widget,account,res);

  }catch(err) {
    res.send('404',err);
  }

};
