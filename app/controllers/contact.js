/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Contact = mongoose.model('Contact');

exports.create = function(req, res) {
    var contact = new Contact(req.body);

    contact.save(function(err) {
        if (err) {
		res.jsonp(err);
        }else {
		res.jsonp("success");
	}
    });
}
