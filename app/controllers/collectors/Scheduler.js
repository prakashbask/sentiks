
var schedule = require('node-schedule');
var feedParser = require('feedparser');
var request = require('request');
var RSSCollector = require('./RSSCollector');
var classifier = require('../classifier/classifier.js');

//var j = schedule.scheduleJob('59 */6 * * *', function(){
console.log("Setting Scheduler");
var rule = new schedule.RecurrenceRule();
rule.hour = new schedule.Range(0,24,6);
rule.minute = 59;
//rule.minute = new schedule.Range(0,59,1);

var j = schedule.scheduleJob(rule, function(){
    new RSSCollector(classifier.getClassifier(),feedParser,request).collect();
});




