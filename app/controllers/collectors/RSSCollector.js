
var mongoose = require('mongoose'),
RSSFeed = mongoose.model('RSSFeed');
//initialize handler for RSS
var feedParser = require('feedparser');
var requestH = require('request');  
var moment = require('moment');

var RSSCollector = function(classifier) {
  this.response = new Object();
  this.classifier = classifier;
  this.reader= feedParser;
  this.request = requestH;
   
  
  
};

RSSCollector.prototype.collectQ = function(keyword,callback) {
  var self = this;
  console.log("In Q function");
  self.collectDB(keyword,callback);
	  
	 
	  
}

RSSCollector.prototype.collect = function(keyword,res) {
  var self = this;
  console.log("Fetching RSS from source")
  self.fetchFromSource(keyword,res);
	  
	 
	  
}

RSSCollector.prototype.collectDB = function(keyword,callback) {
  var self = this;
  var reg = new RegExp(keyword, 'i');
  RSSFeed.find({ 'title': reg }, function(err,feeds) {
  	
	  if (err) {
	  	console.error("RSS DB Error",err);
	  	callback(null,null);
	  } else {
		  //console.log(feeds);
		  self.response.items=feeds;
		  self.response.count=feeds.length;
		  //res.jsonp(self.response);
		  self.response.topResults = new Array();
		  self.response.topResults[0] = new Object;
		  self.response.topResults[0].count=feeds.length;
		  self.response.topResults[0].source="rss";
		  self.response.topResults[0].negativecount=0;
		  self.response.topResults[0].positivecount=0;
		  self.response.topResults[0].neutralcount=0;
		  feeds.forEach(function(feed) {
		  	 if(feed.sentiment == 'positive') {
	 	 		self.response.topResults[0].positivecount +=1;
		 	 } else if (feed.sentiment == 'negative'){
		 	 	self.response.topResults[0].negativecount +=1;
		 	 } else self.response.topResults[0].neutralcount +=1;
		  	
		  })
		  
		  
		  callback(null,self.response);
	  }
  });
	 
	  
}

RSSCollector.prototype.collectTest = function(keyword,res) {
      var self = this;

 	  self.fetchTest(keyword,res);
	    
	  
	  //res.jsonp(self.response);
	  
}
RSSCollector.prototype.collectQTest = function(keyword,callback) {
      var self = this;

 	  //self.fetchTest(keyword,callback);
 	  self.collectDB(keyword,callback);
	    
	  
	  //res.jsonp(self.response);
	  
}

RSSCollector.prototype.parseResponse = function(responseFromSource) { 
	  var self=this;
	  var response=self.response;
	  
	    
	  	  
		  response.count = responseFromSource.items.length;
		  response.items = new Array();
		  
		  var index = 0;
		  var items = responseFromSource.items;
		  for (var key in items) {
		  	
		  	 var item = items[key];
		  	 
		  	 var timeinfo = moment(item.pubdate);
		  	 var timeago = timeinfo.fromNow();
	  		 var timeseconds = timeinfo.unix(); 
		  	 response.items[index] = new Object();
		  	 response.items[index].title=item.title;
		  	 response.items[index].link=item.link; 
		  	 response.items[index].desc=item.description;
		  	 //response.items[index].source =responseFromSource.meta.generator; 
		  	 response.items[index].source =responseFromSource.source.split("//")[1].split("/")[0];
		  	 response.items[index].category='RSS';
		  	 response.items[index].timeago = timeago;
	  		 response.items[index].timeseconds = timeseconds;
		  	 response.items[index].date = timeinfo.date()+'/'+(timeinfo.month()+1)+'/'+timeinfo.year();
		  	 response.items[index].id=Math.random();
		  	 
		  	 response.items[index].influence_count=0;
		  	 
		  	 var sentObj = self.classifier.classify(item.title);
		 	//TO DO ..store this in schema
            //response.items[index].emotions=sentObj.emotionList;
            //response.items[index].keywords=sentObj.keywordList;

		  	 
		 	response.items[index].sentiment=sentObj.sentiment;
	     	response.items[index].sentimentText=sentObj.sentiment;
	  	
		  	index++;
		  }
		
	    
	    
	
	    
};

var persistResponse = function(responseFromSource) {
		//TODO should save in bulk
		responseFromSource.items.forEach(function (item) {
			var feed = new RSSFeed(item);
		    feed.save(function (err) {
		    	if(err) {
		    		console.log("Error in saving RSS Feed" + err);
		    	}
		        
		    });
		});

  	   
};

RSSCollector.prototype.fetchTest = function(keyword,callback) {
	var self=this;
	var items=[];
  	var feedMeta;
  	
  	console.log("Reading Test Stream");
	fs.createReadStream('top.xml')
	  .pipe(new self.reader())
	  .on('error', function (error) {
	    console.log("Error is" + error);
	    callback(error);
	  })
	  .on('meta', function (meta) {
	    feedMeta = meta;
	  })
	  .on('readable', function() {
	    var stream = this, item;
	    
	 	
	    while (item = stream.read()) {
	    		      
	      items.push(item);
	    }
	    
	   
	  })
	  .on('end', function () {
	  	console.log("Test In End Event RSS");
	  	 var result = {
       				'meta': feedMeta,
                    'items': items,
                    'source':'http://localhost/top.xml'
          };
	  	 self.parseResponse(result);
	  	 //res.jsonp(self.response);
	  	 callback(null,self.response);
	  });
	
	
};
RSSCollector.prototype.fetchFromSource = function(keyword,res) {
  var self=this;
  var items=[];
  var feedMeta;
    
  var sources = fs.readFileSync('./app/controllers/collectors/rsssources.cfg').toString().split("\n");
  console.log(sources);
  /*
  sources.forEach(function(source) {
  	console.log(source.split("//")[1].split("/")[0]);
  });*/
  
  sources.forEach(function(source){
  	if(source == '') return; 
  	console.log(source);
  	self.request(source)
	  .pipe(new self.reader({addMeta:false}))
	  .on('error', function(error) {
	    	console.log("Error in Fetching From RSS");
	  })
	  .on('meta', function (meta) {
	    feedMeta = meta;
	  })
	  .on('readable', function () {
	    
	    var stream = this, item;
	 
	    while (item = stream.read()) {
	            
	      items.push(item);
	    }
	    
	      
	  //  self.parseResponse(reply);
	    
//	    persistResponse(reply);
	//	res.jsonp(self.response);
	//	res.jsonp("success");
	      
	  })
	  .on('end', function () {
	  	
	  	 var result = {
       				'meta': feedMeta,
                    'items': items,
                    'source':source
          };
         console.log("End Event in RSS");
	  	 self.parseResponse(result);
	  	 persistResponse(self.response);
	  	 
	  });
	  
 });
 
  
  
};

module.exports = RSSCollector;