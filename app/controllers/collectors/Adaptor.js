
var TwitterCollector = require('./TwitterCollector');
var YoutubeCollector = require('./YoutubeCollector');
var RSSCollector = require('./RSSCollector');
var FacebookCollector = require('./FacebookCollector');
//var scheduler = require('./Scheduler');

var classifier = require('../classifier/classifier.js');

var Q = require('q');
var Logger = require('../logger/logger');
var logger = new Logger;
var TAG='Adaptor:';



var Adaptor = function() {




  return {
    fetchTwitter: function(keyword,classifier,res) {
      return new TwitterCollector(classifier.getClassifier()).collect(keyword,res);
    },
    fetchYoutube: function(keyword,classifier,res){
    	return new YoutubeCollector(classifier.getClassifier()).collect(keyword,res);
    },
    fetchRSS:function(keyword,classifier,res) {
    	return new RSSCollector(classifier.getClassifier()).collectDB(keyword,res);
    },
    fetchAll:function(keyword,ipaddress,res) {
    	var startTime = Date.now();
    	var queue = [];

				// A standard NodeJS function: a parameter and a callback; the callback
				// will return error (if any) as first parameter and result as second
				// parameter.
		function fetchData(collector) {
				  // The 'deferred' object, base of the Promises proposal on CommonJS
				  var deferred = Q.defer();
				  if(process.env.NODE_ENV == 'production') {
					  collector.collectQ(keyword,function(err,response) {
					  		if(err) {
					  			deferred.reject(err);
					  		} else {
					  			deferred.resolve(response);
					  		}
					  });
				  }
				  else {
				  	  collector.collectQTest(keyword,function(err,response) {
					  		if(err) {
					  			deferred.reject(err);
					  		} else {
					  			deferred.resolve(response);
					  		}
					  });
				  }


				  return deferred.promise;
		}

		function populateCollectors() {

				queue.push(fetchData(new TwitterCollector(classifier.getClassifier())));
				queue.push(fetchData(new YoutubeCollector(classifier.getClassifier())));
				queue.push(fetchData(new RSSCollector(classifier.getClassifier())));
				queue.push(fetchData(new FacebookCollector(classifier.getClassifier())));



		    }


				populateCollectors();

				// Q.all: execute an array of 'promises' and 'then' call either a resolve
				// callback (fulfilled promises) or reject callback (rejected promises)
				var finalResponse=new Object();
				finalResponse.count=0;
				finalResponse.items=new Array();
				finalResponse.topResults=new Array();
				Q.all(queue).then(function(ful) {

				  // All the results from Q.all are on the argument as an array



				  ful.forEach(function(response) {
				  	 if (response){
					  	 finalResponse.count += response.count;
					  	 finalResponse.items = finalResponse.items.concat(response.items);
					  	 finalResponse.topResults = finalResponse.topResults.concat(response.topResults);
				  	 }

				  });


				}, function(rej) {

				  // The first rejected (error thrown) will be here only
				  console.log(TAG,'rejected', rej);
				}).fail(function(err) {

				  // If something whent wrong, then we catch it here, usually when there is no
				  // rejected callback.
				  console.log(TAG,'fail', err);
				}).fin(function() {

				  // Finally statemen; executed no matter of the above results
				  console.log(TAG,'finally');
				  res.jsonp(finalResponse);


				  	timeTaken = (Date.now() - startTime)/1000; //convert into seconds
				  	setTimeout(function() {
 						logger.save(keyword,ipaddress,timeTaken);
 					},10000);

				});




    }

  };
};

module.exports = Adaptor;
