var moment = require('moment');
var config = require('../../../../config/config');
var youtubev2 = require('youtube-feeds');
var youtube = require('youtube-api');
var gapis = require('googleapis');
var OAuth2 = gapis.auth.OAuth2
var oauth2Client = new OAuth2(config.youtube.clientID, config.youtube.clientSecret,config.youtube.callbackURL);
var youtubegapi = gapis.youtube('v3');
var youtubeanalytics = gapis.youtubeAnalytics('v1');
var Q = require('q');
var mongoose = require('mongoose'),
  Widget = mongoose.model('Widget'),
    WidgetData = mongoose.model('WidgetData'),
    Account = mongoose.model('Account'),
     _ = require('underscore'),
     CloudDatastore = require('../../lib/CloudDatastore.js') ;

var TAG="YoutubeCollector:";

var urlMap = {};
urlMap['Overview']={
    views:'Views',
    likes:'Likes',
    dislikes:'Dislikes',
    comments:'Comments',
    favoritesAdded:'Favorites',
    estimatedMinutesWatched:'EstMinutes',
    averageViewDuration:'AvgDuration'
};


var YoutubeCollector = function(classifier) {
  this.response = new Object();
  this.classifier = classifier;
  this.reader = '';
  this.widget = '';
};

YoutubeCollector.prototype.authenticate = function(account,callback) {
  try {

      var at = account.accessToken || null;
      var rt = account.refreshToken || null;
      console.log(TAG,'authenticate:',at,rt);
      oauth2Client.setCredentials({
          access_token: at,
          refresh_token: rt
      });

      oauth2Client.refreshAccessToken(function(err, tokens) {
        if(err){
          if(callback)callback(err,null);
        } else {
            console.log(TAG,'refreshTokens:');
            if(callback)callback(null,tokens);
        }
      });

  }catch(err){
    if(callback)callback(err);
  }

}


YoutubeCollector.prototype.fetchComments = function(videos,callback) {
  var self = this;
  var queue = [];
  // A standard NodeJS function: a parameter and a callback; the callback
      // will return error (if any) as first parameter and result as second
      // parameter.
  function fetchData(videoid) {
        // The 'deferred' object, base of the Promises proposal on CommonJS
        var deferred = Q.defer();


            youtubev2.feeds.comments(videoid,function(err,response) {

                if(err) {
                  deferred.reject(err);
                } else {
                  deferred.resolve(response);
                }
            });

        return deferred.promise;
  }

  function populateCollectors() {
      _.each(videos,function(videoId){
        queue.push(fetchData(videoId));
      });

  }


  populateCollectors(videos);

      // Q.all: execute an array of 'promises' and 'then' call either a resolve
      // callback (fulfilled promises) or reject callback (rejected promises)
      var comments=new Array();

      Q.all(queue).then(function(ful) {

        // All the results from Q.all are on the argument as an array



        ful.forEach(function(response) {
           if (response && response.entry){
              if(Array.isArray(response.entry)){
               comments = comments.concat(response.entry);
             }else {
               comments.push(response.entry);
             }
           }

        });


      }, function(rej) {

        // The first rejected (error thrown) will be here only
        console.log(TAG,'rejected', rej);
      }).fail(function(err) {

        // If something whent wrong, then we catch it here, usually when there is no
        // rejected callback.
        console.log(TAG,'fail', err);
      }).fin(function() {

        // Finally statemen; executed no matter of the above results
        console.log(TAG,'finally');
        self.parseAndPersistMentionsResponse(comments,callback);

      });

};


YoutubeCollector.prototype.collectObjectsQ = function(widget,account,callback) {
  var self = this;



  Account.findOne({domain:'youtube',name:account.accountName,user:widget.user}).exec(function(err, accountdata) {
      try {

        if (err) {

            if(callback)callback(err,null);
        } else {

           self.widget = widget;
            self.reader = youtubegapi;
            /*
            self.authenticate(accountdata,callback);
            self.fetchObjectsFromSource(widget,account,callback);
            */

            self.authenticate(accountdata,function(err,response){
                    //self.reader.setAppSecret(config.Youtube.clientSecret);
                if(err) {
                  if(callback)callback(err,null);
                } else {
                  self.fetchObjectsFromSource(widget,account,callback);
                }


            });


        }
      }catch (err) {
        if(callback)callback(err,null);
      }
    });




}


YoutubeCollector.prototype.collectData = function(widget,account) {
  var self = this;
/*
  Account.findOne({domain:'youtube',name:widget.config.accounts.selected}).exec(function(err, accountdata) {
        if (err) {

        } else {
        */
           self.widget = widget;
            self.reader = youtubegapi;
            //self.reader.authenticate({type:'oauth',token:account.accessToken});
            /*
            self.authenticate(account,callback);

            if(widget.type.indexOf('search') > -1 ) {
              //self.fetchMentionsFromSource(widget.config.custom.keyword);
             self.fetchMentionsFromSource(widget.config.objects.selected);
            } else {
              self.fetchMetricsFromSource(widget.config.objects.selected,widget.config.custom.videos.selected,widget.config.metrics.selected);
            }
            */

            self.authenticate(account,function(err,response){
              if(err) {
                if(callback)callback(err,null);
              } else {
                if(widget.type.indexOf('search') > -1 ) {
                  //self.fetchMentionsFromSource(widget.config.custom.keyword);
                 self.fetchMentionsFromSource(widget.config.objects.selected);
                } else {
                  self.fetchMetricsFromSource(widget.config.objects.selected,widget.config.custom.videos.selected,widget.config.metrics.selected);
                }
              }
            });


}

YoutubeCollector.prototype.collectDataQ = function(widget,account,callback) {
  var self = this;


/*
  Account.findOne({domain:'Youtube',name:widget.config.accounts.selected}).exec(function(err, accountdata) {
        if (err) {
            if(callback)callback(null,null);
        } else {
          */
          try {
          console.log(TAG,'collectDataQ:');
           self.widget = widget;
           self.reader = youtubegapi;
           /*
           self.authenticate(account,callback);
           if(widget.type.indexOf('search') > -1 ) {
              self.fetchMentionsFromSource(widget.config.objects.selected,callback);
            } else {

              self.fetchMetricsFromSource(widget.config.objects.selected,widget.config.custom.videos.selected,widget.config.metrics.selected,callback);
            }
            */

            self.authenticate(account,function(err,response){

              if(err){
                if(callback)callback(err);
              }else {
                 if(widget.type.indexOf('search') > -1 ) {
                   self.fetchMentionsFromSource(widget.config.objects.selected,callback);
                 } else {

                   self.fetchMetricsFromSource(widget.config.objects.selected,widget.config.custom.videos.selected,widget.config.metrics.selected,callback);
                 }
              }
            });

          }catch(err) {
            console.log(TAG,err);
            if(callback)callback(err,null);
          }
             /*
        }
    });
    */



}
YoutubeCollector.prototype.parseObjectsResponse = function(responseFromSource,callback) {
    var self=this;
    var response=self.response;


    try {
      var dataitems = {};
    _.each(responseFromSource.items,function(item) {
      dataitems[item.snippet.title]= item.contentDetails.relatedPlaylists.uploads;

    })

     //TODO: Refactor code
     if(callback) {
       callback(null,dataitems);
     }
   } catch(err) {
      if(callback)callback(err,null);
    }

};

YoutubeCollector.prototype.parseVideosResponse = function(responseFromSource,callback) {
    var self=this;
    var response=self.response;


    try {
      var dataitems = {};
    _.each(responseFromSource.items,function(item) {
      dataitems[item.snippet.resourceId.videoId]=item.snippet.title;

    })

     //TODO: Refactor code
     if(callback) {
       callback(null,dataitems);
     }
   } catch(err) {
      if(callback)callback(err,null);
    }

};


YoutubeCollector.prototype.parseResponse = function(responseFromSource) {
	  var self=this;
	  var response=self.response;



		  response.count = responseFromSource.itemsPerPage;
		  response.items = new Array();
		 response.topResults = new Array();
		  response.topResults[0] = new Object;
		  response.topResults[0].count=response.count;
		  response.topResults[0].source="youtube";
		  response.topResults[0].negativecount=0;
		  response.topResults[0].positivecount=0;
		  response.topResults[0].neutralcount=0;
		  var index = 0;
		  var items = responseFromSource.items;
		  for (var key in items) {
		  	 var item = items[key];
		  	 var title = item.title;
		  	 var desc = item.description;
		  	 var link = item.thumbnail.sqDefault;
		  	  var timeinfo = moment(item.updated)
	  	 	 var timeago = timeinfo.fromNow();
	  		 var timeseconds = timeinfo.unix();
		  	 response.items[index] = new Object();
		  	 response.items[index].youtubeCustom = new Object();

		  	 response.items[index].title= title;
		  	 response.items[index].link=link;
		  	 response.items[index].source = 'http://www.youtube.com/embed/'+item.id;
		  	 response.items[index].desc=desc;
		  	 response.items[index].category='youtube';
		  	  response.items[index].timeago = timeago;
	  		 response.items[index].timeseconds = timeseconds;
		  	 response.items[index].date = timeinfo.date()+'/'+(timeinfo.month()+1)+'/'+timeinfo.year();
		  	 response.items[index].id=Math.random();
		  	 response.items[index].influence_count=item.viewCount||0;
		  	 response.items[index].youtubeCustom.likesCount = item.likeCount || 0;
		  	 response.items[index].youtubeCustom.ratingCount = item.ratingCount || 0;
		  	 response.items[index].youtubeCustom.viewCount = item.viewCount || 0;
		  	 response.items[index].youtubeCustom.favouriteCount = item.favoriteCount || 0;
		  	 response.items[index].youtubeCustom.commentCount = item.commentCount || 0;




		  	 var sentObj = self.classifier.classify(desc);

		  	 response.items[index].emotions=sentObj.emotionList;
             response.items[index].keywords=sentObj.keywordList;

		 	response.items[index].sentiment='neutral';
	     	response.items[index].sentimentText=sentObj.sentiment;
		  	if(sentObj.sentiment == 'positive') {
		 	 	response.topResults[0].positivecount +=1;
		 	 } else if (sentObj.sentiment == 'negative'){
		 	 	response.topResults[0].negativecount +=1;
		 	 } else response.topResults[0].neutralcount +=1;
		  	 index++;
		  }







};

YoutubeCollector.prototype.parseAndPersistAnalyticsResponse = function(responseFromSource,metricname,callback) {
    var self=this;
    var response=self.response;

    var dataitems = {};
    var widgetdatalist=[];
    if(!responseFromSource.rows){
      if(callback)callback(null,null);
      return;
    }

    var headers = [];
    _.each(responseFromSource.columnHeaders, function(header){
      headers.push(header.name);
    });

    _.each(responseFromSource.rows,function(item) {

        //var uiMetricName = urlMap[metricname][item.name];

        var timeseconds = moment(item[0]).unix();
        if(!dataitems[timeseconds]) {
          dataitems[timeseconds]={}
        }
        for(var index=1;index < item.length;index++) {
          var uiMetricName = urlMap[metricname][headers[index]];

           dataitems[timeseconds][uiMetricName]=item[index];

        }

    });


      _.each(dataitems,function(val,key){

          //convert key into timemillis
            //var timeInfo = key * 1000;
            var resObj = {
                user: self.widget.user,
                dashboardId: self.widget.dashboardId,
                widgetId: self.widget._id,
                data : val,
                createdAt: (key*1000)

                };
              widgetdatalist.push(resObj);

        });

        //TODO: Refactor code
        if(callback) {
         callback(null,widgetdatalist);
        }else if(self.widget.config.storage.selected === 'Remote') {
        _.each(widgetdatalist,function(widgetdata){
          widgetdata.save(function(err){console.log(TAG,"parseAndPersisitAnalyticsResponse:",err)});
        });
        } else {
          Account.findOne({domain:'dropbox',name:self.widget.config.storage.selected,user:self.widget.user}).exec(function(err, accountdata) {
              if (err) {

              } else {
                   var clouddatastore = new CloudDatastore({accessToken:accountdata.accessToken});
                   _.each(widgetdatalist,function(widgetdata){
                     clouddatastore.save(widgetdata,function(err) {
                       if(err) console.log(TAG,'parseAndPersistAnalyticsResponse:',err)
                       //else
                       //clouddatastore.close();
                     });
                   });
              }
          });
        }
};

YoutubeCollector.prototype.parseAndPersistResponse = function(responseFromSource,callback) {
    var self=this;
    var response=self.response;
    try {

        var viewCount = responseFromSource.items[0].statistics.viewCount;
        var likeCount = responseFromSource.items[0].statistics.likeCount;
        var dislikeCount = responseFromSource.items[0].statistics.dislikeCount;
        var favoriteCount = responseFromSource.items[0].statistics.favoriteCount;
        var commentCount = responseFromSource.items[0].statistics.commentCount;



       var widgetdata = new WidgetData( {
                user: self.widget.user,
                dashboardId: self.widget.dashboardId,
                widgetId: self.widget._id,
                data : {
                  Views: viewCount,
                  Likes: likeCount,
                  Dislikes:dislikeCount,
                  Comments:commentCount,
                  Favorites:favoriteCount

                }

              });


       //TODO: Refactor code
       if(callback) {
         callback(null,widgetdata);
       }else if(self.widget.config.storage.selected === 'Remote') {
        widgetdata.save(function(err){console.log(TAG,"parseAndPersistAnalyticsResponse:",err)});
        } else {
          Account.findOne({domain:'dropbox',name:self.widget.config.storage.selected,user:self.widget.user}).exec(function(err, accountdata) {
              if (err) {

              } else {
                   var clouddatastore = new CloudDatastore({accessToken:accountdata.accessToken});
                   clouddatastore.save(widgetdata,function(err) {
                     if(err) console.log(TAG,'parseAndPersistAnalyticsResponse:',err)
                     //else
                     //clouddatastore.close();
                   });
              }
          });
        }
      }catch(err) {
        console.log(TAG,'parseAndPersistAnalyticsResponse:',err);
        if(callback)callback(err,null);
      }

};


YoutubeCollector.prototype.parseAndPersistMentionsResponse = function(responseFromSrc,callback) {
    var self=this;
    var response=self.response;
    try {
      var data = responseFromSrc;



      response.count = data.length;

      response.items = new Array();
      response.topResults = new Array();
        response.topResults[0] = new Object;
        response.topResults[0].count=response.count;
        response.topResults[0].source="Youtube";
        response.topResults[0].negativecount=0;
        response.topResults[0].positivecount=0;
        response.topResults[0].neutralcount=0;
      var index = 0;
      for (var key in data) {
         var comment = data[key];
         var timeinfo = moment(comment.published.$t,'YYYY-MM-DD hh:mm:ss Z');
         //console.log(timeinfo);

         var title = comment.title.$t
         if(!title || title === '') continue;
         var titleSliced = null;
         if(title.length > 100){
           titleSliced = title.slice(0,100) + "...";
         }
         response.items[index] = new Object();
         response.items[index].metrics = new Object();
         response.items[index].title=titleSliced || title;
         response.items[index].link='http://www.youtube.com';
         response.items[index].source = 'http://youtube.com';
         response.items[index].timeago = timeinfo.fromNow();
         response.items[index].timeseconds = timeinfo.unix();
         response.items[index].date = timeinfo.date()+'/'+(timeinfo.month()+1)+'/'+timeinfo.year();
         response.items[index].desc=title;
         response.items[index].category='youtube';
         response.items[index].id=Math.random();
         response.items[index].created_at=comment.published.$t;
         response.items[index].replies=comment.yt$replyCount.$t || 0;
         response.items[index].metrics['Replies']=comment.yt$replyCount.$t || 0;

         response.items[index].influence_count=response.items[index].replies;

         var sentObj = self.classifier.classify(response.items[index].title);
         response.items[index].emotions=sentObj.emotionList;
           response.items[index].keywords=sentObj.keywordList;

         response.items[index].sentiment=sentObj.sentiment;
         response.items[index].sentimentText=sentObj.sentiment;

       if(sentObj.sentiment == 'positive') {
          response.topResults[0].positivecount +=1;
        } else if (sentObj.sentiment == 'negative'){
          response.topResults[0].negativecount +=1;
        } else response.topResults[0].neutralcount +=1;

         index++;


      }
       var widgetdata = new WidgetData( {
              user: self.widget.user,
              dashboardId: self.widget.dashboardId,
              widgetId: self.widget._id,
              data : self.response

            });


      if(callback) {
       callback(null,widgetdata);
      } else {
        widgetdata.save(function(err){console.log("Saving widget data",err)});
      }
    } catch (err) {
      console.log(TAG,err);
      if(callback)callback(err,null);

    }

};
YoutubeCollector.prototype.fetchObjectsFromSource = function(widget,account,callback) {
  var self=this;


    if(account.channelName) {
      self.reader.playlistItems.list({"part":"snippet","playlistId":account.channelName,"auth": oauth2Client},function(err,data){
        if(err) {
          console.log("Error in Fetching From Youtube",err);
          if(callback)callback(err,null);

        } else {
          console.log(TAG,'fetchObjectFromSource:');
          self.parseVideosResponse(data,callback);

        }

      })

    } else {
      self.reader.channels.list({"part":"snippet,contentDetails","mine":true,"auth": oauth2Client},function(err,data) {
        if(err) {
          console.log(TAG,'fetchObjectsFromSource:',err);
          if(callback)callback(err,null);

        } else {
          console.log(TAG,'fetchObjectsFromSource:');
          self.parseObjectsResponse(data,callback);

        }

    });
  }
 };

function formatDateString(date) {
    var yyyy = date.getFullYear().toString();
    var mm = padToTwoCharacters(date.getMonth() + 1);
    var dd = padToTwoCharacters(date.getDate());

    return yyyy + '-' + mm + '-' + dd;
  }

  // If number is a single digit, prepend a '0'. Otherwise, return it as a string.
  function padToTwoCharacters(number) {
    if (number < 10) {
      return '0' + number;
    } else {
      return number.toString();
    }
  }

YoutubeCollector.prototype.fetchMetricsFromSource = function(channelId,videoId,selectedMetric,callback) {
   var self=this;
  var ONE_MONTH_IN_MILLISECONDS = 1000 * 60 * 60 * 24 * 30;
  var today = new Date();
  var lastMonth = new Date(today.getTime() - ONE_MONTH_IN_MILLISECONDS);
  //ids:'channel==UCSOa0RtWby1kIVNCz-GO1mA',
  //'views,comments,favoritesAdded,likes,dislikes,estimatedMinutesWatched,averageViewDuration'
  var metriclist=[];


  try {
      _.each(urlMap[selectedMetric],function(val,key){
        metriclist.push(key);
      });
      var analyticsParams =  {
        'start-date':formatDateString(lastMonth),
        'end-date':formatDateString(today),
        ids:'channel==MINE',
        dimensions:'day',
        filters:'video==' + videoId,
        metrics:metriclist.join(','),
        sort:'day',
        auth: oauth2Client
      }
      console.log(TAG,'fetchingMetricsFromSource:');

      youtubeanalytics.reports.query(analyticsParams,function(err,response){
        try {
          if(err){
            if(callback)callback(err);
          } else {
            self.parseAndPersistAnalyticsResponse(response,selectedMetric,callback);
          }
        }catch(err) {
          if(callback)callback(err);
        }

      });
      /*
       self.reader.videos.list({"part":"statistics","id":videoId},function(err, data) {
           if(err) {
             console.log("Error in Fetching From Youtube",err);
             if(callback)callback(err,null);

           } else {
             console.log("Reading from Youtube",data);
             try {

             self.parseAndPersistResponse(data,callback);
           }catch(err) {
             if(callback)callback(err,null);
             console.log("Error in Youtube",err);
           }
           //persistResponse(data);
           //res.jsonp(self.response);
           //callback(null,self.response);
           }
       });
       */
  }catch(err){
    if(callback)callback(err);
  }
};


YoutubeCollector.prototype.fetchMentionsFromSource = function(entityname,callback) {
  var self=this;

  try {
    self.reader.playlistItems.list({"part":"snippet","playlistId":entityname,"auth": oauth2Client},function(err,data) {
      if(err) {
        console.log(TAG,'fetchMentionsFromSource:',err);
        callback(err,null);
      } else {
        console.log(TAG,'fetchMentionsFromSource:');
        var videos=[];
        for(var key in data.items) {
          videos.push(data.items[key].snippet.resourceId.videoId);

        }
        self.fetchComments(videos,callback);


      }
  });

  }catch(err){
    if(callback)callback(err);
  }
};

module.exports = YoutubeCollector;
