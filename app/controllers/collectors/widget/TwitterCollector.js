
var moment = require('moment');
var config = require('../../../../config/config');
var twitter = require('twit');
var twittertxt=require('twitter-text');
twittertxt.autoLink(twittertxt.htmlEscape('#hello < @world >'));
var mongoose = require('mongoose'),
	Widget = mongoose.model('Widget'),
    WidgetData = mongoose.model('WidgetData'),
    Account = mongoose.model('Account'),
     _ = require('underscore'),
     CloudDatastore = require('../../lib/CloudDatastore.js') ;


var TAG='TwitterCollector:';
//https://api.twitter.com/1.1/users/show.json?screen_name=rsarver


var TwitterCollector = function(classifier) {
  this.response = new Object();
  this.classifier = classifier;
  this.reader = '';
  this.widget = '';



};


TwitterCollector.prototype.collectData = function(widget,account) {
  var self = this;

			try {
     			self.widget = widget;
          self.reader = new twitter({
		        consumer_key: config.twitter.clientID,
		        consumer_secret: config.twitter.clientSecret,
		        access_token: account.accessToken,
		        access_token_secret: account.accessTokenSecret
	  			});
             if(widget.type.indexOf('search') > -1 ) {
             	self.fetchMentionsFromSource(widget.config.custom.keyword);
             } else {
             	self.fetchMetricsFromSource(widget.config.custom.username);
             }

			}catch(err){
				console.log(TAG,err);
			}
}

TwitterCollector.prototype.collectDataQ = function(widget,account,callback) {
  var self = this;


/*
	Account.findOne({domain:'twitter',name:widget.config.accounts.selected}).exec(function(err, accountdata) {
        if (err) {
            if(callback)callback(null,null);
        } else {
        	*/
					try {
						console.log(TAG,'collectDataQ:',account.accessToken);
	       		self.widget = widget;
	            self.reader = new twitter({
		        consumer_key: config.twitter.clientID,
		        consumer_secret: config.twitter.clientSecret,
		        access_token: account.accessToken,
		        access_token_secret: account.accessTokenSecret
		  		});
	             if(widget.type.indexOf('search') > -1 ) {
	             	self.fetchMentionsFromSource(widget.config.custom.keyword,callback);
	             } else {
	             	self.fetchMetricsFromSource(widget.config.custom.username,callback);
	             }
					}catch (err) {
						if(callback)callback(err);
					}
       /*
        }
    });
 	 */



}


TwitterCollector.prototype.parseAndPersistResponse = function(responseFromSource,callback) {
	  var self=this;
	  var response=self.response;
		try {
			  var favouritesCount = responseFromSource.favourites_count;
			  var listedCount = responseFromSource.listed_count;
			  var followersCount = responseFromSource.followers_count;
			  var tweetsCount = responseFromSource.statuses_count;
			  var friendsCount = responseFromSource.friends_count;


			 var widgetdata = new WidgetData( {
				  			user: self.widget.user,
				  			dashboardId: self.widget.dashboardId,
				  			widgetId: self.widget._id,
				  			data : { Likes:favouritesCount,
							  	Listed: listedCount,
							  	Followers: followersCount,
							  	Tweets: tweetsCount,
							  	Friends: friendsCount
							  }

			  			});

			 //TODO: Refactor code
			 if(callback) {
			 	callback(null,widgetdata);
			 }else if(self.widget.config.storage.selected === 'Remote') {
			  widgetdata.save(function(err){console.log("Saving widget data",err)});
		    } else {
		    	Account.findOne({domain:'dropbox',name:self.widget.config.storage.selected,user:self.widget.user}).exec(function(err, accountdata) {
			        if (err) {

			        } else {
			       			var clouddatastore = new CloudDatastore({accessToken:accountdata.accessToken});
			       			clouddatastore.save(widgetdata,function(err) {
			       				if(err) console.log(TAG,'parseAndPersistResponse:',err)
			       				//else
			       				//clouddatastore.close();
			       			});
			        }
		    	});
		    }
			}catch(err) {
				if(callback)callback(err);
			}

};

TwitterCollector.prototype.parseAndPersistMentionsResponse = function(responseFromSource,callback) {
	  var self=this;
	  var response=self.response;

		try {
			  var statuses = responseFromSource.statuses;

			  response.count = responseFromSource.search_metadata.count;
			  response.items = new Array();
			 response.topResults = new Array();
				  response.topResults[0] = new Object;
				  response.topResults[0].count=response.count;
				  response.topResults[0].source="twitter";
				  response.topResults[0].negativecount=0;
				  response.topResults[0].positivecount=0;
				  response.topResults[0].neutralcount=0;
			  var index = 0;

			  for (var key in statuses) {
			  	 var status = statuses[key];
			  	 var timeinfo = moment(status.created_at,'dd MMM DD HH:mm:ss ZZ YYYY','en')

			  	 response.items[index] = new Object();
			  	 response.items[index].user = new Object();
			  	 response.items[index].metrics = new Object();
			  	 response.items[index].title=status.text;
			  	 response.items[index].link='http://twitter.com/' +status.user.screen_name+'/status/'+status.id_str;
			  	 response.items[index].source = 'http://twitter.com';
			  	 response.items[index].timeago = timeinfo.fromNow();
			  	 response.items[index].timeseconds = timeinfo.unix();
			  	 response.items[index].date = timeinfo.date()+'/'+(timeinfo.month()+1)+'/'+timeinfo.year();
			  	 response.items[index].desc="";
			  	 response.items[index].category='twitter';
			  	 response.items[index].id=Math.random();
			  	 response.items[index].user.screen_name=status.user.screen_name;
			  	 response.items[index].user.profile_image_url=status.user.profile_image_url;
			  	 response.items[index].user.name=status.user.name;
			  	 response.items[index].user.reply_to=status.in_reply_to_user_id_str;
			  	 response.items[index].user.retweet_to=status.id_str;
			  	 response.items[index].created_at=status.created_at;
			  	 response.items[index].user.followers_count=status.user.followers_count;
			  	 response.items[index].retweet_count=status.retweet_count;
			  	 response.items[index].influence_count=status.user.followers_count;
			  	 response.items[index].metrics['Retweets'] = status.retweet_count || 0;
			  	 response.items[index].metrics['Followers'] = status.user.followers_count || 0;
			  	 response.items[index].metrics['Friends'] = status.user.friends_count || 0;
			  	 response.items[index].metrics['Favourites'] = status.favorite_count || 0;
			  	 response.items[index].metrics['Statuses'] = status.user.statuses_count || 0;

			  	 response.items[index].usermentions=twittertxt.extractMentions(status.text);
		         response.items[index].hashtagmentions=twittertxt.extractHashtags(status.text);
		         status.text = status.text.replace(/\.|"|,|\!|(\s+|^)@\w+|(\s+|^)#\w+/ig,"");



			  	 var sentObj = self.classifier.classify(status.text);

			  	 response.items[index].sentiment=sentObj.sentiment;
			     response.items[index].sentimentText=sentObj.sentiment;
		         response.items[index].emotions=sentObj.emotionList;
		         response.items[index].keywords=sentObj.keywordList;

			 	 if(sentObj.sentiment == 'positive') {
			 	 	response.topResults[0].positivecount +=1;
			 	 } else if (sentObj.sentiment == 'negative'){
			 	 	response.topResults[0].negativecount +=1;
			 	 } else response.topResults[0].neutralcount +=1;

			  	 index++;


			  }
			   var widgetdata = new WidgetData( {
				  			user: self.widget.user,
				  			dashboardId: self.widget.dashboardId,
				  			widgetId: self.widget._id,
				  			data : self.response

			  			});


			  if(callback) {
			 		callback(null,widgetdata);
			  } else {
			  	widgetdata.save(function(err){console.log(TAG,'parseAndPersistMentions:',err)});
			  }
		}catch(err) {
			if(callback)callback(err);
		}

};



TwitterCollector.prototype.fetchMetricsFromSource = function(username,callback) {
  var self=this;

	try {
	  self.reader.get('users/show', { screen_name: username }, function(err, data) {
		  	if(err) {
		  		console.log(TAG,'fetchMetricsFromSource:',err);
		  		if(callback)callback(err,null);

		  	} else {
		  		console.log(TAG,'fetchMetricsFromSource',data);
					self.parseAndPersistResponse(data,callback);
				//persistResponse(data);
				//res.jsonp(self.response);
				//callback(null,self.response);
		  	}
	  });
	}catch(err){
		if(callback)callback(err);
	}
 };
TwitterCollector.prototype.fetchMentionsFromSource = function(keyword,callback) {
  var self=this;

	try {
	  var query = keyword + '-filter:retweets -filter:links -filter:replies -filter:images';

	  self.reader.get('search/tweets', { q: query, count: 30, lang:'en', locale:'en' }, function(err, reply) {
		  	if(err) {
		  		console.log(TAG,'fetchMetionsFromSource:',err);
		  		if(callback)callback(err,null);
		  	} else {

				self.parseAndPersistMentionsResponse(reply,callback);
				//persistResponse(reply);
				//res.jsonp(self.response);
				//callback(null,self.response);
		  	}
	  });
	}catch(err){
		if(callback)callback(err);
	}

};

module.exports = TwitterCollector;
