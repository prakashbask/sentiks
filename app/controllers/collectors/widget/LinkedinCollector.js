
var moment = require('moment');
var config = require('../../../../config/config');
var Linkedin = require('node-linkedin')(config.linkedin.clientID,config.linkedin.clientSecret,config.linkedin.callbackURL);
var mongoose = require('mongoose'),
	Widget = mongoose.model('Widget'),
    WidgetData = mongoose.model('WidgetData'),
    Account = mongoose.model('Account'),
     _ = require('underscore'),
     CloudDatastore = require('../../lib/CloudDatastore.js') ;


var TAG="LinkedInCollector:";



var LinkedinCollector = function(classifier) {
  this.response = new Object();
  this.classifier = classifier;
  this.reader = '';
  this.widget = '';



};


LinkedinCollector.prototype.collectData = function(widget,account) {
  var self = this;
/*
	Account.findOne({domain:'twitter',name:widget.config.accounts.selected}).exec(function(err, accountdata) {
        if (err) {

        } else {
      */
			try {
       		self.widget = widget;
            self.reader = Linkedin.init(account.accessToken);
             if(widget.type.indexOf('search') > -1 ) {
             	self.fetchMentionsFromSource(widget.config.custom.keyword);
             } else {
             	self.fetchMetricsFromSource(widget.config.custom.username);
             }
			}catch(err) {
				console.log(TAG,err);
			}
        /*
        }
    });
 	 */



}

LinkedinCollector.prototype.collectDataQ = function(widget,account,callback) {
  var self = this;
  console.log(TAG,'collectDataQ:',account.accessToken);

/*
	Account.findOne({domain:'twitter',name:widget.config.accounts.selected}).exec(function(err, accountdata) {
        if (err) {
            if(callback)callback(null,null);
        } else {
        	*/
					try {
       		self.widget = widget;

          	self.reader = Linkedin.init(account.accessToken);
             if(widget.type.indexOf('search') > -1 ) {
             	self.fetchMentionsFromSource(widget.config.custom.keyword,callback);
             } else {
             	self.fetchMetricsFromSource(widget.config.custom.username,callback);
             }
					}catch(err) {
						console.log(TAG,err);
					}
       /*
        }
    });
 	 */



}


LinkedinCollector.prototype.parseAndPersistResponse = function(responseFromSource,callback) {
	  var self=this;
	  var response=self.response;
		try {
		  var connectionsCount = responseFromSource.numConnections;
		  var recommendationsRecdCount = responseFromSource.recommendationsReceived._total;
		  var profileViewsCount = responseFromSource.relatedProfileViews._total;


		 var widgetdata = new WidgetData( {
			  			user: self.widget.user,
			  			dashboardId: self.widget.dashboardId,
			  			widgetId: self.widget._id,
			  			data : {
						  	Connections: connectionsCount,
						  	Recommendations: recommendationsRecdCount,
						  	ProfileViews:profileViewsCount

						  }

		  			});

	 //TODO: Refactor code
	 if(callback) {
	 	callback(null,widgetdata);
	 }else if(self.widget.config.storage.selected === 'Remote') {
	  widgetdata.save(function(err){console.log(TAG,'parseAndPersistResponse:',err)});
    } else {
    	Account.findOne({domain:'dropbox',name:self.widget.config.storage.selected,user:self.widget.user}).exec(function(err, accountdata) {
	        if (err) {

	        } else {
	       			var clouddatastore = new CloudDatastore({accessToken:accountdata.accessToken});
	       			clouddatastore.save(widgetdata,function(err) {
	       				if(err) console.log(TAG,'parseAndPersistResponse:',err)
	       				//else
	       				//clouddatastore.close();
	       			});
	        }
    	});
    }
	}catch(err) {
		if(callback)callback(err);
	}

};

LinkedinCollector.prototype.parseAndPersistMentionsResponse = function(responseFromSource,callback) {
	/*
	  var self=this;
	  var response=self.response;
	  var statuses = responseFromSource.statuses;

	  response.count = responseFromSource.search_metadata.count;
	  response.items = new Array();
	 response.topResults = new Array();
		  response.topResults[0] = new Object;
		  response.topResults[0].count=response.count;
		  response.topResults[0].source="linkedin";
		  response.topResults[0].negativecount=0;
		  response.topResults[0].positivecount=0;
		  response.topResults[0].neutralcount=0;
	  var index = 0;

	  for (var key in statuses) {
	  	 var status = statuses[key];
	  	 var timeinfo = moment(status.created_at,'dd MMM DD HH:mm:ss ZZ YYYY','en')

	  	 response.items[index] = new Object();
	  	 response.items[index].user = new Object();
	  	 response.items[index].metrics = new Object();
	  	 response.items[index].title=status.text;
	  	 response.items[index].link='http://twitter.com/' +status.user.screen_name+'/status/'+status.id_str;
	  	 response.items[index].source = 'http://twitter.com';
	  	 response.items[index].timeago = timeinfo.fromNow();
	  	 response.items[index].timeseconds = timeinfo.unix();
	  	 response.items[index].date = timeinfo.date()+'/'+(timeinfo.month()+1)+'/'+timeinfo.year();
	  	 response.items[index].desc="";
	  	 response.items[index].category='twitter';
	  	 response.items[index].id=Math.random();
	  	 response.items[index].user.screen_name=status.user.screen_name;
	  	 response.items[index].user.profile_image_url=status.user.profile_image_url;
	  	 response.items[index].user.name=status.user.name;
	  	 response.items[index].user.reply_to=status.in_reply_to_user_id_str;
	  	 response.items[index].user.retweet_to=status.id_str;
	  	 response.items[index].created_at=status.created_at;
	  	 response.items[index].user.followers_count=status.user.followers_count;
	  	 response.items[index].retweet_count=status.retweet_count;
	  	 response.items[index].influence_count=status.user.followers_count;
	  	 response.items[index].metrics['Retweets'] = status.retweet_count || 0;
	  	 response.items[index].metrics['Followers'] = status.user.followers_count || 0;
	  	 response.items[index].metrics['Friends'] = status.user.friends_count || 0;
	  	 response.items[index].metrics['Favourites'] = status.favorite_count || 0;
	  	 response.items[index].metrics['Statuses'] = status.user.statuses_count || 0;

	  	 response.items[index].usermentions=twittertxt.extractMentions(status.text);
         response.items[index].hashtagmentions=twittertxt.extractHashtags(status.text);
         status.text = status.text.replace(/\.|"|,|\!|(\s+|^)@\w+|(\s+|^)#\w+/ig,"");



	  	 var sentObj = self.classifier.classify(status.text);

	  	 response.items[index].sentiment=sentObj.sentiment;
	     response.items[index].sentimentText=sentObj.sentiment;
         response.items[index].emotions=sentObj.emotionList;
         response.items[index].keywords=sentObj.keywordList;

	 	 if(sentObj.sentiment == 'positive') {
	 	 	response.topResults[0].positivecount +=1;
	 	 } else if (sentObj.sentiment == 'negative'){
	 	 	response.topResults[0].negativecount +=1;
	 	 } else response.topResults[0].neutralcount +=1;

	  	 index++;


	  }
	   var widgetdata = new WidgetData( {
		  			user: self.widget.user,
		  			dashboardId: self.widget.dashboardId,
		  			widgetId: self.widget._id,
		  			data : self.response

	  			});


	  if(callback) {
	 	callback(null,widgetdata);
	  } else {
	  	widgetdata.save(function(err){console.log("Saving widget data",err)});
	  }
	 */
};



LinkedinCollector.prototype.fetchMetricsFromSource = function(username,callback) {
  var self=this;

	try {
	  self.reader.people.me(function(err, data) {
		  	if(err) {
		  		console.log(TAG,"fetchMetricsFromSource:",err);
		  		if(callback)callback(err,null);

		  	} else {
		  		console.log(TAG,"fetchMetricsFromSource:",data);
		  		try {
					self.parseAndPersistResponse(data,callback);
				}catch(err) {
					console.log(TAG,"fetchMetricsFromSource:",err);
					if(callback)callback(err,null);
				}
				//persistResponse(data);
				//res.jsonp(self.response);
				//callback(null,self.response);
		  	}
	  });

}catch(err){
	if(callback)callback(err);
}
 };
LinkedinCollector.prototype.fetchMentionsFromSource = function(keyword,callback) {
 /*
  var self=this;
  var query = keyword + '-filter:retweets -filter:links -filter:replies -filter:images';

  self.reader.get('search/tweets', { q: query, count: 30, lang:'en', locale:'en' }, function(err, reply) {
	  	if(err) {

	  		if(callback)callback(null,null);
	  	} else {

			self.parseAndPersistMentionsResponse(reply,callback);
			//persistResponse(reply);
			//res.jsonp(self.response);
			//callback(null,self.response);
	  	}
  });
 */

};

module.exports = LinkedinCollector;
