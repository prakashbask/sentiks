
var TwitterCollector = require('./TwitterCollector');
var FacebookCollector = require('./FacebookCollector');
var LinkedinCollector = require('./LinkedinCollector');
var YoutubeCollector = require('./YoutubeCollector');
var classifier = require('../../classifier/classifier.js');
var cMap={};

cMap['twitter'] = TwitterCollector;
cMap['facebook'] = FacebookCollector;
cMap['linkedin'] = LinkedinCollector;
cMap['youtube'] = YoutubeCollector;



exports.getCollector = function (type) {
    return new cMap[(type.split('-'))[0]](classifier.getClassifier());
};
