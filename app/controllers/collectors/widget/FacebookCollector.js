var moment = require('moment');
var config = require('../../../../config/config');
//var facebook = require('fbgraph');
var facebook = require('fb');
var mongoose = require('mongoose'),
	Widget = mongoose.model('Widget'),
    WidgetData = mongoose.model('WidgetData'),
    Account = mongoose.model('Account'),
     _ = require('underscore'),
     CloudDatastore = require('../../lib/CloudDatastore.js') ;

var TAG='FacebookCollector:';

var urlMap = {};
urlMap['Overview']={
		page_impressions_unique:'Reach',
		page_views:'Views',
		page_engaged_users:'Engaged',
		page_consumptions:'Clicks',
		page_fan_adds:'Likes'
};
urlMap['Reach']={
	page_impressions_unique:'Reach',
	page_impressions_paid_unique:'Paid',
	page_impressions_organic_unique:'Organic',
	page_impressions_viral_unique:'Viral'

};
urlMap['Impressions']={
	page_impressions:'Impressions',
	page_impressions_paid:'Paid',
	page_impressions_organic:'Organic',
	page_impressions_viral:'Viral'

};
urlMap['Views']={
	page_views:'Views',
	page_views_login:'Login',
	page_views_logout:'Logout'

};
urlMap['Likes']={
	page_fan_adds:'Likes',
	page_fan_removes:'Unlikes'

};
urlMap['Stories']={
	page_stories:'Stories'

};
urlMap['Checkins']={
	page_places_checkin_total:'Checkins',
	page_places_checkin_mobile:'Mobile'

};
urlMap['Clicks']={
	page_consumptions:'Clicks',
	page_consumptions_unique:'Unique'

};
urlMap['Posts']={
	page_posts_impressions_unique:'Posts Reach',
	page_posts_impressions_paid_unique:'Posts Paid',
	page_posts_impressions_organic_unique:'Posts Organic',
	page_posts_impressions_viral_unique:'Posts Viral'

};
//initialize handler for facebook
//facebook  app_id=595549690523774
//facebook app secret= c7da7e697f0e3bb0dc0ff1fd41f3ae09



//https://graph.facebook.com/search?q=kejriwal&type=post&access_token=595549690523774|c7da7e697f0e3bb0dc0ff1fd41f3ae09



var FacebookCollector = function(classifier) {
  this.response = new Object();
  this.classifier = classifier;
  this.reader = '';
  this.widget = '';




};
FacebookCollector.prototype.collectObjectsQ = function(widget,account,callback) {
  var self = this;


 try {
	Account.findOne({domain:'facebook',name:account.accountName,user:widget.user}).exec(function(err, accountdata) {
        if (err) {
            if(callback)callback(err,null);
        } else {

       		self.widget = widget;
            self.reader = facebook;
            self.reader.setAccessToken(accountdata.accessToken);
            //self.reader.setAppSecret(config.facebook.clientSecret);
            self.fetchObjectsFromSource(widget,callback);

        }
    });

	}catch(err) {
		if(callback)callback(err);
	}


}

FacebookCollector.prototype.collectData = function(widget,account) {
  var self = this;
/*
	Account.findOne({domain:'facebook',name:widget.config.accounts.selected}).exec(function(err, accountdata) {
        if (err) {

        } else {
        */
			try {
       		self.widget = widget;
            self.reader = facebook;
            self.reader.setAccessToken(account.accessToken);
       //     self.reader.setAppSecret(config.facebook.clientSecret);

             if(widget.type.indexOf('search') > -1 ) {
             	//self.fetchMentionsFromSource(widget.config.custom.keyword);
							self.fetchMentionsFromSource(widget.config.objects.selected);
             } else {
             	self.fetchMetricsFromSourceBatch(widget.config.objects.selected,widget.config.metrics.selected);
             }
         /*
        }
    });
 	 */
	} catch(err) {
		if(callback)callback(err);
	}


}

FacebookCollector.prototype.collectDataQ = function(widget,account,callback) {
  var self = this;


/*
	Account.findOne({domain:'facebook',name:widget.config.accounts.selected}).exec(function(err, accountdata) {
        if (err) {
            if(callback)callback(null,null);
        } else {
        	*/
			try {
       		self.widget = widget;
            self.reader = facebook;
            self.reader.setAccessToken(account.accessToken);
          //  self.reader.setAppSecret(config.facebook.clientSecret);
             if(widget.type.indexOf('search') > -1 ) {
             	self.fetchMentionsFromSource(widget.config.objects.selected,callback);
             } else {

             	self.fetchMetricsFromSourceBatch(widget.config.objects.selected,widget.config.metrics.selected,callback);
             }
             /*
        }
    });
 	 */
		}catch (err) {
			if(callback)callback(err);
		}


}
FacebookCollector.prototype.parseObjectsResponse = function(responseFromSource,callback) {
	  var self=this;
	  var response=self.response;

		try {

			  var dataitems = {};
			_.each(responseFromSource.data,function(item) {
				dataitems[item.name]= item.id;

			})

			 //TODO: Refactor code
			 if(callback) {
			 	callback(null,dataitems);
			 }
		}catch(err) {
			if(callback)callback(err);
		}

};

FacebookCollector.prototype.parseAndPersistResponse = function(responseFromSource,metricname,callback) {
	  var self=this;
	  var response=self.response;


	try {
	  var dataitems = {};
	  var widgetdatalist=[];
	_.each(responseFromSource,function(item) {

		var uiMetricName = urlMap[metricname][item.name];
		_.each(item.values,function(valueItem){
			var timeseconds = moment(valueItem.end_time).unix();
			if(!dataitems[timeseconds]) {
				dataitems[timeseconds]={}
			}

			dataitems[timeseconds][uiMetricName]=valueItem.value;
			//dataitems[timeseconds][uiMetricName]=Math.floor((Math.random()*1000)+1);

		});

	});


		_.each(dataitems,function(val,key){

			//convert key into timemillis
				//var timeInfo = key * 1000;
				var resObj = {
		  			user: self.widget.user,
		  			dashboardId: self.widget.dashboardId,
		  			widgetId: self.widget._id,
		  			data : val,
		  			createdAt: (key*1000)

	  				};
	  			widgetdatalist.push(resObj);

		});

	 //TODO: Refactor code
	 if(callback) {
	 	callback(null,widgetdatalist);
	 }else if(self.widget.config.storage.selected === 'Remote') {
	  _.each(widgetdatalist,function(widgetdata){
	  	widgetdata.save(function(err){console.log(TAG,"parseAndPersistResponse:",err)});
	  });
    } else {
    	Account.findOne({domain:'dropbox',name:self.widget.config.storage.selected,user:self.widget.user}).exec(function(err, accountdata) {
	        if (err) {

	        } else {
	       			var clouddatastore = new CloudDatastore({accessToken:accountdata.accessToken});
	       			_.each(widgetdatalist,function(widgetdata){
		       			clouddatastore.save(widgetdata,function(err) {
		       				if(err) console.log(TAG,"parseAndPersistResponse:",err)
		       				//else
		       				//clouddatastore.close();
		       			});
		       		});
	        }
    	});
    }
	}catch(err) {
		if(callback)callback(err);
	}

};

FacebookCollector.prototype.parseAndPersistMentionsResponse = function(responseFromSrc,callback) {
	  var self=this;
	  var response=self.response;
		try {
			var data = [];
			for (var key in responseFromSrc.data) {
				if(responseFromSrc.data[key].comments) {
					data = data.concat(responseFromSrc.data[key].comments.data);

				}
			}


		  response.count = data.length;

		  response.items = new Array();
		  response.topResults = new Array();
			  response.topResults[0] = new Object;
			  response.topResults[0].count=response.count;
			  response.topResults[0].source="facebook";
			  response.topResults[0].negativecount=0;
			  response.topResults[0].positivecount=0;
			  response.topResults[0].neutralcount=0;
		  var index = 0;
		  for (var key in data) {
		  	 var comment = data[key];
		  	 var timeinfo = moment(comment.created_time,'YYYY-MM-DD hh:mm:ss Z');
		  	 //console.log(timeinfo);

		  	 var title = comment.message || comment.name;
		  	 if(!title) continue;
		  	 var titleSliced = null;
		  	 if(title.length > 100){
		  	 	titleSliced = title.slice(0,100) + "...";
		  	 }
		  	 response.items[index] = new Object();
		  	 response.items[index].metrics = new Object();
		  	 response.items[index].title=titleSliced || title;
		  	 response.items[index].link=comment.link || 'http://www.facebook.com';
		  	 response.items[index].source = 'http://facebook.com';
		  	 response.items[index].timeago = timeinfo.fromNow();
		  	 response.items[index].timeseconds = timeinfo.unix();
		  	 response.items[index].date = timeinfo.date()+'/'+(timeinfo.month()+1)+'/'+timeinfo.year();
		  	 response.items[index].desc=title;
		  	 response.items[index].category='facebook';
		  	 response.items[index].id=Math.random();
		  	 response.items[index].created_at=comment.created_time;
		  	 response.items[index].likes=comment.like_count || 0;
		  	 response.items[index].metrics['Likes']=comment.like_count || 0;

		  	 response.items[index].influence_count=response.items[index].likes;

		  	 var sentObj = self.classifier.classify(response.items[index].title);
		  	 response.items[index].emotions=sentObj.emotionList;
	         response.items[index].keywords=sentObj.keywordList;

		  	 response.items[index].sentiment=sentObj.sentiment;
		     response.items[index].sentimentText=sentObj.sentiment;

		 	if(sentObj.sentiment == 'positive') {
		 	 	response.topResults[0].positivecount +=1;
		 	 } else if (sentObj.sentiment == 'negative'){
		 	 	response.topResults[0].negativecount +=1;
		 	 } else response.topResults[0].neutralcount +=1;

		  	 index++;


		  }
		   var widgetdata = new WidgetData( {
			  			user: self.widget.user,
			  			dashboardId: self.widget.dashboardId,
			  			widgetId: self.widget._id,
			  			data : self.response

		  			});


		  if(callback) {
		 	callback(null,widgetdata);
		  } else {
		  	widgetdata.save(function(err){console.log(TAG,"parseAndPersistMentionsResponse:",err)});
		  }
		} catch (err) {
			console.log(TAG,'parseAndPersistMentionsResponse:',err);
			if(callback)callback(err,null);

		}

};
FacebookCollector.prototype.fetchObjectsFromSource = function(widget,callback) {
  var self=this;

  var url = 'me/accounts';
  //self.reader.get(url,function(err, data) {
	try {
	  	self.reader.api(url,function(reply) {
		  	if(!reply || reply.error) {

		  		console.log(TAG,'fetchObjectsFromSource:',!reply ? 'error occurred' : reply.error);
		  		if(callback)callback('Error in fetching objects',null);

		  	} else {
					console.log(TAG,"fetchObjectsFromSource:",reply);
				self.parseObjectsResponse(reply,callback);
				//persistResponse(data);
				//res.jsonp(self.response);
				//callback(null,self.response);
		  	}
	  });
	}catch(err) {
		if(callback)callback(err);
	}
 };


FacebookCollector.prototype.fetchMetricsFromSource = function(entityname,callback) {
  var self=this;
  //fetch one day metric
  var until = Math.floor(Date.now()/1000);
  var since = until - (30*86400);
  var url = entityname+'/insights';

	try {
  //self.reader.get(url,{since: since,until:until,period:"day",limit:5000}, function(err, data) {
		  	self.reader.api(url,{since: since,until:until,period:"day",limit:5000}, function(reply) {
			  if(!reply || reply.error) {

			  		console.log(TAG,'fetchMetricsFromSource:',!reply ? 'error occurred' : reply.error);
			  		if(callback)callback('Error in fetching metrics',null);

			  	} else {
			  		console.log(TAG,"fetchMetricsFromSource:",reply);
					self.parseAndPersistResponse(reply.data,callback);
					//persistResponse(data);
					//res.jsonp(self.response);
					//callback(null,self.response);
			  	}
		  });
	}catch(err) {
		if(callback)callback(err);
	}

 };

 var buildUrls = function(entityname,metricname) {
 	var baseUrl = entityname+'/insights/';
 	var urllist = [];

 	var keys = _.keys(urlMap[metricname]);
 	_.each(keys,function(key){
 		var endUrl = baseUrl + key;
 		urllist.push(endUrl);

 	});
 	return urllist;
 };

 var buildBatchObj = function(urllist,filter) {

 	var batchObj = {
 		batch:[]
 	}

 	_.each(urllist,function(url){

 		var urlObj = {
 			method:'get',
 			relative_url:url+filter
 		}
 		batchObj.batch.push(urlObj);
 	});
 	return batchObj;
 };

 FacebookCollector.prototype.fetchMetricsFromSourceBatch = function(entityname,metricname,callback) {
  var self=this;
  //fetch one day metric
  var until = Math.floor(Date.now()/1000);
  var since = until - (30*86400);
  var filter = '?limit=5000&period=day&since='+since+'&until='+until;
	try {
	  var urllist = buildUrls(entityname,metricname);

	  var batchObj = buildBatchObj(urllist,filter);

	  //self.reader.get(url,{since: since,until:until,period:"day",limit:5000}, function(err, data) {
	  	/*
	  	batch: [
	  			{method: 'get',relative_url: url+'/page_impressions_unique'},
	  			{method: 'get',relative_url: url+'/page_views'},
	  			{method: 'get',relative_url: url+'/page_engaged_users'},
	  			{method: 'get',relative_url: url+'/page_consumptions'},
	  			{method: 'get',relative_url: url+'/page_fans'}
	  		]*/
	  	self.reader.api('','post',batchObj,function(res) {

			var isError=false;
			var data = [];
		    if(!res || res.error) {
		        console.log(TAG,'fetchMetricsFromSourceBatch:',!res ? 'error occurred' : res.error);
		        isError = true;
		    }else {
				_.each(res,function(resOne) {
					resItem = JSON.parse(resOne.body);
					if(!resItem || resItem.error) {
						console.log(TAG,'fetchMetricsFromSourceBatch:',!resItem ? 'error occurred' : resItem.error)
					}else {
						data = data.concat(resItem.data);
					}

				});

				/*
			    res0 = JSON.parse(res[0].body);
			    res1 = JSON.parse(res[1].body);
			    res2 = JSON.parse(res[2].body);
			    res3 = JSON.parse(res[3].body);
			    res4 = JSON.parse(res[4].body);

			    console.log(res0,res1,res2,res3,res4)
			    */
		    }

		    if(isError) {
		    	if(callback)callback("Error in fetching from facebook",null);
		  	} else {
		  		console.log(TAG,"fetchMetricsFromSourceBatch:");
				self.parseAndPersistResponse(data,metricname,callback);
				//persistResponse(data);
				//res.jsonp(self.response);
				//callback(null,self.response);
		  	}
	  });
	}catch(err) {
		if(callback)callback(err);
	}
 };

FacebookCollector.prototype.fetchMentionsFromSource = function(entityname,callback) {
  var self=this;
	/*
  var searchOptions = {
    q:     keyword,
    type:  "post",
    fields: "id,from,message,picture,link,name,caption,description,icon,privacy,type,created_time,updated_time,shares"
  };
	*/
  var url =entityname + '/posts';
	var type = 'get';
  //var searchOptions = {
	//	fields: "id,from,message,picture,link,name,caption,description,icon,privacy,type,created_time,updated_time,shares"
//	};
  //self.reader.get(searchOptions, function(err, reply) {
	try {
  	self.reader.api(url,type, function(reply) {
  		 if(!reply || reply.error) {

	  		console.log(TAG,'fetchMentionsFromSource:',!reply ? 'error occurred' : reply.error);
	  		callback("Error in fetching from facebook",null);
	  	} else {
	  		console.log(TAG,"fetchMentionsFromSource:",reply);
			self.parseAndPersistMentionsResponse(reply,callback);

	  	}
  	});
	}catch(err) {
		if(callback)callback(err);
	}



};

module.exports = FacebookCollector;
