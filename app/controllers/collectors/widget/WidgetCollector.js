
/*
var TwitterCollector = require('./TwitterCollector');
var YoutubeCollector = require('./YoutubeCollector');
var RSSCollector = require('./RSSCollector');
var FacebookCollector = require('./FacebookCollector');
*/
var mongoose = require('mongoose'),
    Widget = mongoose.model('Widget'),
	_ = require('underscore');
var schedule = require('node-schedule');

var EndpointCollector = require('./EndpointCollector.js');


var Logger = require('../../logger/logger');
var logger = new Logger;
var endpointCollector = new EndpointCollector;
//var jobMap = {};
var TAG="WidgetCollector:";

var WidgetCollector = function() {


//TODO: use widget.interval to schedule the job
    	//TODO: Schedule the jobs during server initialization
    	//TODO: Cancel the job once widget is removed
    	//TODO: Store the job in a Map
    	//TODO: Check the usage fo agenda

  return {
    scheduleJob: function(widget,account,immediate) {
      try {
    	console.log(TAG,'scheduleJob:');
    	var widgetClone = _(widget).clone();
    	if(immediate) {
    	//	schedule.scheduleJob(Date.now, function() {
    		setTimeout(function() {

    			endpointCollector.collect(widgetClone,account);
    		},1000);
    		//});
    	}

    	var rule = new schedule.RecurrenceRule();
	    rule.hour = new schedule.Range(0,24,widgetClone.config.interval);
	 //   rule.hour = widget.interval;
	 //	rule.hour = widget.interval;
		rule.minute = 59;

    	var job = schedule.scheduleJob(rule, function() {
    		console.log(TAG,'scheduleJob:',widgetClone);
    		endpointCollector.collect(widgetClone,account);
	//  	addToJobMap(job);
    	});
    }catch(err){
      console.log(TAG,err);
    }


    },
    fetchRealTime: function(widget,account,res) {

    	console.log(TAG,'fecthRealTime:');
      try {
  		  var widgetClone = _(widget).clone();
      	endpointCollector.collectReal(widgetClone,res,"data",account);

    	}catch(err) {
	console.log(err);
        res.send('404','Error in fetching data');
      }

    },
    fetchObjects: function(widget,account, res) {

    	console.log(TAG,'fetchObjects:',account);
      try {
		      var widgetClone = _(widget).clone();

    	   endpointCollector.collectReal(widgetClone,res,"objects",account);
      }catch(err) {
        res.send('404','Error in fetching data');
      }



    }


  };
};

var init = function() {

	Widget.find({},function(err, widgets) {
		if(err) {

		}else {
			console.log(TAG,'init:');
			_.each(widgets,function(widget) {
			  try {
				var widgetClone = _(widget).clone();
				if(widgetClone.config.storage.selected === 'Passthru' || !widgetClone.config.status ) {
					//do nothing
				} else {
					//console.log(widgetClone.config.status);
					//console.log(widgetClone.config.interval);
					var rule = new schedule.RecurrenceRule();

		   			rule.hour = new schedule.Range(0,24,widgetClone.config.interval);
		 			//   rule.hour = widget.interval;
		 			//rule.hour = 23;
					rule.minute = 59;
					//console.log("Adding Job", widget);
					//console.log(rule);
					var job = schedule.scheduleJob(rule, function() {
						console.log(TAG,'init:', widgetClone);
	    				endpointCollector.collect(widgetClone);
				//  	addToJobMap(job);
	    			});
	    		}
			}catch(err) {
				console.log(TAG,err);
			}
			});
		}
	})
}

//init();
module.exports = WidgetCollector;
