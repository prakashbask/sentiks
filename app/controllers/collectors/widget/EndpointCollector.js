






var Q = require('q');
var Logger = require('../../logger/logger');
var logger = new Logger;
var collectorMap = require('./CollectorMap');
var TAG='EndpointCollector:';


var EndpointCollector = function() {




  return {
    collect: function(widget,account) {

    	var collector = collectorMap.getCollector(widget.type);
    	collector.collectData(widget,account);

    },
	collectReal: function(widget,res,collectionType,account) {
		var queue = [];
		// A standard NodeJS function: a parameter and a callback; the callback
				// will return error (if any) as first parameter and result as second
				// parameter.
		function fetchData() {
				  // The 'deferred' object, base of the Promises proposal on CommonJS
				  var deferred = Q.defer();
					var collector = collectorMap.getCollector(widget.type);
				    //TODO factor the code here
				    if(collectionType === 'data') {
				    	console.log(TAG,"collectReal:fetchData:");
						  collector.collectDataQ(widget,account,function(err,response) {
						  		if(err) {
						  			deferred.reject(err);
						  		} else {
						  			deferred.resolve(response);
						  		}
						  });
					 }else if(collectionType === 'objects'){
					 	collector.collectObjectsQ(widget,account,function(err,response) {
						  		if(err) {
						  			deferred.reject(err);
						  		} else {
						  			deferred.resolve(response);
						  		}
						 });
					 }


				  return deferred.promise;
		}

		function populateCollectors() {

				queue.push(fetchData());




		    }


				populateCollectors();

				// Q.all: execute an array of 'promises' and 'then' call either a resolve
				// callback (fulfilled promises) or reject callback (rejected promises)
				var finalResponse=new Array();

				Q.all(queue).then(function(ful) {

				  // All the results from Q.all are on the argument as an array



				  ful.forEach(function(response) {
				  	 if (response){
				  	 	 if(Array.isArray(response)){
					  	 	finalResponse = finalResponse.concat(response);
					  	 }else {
					  	 	finalResponse.push(response);
					  	 }
				  	 }

				  });
          res.jsonp(finalResponse);


				}, function(rej) {

				  // The first rejected (error thrown) will be here only
				  console.log(TAG,'populateCollectors:rejected', rej,widget,account);
          res.send('404',rej);

				}).fail(function(err) {

				  // If something whent wrong, then we catch it here, usually when there is no
				  // rejected callback.
				  console.log(TAG,'populateCollectors:fail', err,widget,account);
          res.send('404',err);
				}).fin(function() {

				  // Finally statemen; executed no matter of the above results
				  console.log(TAG,'populateCollectors:finally');

				});

	}

  };
};

module.exports = EndpointCollector;
