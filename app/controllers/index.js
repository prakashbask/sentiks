/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('underscore');
    fs = require('fs');



var classifier = require('./classifier/classifier.js');

//var Adaptor = require('./collectors/Adaptor');
//var adaptor = new Adaptor;
var moment = require('moment');
var AccessChecker = require('./AccessChecker');
var accessChecker = new AccessChecker();

var TAG='Index:';





//facebook  app_id=595549690523774
//facebook app secret= c7da7e697f0e3bb0dc0ff1fd41f3ae09








exports.render = function(req, res) {
    res.render('index', {
        user: req.user ? JSON.stringify(req.user) : "null"
    });
};

/*

exports.fetchTwitter = function(req,res) {

	  adaptor.fetchTwitter(req.param('keyword'),classifier.getClassifier(),res);


};


exports.fetchYoutube = function(req,res) {
	adaptor.fetchYoutube(req.param('keyword'),classifier,res);
	//adaptor.fetchRSS(req.param('keyword'),classifier.getClassifier(),res);




};

exports.fetchAll = function(req,res) {
	var ipaddress = req.headers['x-forwarded-for'] ||
	    					    req.connection.remoteAddress ||
	     						req.socket.remoteAddress ||
	     						req.connection.socket.remoteAddress;
	var keyword = req.param('keyword');

	if(accessChecker.provideAccess(ipaddress,keyword,res)) {
		adaptor.fetchAll(keyword,ipaddress,res);
	} else {
		res.jsonp("Access is Denied");
	}


}

*/

exports.setStream = function(req,res) {
	var keyToMonitor = req.param('keyword');
	var action =  req.param('streamaction');
	var stream;
	if(action == 'start') {
		stream = twit.stream('statuses/filter', { track: keyToMonitor });

		stream.on('tweet', function (tweet) {
  	 	fs.appendFile(keyToMonitor + '.txt', tweet + '\n' , function (err) {
		  	if (err) return console.log(TAG,err);
		  	console.log(TAG);
		  });
		});
	}else if (action == 'stop') {
		stream.stop();
	}


};

exports.analyse = function(req,res) {
	var keyToAnalyse = req.param('keyword');

	var response = new Object();
	var sentObj = classifier.getClassifier().classify(keyToAnalyse);
	//var sentimentWords = classifiernaturalnew.classify(keyToAnalyse);
	response.sentimentNode = sentObj.sentiment;
	//response.sentimentNatural = sentimentWords;
	console.log(TAG,'key is' + keyToAnalyse);
	res.jsonp(response);
};

exports.updateSentiment = function(req,res) {
	var trainerAction = req.param('keyword');
	if(trainerAction == "update") {
		var keyToAnalyse = req.query.keyword;
		var action = req.query.action;
		var type = req.query.type;
		console.log (TAG,keyToAnalyse,action, type);
		if(type == 'text') {
			console.log(TAG,"Added to text classification");


			var subresult = keyToAnalyse.split(" ");
			var now = moment();

			var fileDate = now.year().toString()+now.month().toString()+now.date().toString();

			console.log(TAG,fileDate);
			if(subresult.length == 1) {
				console.log(TAG,"Added One word");
				//fs.appendFile('node_modules/node-sentiment/data/'+action + 'words.txt', keyToAnalyse + '\n' , function (err) {
				fs.appendFile(fileDate+action + 'words.txt', keyToAnalyse + '\n' , function (err) {
				  	if (err) return console.log(TAG,err);

				  });
				 // classifiernaturalnew.addDocument(keyToAnalyse, action);

			} else {
				console.log(TAG,"Added One Sentence");
				//fs.appendFile('node_modules/node-sentiment/data/'+action + '/6.txt', keyToAnalyse + '\n' , function (err) {
				fs.appendFile(fileDate+action + '.txt', keyToAnalyse + '\n' , function (err) {
				  	if (err) return console.log(TAG,err);
				  	console.log(TAG,"Written text");
				  });

			}
		}
			//TODO: add it to the sentiment module also and reload the module


	} else if(trainerAction == "train") {
		var keyToAnalyse = req.param('keyword');
		console.log(TAG,"Training the sentiment");

	/*
		var sentimentWords = classifiernaturalnew.train();

		var newRaw = JSON.stringify(classifiernaturalnew);
		fs.writeFile('classifiernaturalnew.json', newRaw, function (err) {
			  	if(err) {
		     		 console.log(err);
		  		 } else {
		      		console.log("classifier output saved ");
		    	}

		  });
		*/
		  classifier = new SentimentClassifier();



	}

		res.jsonp("success");

};
