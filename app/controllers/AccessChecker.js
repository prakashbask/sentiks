var mongoose = require('mongoose'),
denied = mongoose.model('DeniedLog');
var config = require('../../config/config');
var accessmap = {};

var AccessChecker = function() {

  
  return {

	fetchFromDB: function(res) {
	  
	  
	  log.find({}, function(err,logs) {
	  		
		  if (err) {
		  	return console.error(err);
		  } else {
			  
			  res.jsonp(logs);
			  
		  }
	  });
		 
		  
	},
	provideAccess:function(ipaddress,keyword,res) {
				
				var value = accessmap[ipaddress];
				
				if(value) {
										
					var currentTimeinSeconds = Date.now()/1000; //in seconds
					var diffInTime = currentTimeinSeconds - value.dateFirstAccessed;
					
					
					if (diffInTime < config.access.number.duration ) {
						if(value.count >	config.access.number.numberOfTimesDay) {
							console.log("Excceeded Count");
							var errStatus = config.access.number.errstatus;
							var reason = config.access.number.reason;
							res.status(errStatus);
							
							var logMessage = new denied({'ipAddress':ipaddress,'keyword':keyword, 'reason':errStatus});
				    		logMessage.save(function (err) {
					    		if(err) {
					    			console.log("Error in saving Log" + err);
					    		}
				        
				   		 	});
							return false;		
						} else {
							value.count +=1;
							
						}
					} else {
						//New day, restart the counting
						value.dateFirstAccessed = currentTimeinSeconds;
						value.count =1;				
					}
					return true;
					
				} else {
					var value = new Object();
					value.count =1;
					value.dateFirstAccessed = Date.now()/1000;		//in Seconds			
					accessmap[ipaddress]=value;
					
					return true;
				}
				
				
	
	  	   
	}

};

};
module.exports=AccessChecker;
