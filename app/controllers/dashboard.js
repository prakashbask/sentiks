/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Widget = require('./widget'),
	Widget = mongoose.model('Widget'),
    WidgetData = mongoose.model('WidgetData'),
    Dashboard = mongoose.model('Dashboard');
     _ = require('underscore');

var TAG="Dashboard:";


exports.dashboard = function(req, res, next, id) {
    Dashboard.load(id, function(err, dashboard) {
        if (err) return next(err);
        if (!dashboard) return next(new Error('Failed to load Dashboard ' + id));
        req.dashboard = dashboard;
        next();
    });
};
exports.createDashboard = function(req, res) {
    var dashboard = new Dashboard(req.body);
	dashboard.user = req.user._id;
    dashboard.save(function(err) {
        if (err) {
				res.send('404',err);
        }else {
		res.jsonp(dashboard);
	}
    });
}
exports.updateDashboard = function(req, res) {

    var dashboard = req.dashboard;
    console.log(TAG,req.dashboard);
    dashboard = _.extend(dashboard, req.body);

    dashboard.save(function(err) {
			if(err) {
				res.send('404',err);
			}else {
        res.jsonp(dashboard);
			}
    });

}

exports.removeDashboard = function(req, res) {
    var dashboard= req.dashboard;

    dashboard.remove(function(err) {
        if (err) {
            res.send('404',err);
        } else {
        	WidgetData.remove({user:dashboard.user,dashboardId:dashboard._id}).exec();
					Widget.remove({user:dashboard.user,dashboardId:dashboard._id}).exec();

            res.jsonp(dashboard);
        }
    });
    //ToDo remove widget and widget data also
};


exports.allDashboards = function(req, res) {

    Dashboard.find({user:req.user._id}).sort('-created').exec(function(err, dashboards) {
        if (err) {
						res.send('404',err);
        } else {
            res.jsonp(dashboards);
        }
    });
};

exports.findDashboard = function(req, res, next, id) {
    Dashboard.load(id, function(err, dashboard) {
        if (err) return next(err);
        if (!dashboard) return next(new Error('Failed to load Dashboard ' + id));
        req.dashboard = dashboard;
        next();
    });
};

exports.getDashboardData = function(req, res) {



        var userId = req.param('userId');
      Dashboard.find({user:userId}).sort({createdAt:1}).exec(function(err, dashboards) {
        if (err) {
        		res.send('404',err);
        } else {
        	if(dashboards) {
            	res.jsonp(dashboards);
           } else {
           		res.status(404);
           }
        }
    });
};
