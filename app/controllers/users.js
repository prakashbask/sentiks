/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Account = mongoose.model('Account');

var TAG="User:";
/**
 * Auth callback
 */
exports.authCallback = function(req, res, next) {
	//console.log("In AuthCallback",req.user,req.account,req.user.account);
	var account = req.user.account || req.account;
    //var account = req.account;

	//console.log("in callback user",req.user);
	//console.log("in callback account",req.account);
    // Associate the Twitter account with the logged-in user.
   // account.user = user.local.email;
   // account.save(function(err) {
     // if (err) { return self.error(err); }

    //});
	var urlparam = '?accountId='+account._id +'&name='+account.name+'&category='+account.category+'&token='+account.accessToken+'&tokenSecret='+account.accessTokenSecret+'&refreshToken='+account.refreshToken;
	var url = '/views/auth/oauth2callback.html'+urlparam;
    res.redirect(url);
   // res.redirect('/');
};
exports.updateAccount = function(req, res, next) {

	var userdetails = req.body;
	Account.update({_id: userdetails.accountId},{user:userdetails.userId}, function(err,affected) {
		if(err) {
			console.log(TAG,err);
  			res.status(400).send(err);
  		}else {
  			res.status(200);
  		}
	});

};
/**
 * Show login form
 */
exports.signin = function(req, res) {
    res.render('users/signin', {
        title: 'Signin',
        message: req.flash('error')
    });
};
exports.signinaccount = function(req, res) {
     if(req.isAuthenticated()) {
        return res.redirect('/');
    }
    res.redirect('#!/login');
};

/**
 * Show sign up form
 */
exports.signup = function(req, res) {
    res.render('users/signup', {
        title: 'Sign up',
        user: new User()
    });
};

/**
 * Logout
 */
exports.signout = function(req, res) {
    req.logout();
    res.redirect('/');
};

/**
 * Session
 */
exports.session = function(req, res) {
    res.redirect('/dashboard');
};

/**
 * Create user
 */
exports.create = function(req, res) {
	console.log(TAG,req.body);
   var user = new User(req.body);

    user.provider = 'local';

    // because we set our user.provider to local our models/user.js validation will always be true
   // req.assert('email', 'You must enter a valid email address').isEmail();
    //req.assert('password', 'Password must be between 8-20 characters long').len(8, 20);



    // Hard coded for now. Will address this with the user permissions system in v0.3.5
    user.roles = ['authenticated'];
    user.save(function(err) {
    	console.log(err);
        if (err) {
            switch (err.code) {
                case 11000:
                case 11001:
                    res.status(400).send('Email is already registered');
                    break;
                default:
                    res.status(400).send('Please fill all the required fields');
            }

            return res.status(400);
        }
        req.logIn(user, function(err) {
            if (err) return next(err);
            return res.redirect('/dashboard');
        });
        res.status(200);
    });
};

/**
 *  Show profile
 */
exports.show = function(req, res) {
    var user = req.profile;

    res.render('users/show', {
        title: user.name,
        user: user
    });
};

/**
 * Send User
 */
exports.me = function(req, res) {
    res.jsonp(req.user || null);
};

exports.allUsers = function(req, res) {

	 User.find().sort('-created').exec(function(err, users) {
        res.jsonp(users);

    });

};



/**
 * Find user by id
 */
exports.user = function(req, res, next, id) {
	console.log(TAG,id);
    User
        .findOne({
            _id: id
        })
        .exec(function(err, user) {
            if (err) return next(err);
            if (!user) return next(new Error('Failed to load User ' + id));
            req.profile = user;
            next();
        });
};
