
var dropbox = require('./dropbox/dropbox-datastores.js');
var config = require('../../../config/config.js');
var _ = require('underscore');
var mongoose = require('mongoose'),
    WidgetData = mongoose.model('WidgetData');
var CloudDatastore = function(options){
    this.cloudstore  = new dropbox.Client
		({
    		key:config.dropbox.clientID,
    		/*secret:'apisecret',
    		uid:options.did,*/
    		token:options.accessToken
    		
		}).getDatastoreManager();

	console.log(this.cloudstore);
	
}

CloudDatastore.prototype.save = function(widgetData,done) {
	var self  = this;
	console.log("Save Datastore",widgetData);
	self.cloudstore.openDefaultDatastore(function(err,datastore) {
		if(err) {
			console.log("Error in creating datastore for widget",err);
			done(err);
		}else {
			var currentTime = Date.now();
			//console.log(widgetData.widgetId);
			
			var dataTable = datastore.getTable(widgetData.widgetId);
			
			var keys = _.keys(widgetData.data);
			
			_.each(widgetData.data, function(val,key){
					 dataTable.insert({name:key,value:val,createdAt:currentTime});
					
				});
		
			
			/*
			dataTable.insert({data:widgetData.data,createdAt:currentTime});
			*/
			done(null,"success");
		}
	});
	
	
    
};

CloudDatastore.prototype.get = function(widget,done) {
	var self  = this;
	console.log("Get Datastore",widget);
	self.cloudstore.openDefaultDatastore(function(err,datastore) {
		if(err) {
			console.log("Error in opening datastore for widget",err);
			done(err);
		}else {
			var dataTable = datastore.getTable(new String(widget._id));
			
			var recordMap = {};
			var recordlist = dataTable.query();
			//console.log("Got from Datastore",recordlist);
			_.each(recordlist,function(record){
				//console.log("Record is",record);
				var createdAt = record.get('createdAt');
				if(! recordMap[createdAt]) {
					recordMap[createdAt] = {};
				}
				recordMap[createdAt][record.get('name')]=record.get('value');
				
				//console.log("Record Map",recordMap);

			});
			console.log("Record Map",recordMap);
			var keys = _.keys(recordMap);
			var sortedKeys = _.sortBy(keys,function(key){return key});
			var widgetdatalist = [];
			_.each(sortedKeys,function(key) {
			
				 
				var widgetrecord= new WidgetData( {
					user: widget.user,
		  			dashboardId: widget.dashboardId,
		  			widgetId: '1',
		  			data : recordMap[key],
		  			createdAt:key
				});
				widgetdatalist.push(widgetrecord);
			});
			/*
			var recordlist = dataTable.query();
			var widgetdatalist = [];
			_.each(recordlist,function(record){
				var createdAt = record.get('createdAt');
				var data = record.get('data');
				var widgetrecord= new WidgetData( {
					user: widget.user,
		  			dashboardId: widget.dashboardId,
		  			widgetId: widget._id,
		  			data : data,
		  			createdAt:createdAt
				});
				widgetdatalist.push(widgetrecord);
				
			});
			*/
			done(null,widgetdatalist);
		}
	});
	
	
    
};
CloudDatastore.prototype.close = function(done) {
	
	var self = this;
	self.cloudstore.close();
	
};


module.exports = CloudDatastore;
