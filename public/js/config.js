
//Setting HTML5 Location Mode
angular.module('mean').config(['$locationProvider',
    function($locationProvider) {
        //$locationProvider.hashPrefix("!");
        $locationProvider.html5Mode(true);
    	$locationProvider.hashPrefix('!');
    }
]);
angular.module('mean').config(['$compileProvider',
	function($compileProvider) { 
		$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|blob|mailto|file|chrome-extension):/); 
	 }
]);

/*
myApp.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]);*/