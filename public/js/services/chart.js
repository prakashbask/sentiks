angular.module('mean.system').factory("ChartFactory", [
    function() {
    	
    	var Chart = function(){
    		
    		
    		var me = {};
    		
    		var svg;
    		var nvchart;
    		me.update = function() {
    			
    		};
    		me.redraw = function() {
    			
    		}
    		
    		me.destroy = function() {
    			if(svg) { 
		       	 svg.remove();
		         svg=null;
		       };
		        if(nvchart){
			       	if(nvchart.interactiveLayer)nvchart.interactiveLayer.tooltip.chartContainer(null);
			       	nvchart.container=null;
			       	console.log("NVChart",nvchart.that);
			       	nvchart=null;
		       };
    		};
    		
    		me.initLine = function(data,target) {
    			console.log("In Chart Line",data);
    			console.log("Target",target)
    			
				me.destroy();
				nv.addGraph(function() {
								 // if(nvchart)nvchart.remove();
					  nvchart = nv.models.lineChart()
					               .margin({left: 60	,right:10})  //Adjust chart margins to give the x-axis some breathing room.
					               .useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
					               .transitionDuration(350)  //how fast do you want the lines to transition?
					               .showLegend(true)       //Show the legend, allowing users to turn on/off line series.
					               .showYAxis(true)        //Show the y-axis
					               .showXAxis(true)        //Show the x-axis
								  ;
						  
					  var timescale = d3.time.scale()
           							 .domain([data.min_date, data.max_date]);
           			  nvchart.xScale(timescale);
					  nvchart.xAxis.tickFormat(function(d) {
							        return d3.time.format('%b %d')(new Date(d))
					  });
			
								
					 nvchart.yAxis.tickFormat(d3.format(',f'));
	 			     d3.select(target).append("svg")  
								      .datum(data.list)         //Populate the <svg> element with chart data...
								      .call(nvchart);          //Finally, render the chart!
								
								  //Update the chart when window resizes.
					 nv.utils.windowResize(function() { if(nvchart)nvchart.update() });
								   //svg = d3.select(elem[0].children[1].firstElementChild.firstElementChild);
					 svg = d3.select(target.firstElementChild); 
					 return nvchart;
				});
    		};
    		
    		me.initBar = function(data,target) {
    			console.log("In Chart Bar",data);
    			console.log("Target",target)
    				
				me.destroy();
				nv.addGraph(function() {
							//if(nvchart)nvchart.remove();
						      nvchart = nv.models.multiBarChart()
						      .transitionDuration(100)
						      .reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
						      .rotateLabels(0)      //Angle to rotate x-axis labels.
						      .showControls(true)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
						      .groupSpacing(0.1)    //Distance between each group of bars.
						    ;
						    	console.log(nvchart.that);	
							// var timescale = d3.time.scale()
           					//		 .domain([data.min_date, data.max_date]);
           					//	 nvchart.xScale(timescale);
						    nvchart.xAxis.tickFormat(function(d) {
							        
							        return d3.time.format('%b %d')(new Date(d))
							});
							nvchart.yAxis
			          				.tickFormat(d3.format(',f'));
						
						    d3.select(target).append("svg") 
						        .datum(data.list)
						        .call(nvchart);
						
						    nv.utils.windowResize(function() { 
						    	if(nvchart) nvchart.update()
						     });
							svg = d3.select(target.firstElementChild); 
						    return nvchart;
						});
    		};
    		
    		me.initMap = function(data) {
    			
    		};
    		me.initPie = function(data)  {
    			
    		};
    		
    		
    		return me;
    		
    	};
    	
    	
		return {
			chart : Chart
		};
    }
]);

