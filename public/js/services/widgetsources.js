angular.module('mean.system')
  .factory('WidgetSources', function () {


  	var widgetsources = {};
  	widgetsources.sourceslist = [
  		{name:'Sentiment',id:'search'},
  		{name:'Social Media',id:'sm'},
  		{name:'Web Analytics',id:'wa'},
  		{name:'Advertising',id:'ad'},
  		{name:'Email',id:'email'},
  		{name:'Blogs',id:'blogs'},
  		{name:'Custom',id:'custom'},
  		{name:'Monitoring',id:'monitoring'},
  		{name:'Sales',id:'sales'},
  		{name:'Support',id:'support'},

  	];
    widgetsources.sources =  [
		     {
			  category:'sm',
				sources : [
				{
						type:'twitter',
						attr: {
							icon:'fa-twitter',
							hue:'twitterC',
							title:'Twitter',
							desc:'Tweets, Followers, Listed, Favourites'
						},
						supported:true

				},
				{
					type:'facebook',
					attr: {
						icon:'fa-facebook',
						hue:'facebookC',
						title:'Facebook Pages',
						desc:'Likes, Talking, Views'
					},
					supported:true
				},
				{
					type:'linkedin',
					attr: {
						icon:'fa-linkedin',
						hue:'linkedinC',
						title:'LinkedIn',
						desc:'Connections, Recommendations, Profile Views'
					},
					supported:true

				},
				{
					type:'youtube',
					attr: {
						icon:'fa-youtube-play',
						hue:'youtubeC',
						title:'Youtube',
						desc:'Views, Comments, Favorites, Likes, Dislikes'
					},
          supported:true

				},
				{
					type:'googleplus',
					attr: {
						icon:'fa-google-plus',
						hue:'googleplusC',
						title:'GooglePlus',
						desc:'Pluses'
					}

				},
				{
					type:'flickr',
					attr: {
						icon:'fa-cloud',
						hue:'linkedinC',
						title:'Flickr',
						desc:'Slideshows, Photos'
					}

				},
				{
					type:'instagram',
					attr: {
						icon:'fa-cloud',
						hue:'linkedinC',
						title:'Instagram',
						desc:'Photos, Followers, Following'
					}

				},
				{
					type:'klout',
					attr: {
						icon:'fa-cloud',
						hue:'linkedinC',
						title:'Klout',
						desc:'Score'
					}

				},
				{
					type:'pinterest',
					attr: {
						icon:'fa-cloud',
						hue:'linkedinC',
						title:'Pinterest',
						desc:'Boards, Pins, Followers, Following, Likes'
					}

				},
				{
					type:'vimeo',
					attr: {
						icon:'fa-cloud',
						hue:'linkedinC',
						title:'Vimeo',
						desc:'Top Videos, Feed'
					}

				}

			]


		},
		{
			category:'wa',
			sources : [
				{
						type:'alexa',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'Alexa',
							desc:'Pageviews, Reach, Traffic Rank'
						}

				},
				{
						type:'googleanalytics',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'Google Analytics',
							desc:'Content, Conversions, Traffic, Audience'
						}

				},
				{
						type:'chartbeat',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'Chartbeat',
							desc:'Visits, Returning Visits, New Visits'
						}

				},
				{
						type:'gosquared',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'GoSquared',
							desc:'Pages, Returning Visitors'
						}

				}


			]

		},
		{
			category:'ad',
			sources : [
				{
						type:'adsense',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'Google AdSense',
							desc:'Pageviews, Clicks, RPM, CTR, CPC'
						}

				},
				{
						type:'adwords',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'Google AdWords',
							desc:'Impressions, Clicks, Cost'
						}

				},
				{
						type:'adwordsmcc',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'Google AdWords (MCC)',
							desc:'Impressions, Clicks, Cost, Conversions'
						}

				},
				{
						type:'doubleclick',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'Google Doubleclick',
							desc:'Impressions, Clicks, Revenue'
						}

				}
			]

		},
		{
			category:'email',
			sources : [
				{
						type:'aweber',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'AWeber',
							desc:'Clicks, Opens, Sent, Spam'
						}

				},{
						type:'cmonitor',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'Campaign Monitor',
							desc:'Clicks, Opens, Sent, Spam'
						}

				},
				{
						type:'mailchimp',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'MailChimp',
							desc:'Clicks, Opens, Sent, Spam'
						}

				}
			]

		},
		{
			category:'search',
			sources : [
				{
						type:'twitter-search',
						attr: {
							icon:'fa-twitter',
							hue:'twitterC',
							title:'Twitter',
							desc:'Sentiment, Emotions, Trends, Metrics'
						},
						supported:true

				},
				{
						type:'facebook-search',
						attr: {
							icon:'fa-facebook',
							hue:'facebookC',
							title:'Facebook Comments',
							desc:'Sentiment, Emotions, Trends, Metrics'
						},
            supported:true


				},
				{
						type:'youtube-search',
						attr: {
							icon:'fa-youtube-play',
							hue:'youtubeC',
							title:'Youtube Videos',
							desc:'Sentiment, Emotions, Trends, Metrics'
						},
            supported:true


				}
			]

		},
		{
			category:'blogs',
			sources : [
				{
						type:'wordpress',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'WordPress',
							desc:'Posts, Comments, Users'
						}

				}
			]

		},
		{
			category:'custom',
			sources : [
				{
						type:'gsheet',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'Google Spreadsheet',
							desc:'Line, Bar, Pie, Guage, Table, List, Funnel'
						}

				},
				{
						type:'csv',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'CSV',
							desc:'Line, Bar, Pie, Guage, Table, List, Funnel'
						}

				}
			]

		},
		{
			category:'monitoring',
			sources : [
				{
						type:'galerts',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'Google Alerts',
							desc:'Feeds'
						}

				},
				{
						type:'gtrends',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'Google Trends',
							desc:'Trends'
						}

				},
				{
						type:'aws',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'Amazon Web Services',
							desc:'EC2, RDS, ELB'
						}

				}
			]

		},
		{
			category:'sales',
			sources : [
				{
						type:'infusionsoft',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'InfusionSoft',
							desc:'Contacts, Opportunities, Sales'
						}

				},
				{
						type:'paypal',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'PayPal',
							desc:'Activity, Balance'
						}

				},
				{
						type:'salesforce',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'Salesforce',
							desc:'Leads, Accounts, Contacts, Opportunities, Sales'
						}

				}
			]

		},
		{
			category:'support',
			sources : [
				{
						type:'zendesk',
						attr: {
							icon:'fa-cloud',
							hue:'twitterC',
							title:'ZenDesk',
							desc:'Tickets'
						}

				}
			]

		}


    ];

    return widgetsources;
  });
