angular.module('mean.system')
   .factory('RestTimeSeriesDataModel', function ( WidgetDataModel) {
    function RestTimeSeriesDataModel() {
    }

    RestTimeSeriesDataModel.prototype = Object.create(WidgetDataModel.prototype);

    RestTimeSeriesDataModel.prototype.init = function () {
    	console.log("Inited Data Model");
      WidgetDataModel.prototype.init.call(this);

      var params = this.dataModelOptions ? this.dataModelOptions.params : {};
      
      //call resource object on the widget 
      var chart = {
          state:"init"
      };
      WidgetDataModel.prototype.updateScope.call(this, chart);
    
    };
  
    
     RestTimeSeriesDataModel.prototype.updateConfig = function (data) {
      

     
         
            var wr = this.widgetScope.widget.wr;
            //wr.config = data.config;
            //wr.title = data.title;
            //wr.custom=data.custom;
            data.state="update";
            angular.extend(wr,data);
	        wr.$update(function(response) {
	            //$location.path('contact/' + response._id);
	            console.log("Updated successfully");
		        },function(err){
	        	
	        		wr.config.status=false;
	        		this.widgetScope.widget.config.status=false;
	        		
            	}
            	
		     );
		        
	        	 
	        
	     

        WidgetDataModel.prototype.updateScope.call(this, data);
     
    };
     
    return RestTimeSeriesDataModel;
  });