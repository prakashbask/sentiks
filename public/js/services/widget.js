
angular.module('mean.system')
  .factory('DashboardState', ['WidgetModel', function (WidgetModel) {
    function DashboardState(useLocalStorage, widgetDefinitions) {
      this.useLocalStorage = !!useLocalStorage;
      this.widgetDefinitions = widgetDefinitions;
    }

    DashboardState.prototype = {
      // Takes array of widgets, serializes, and saves state.
      // (currently stored in localStorage)
      save: function (seldashboard,widgets) {
        console.log('saving dash state');
        /*
        if (!this.useLocalStorage) {
          return true;
        }
        */
       
       var objectToSave = {
       		seldashboardId: seldashboard._id,
       		widgets:{}
       	
       }
       
       var key = seldashboard.user;
       //localStorage.setItem('selectedDashboard',JSON.stringify(dashObject));
        localStorage.removeItem(key);
        _.map(widgets, function (widget) {
	          
	          var widgetObject = {
	            title: widget.title,
	            type: widget.type,
	            dimensions: angular.copy(widget.dimensions)
	            	            
	          };
	          
		   	  objectToSave.widgets[widget.wr._id]=widgetObject;
          	  
        });
        var serialized = JSON.stringify(objectToSave);
        localStorage.setItem(key, serialized);
        return true;
      },
	  get: function(key) {
	  	
	  	var serialized,deserialized;
	  	 if (!(serialized = localStorage.getItem(key))) {
          return null;
        }

        try { // to deserialize the string
          deserialized = JSON.parse(serialized);
        } catch (e) {
          // bad JSON, clear localStorage
          localStorage.removeItem(key);
          return null;
        }

		return deserialized;
	  	
	  },
	  loadState: function(key) {
	  	var serialized,deserialized;
	  	 if (!(serialized = localStorage.getItem(key))) {
          return null;
        }

        try { // to deserialize the string
          deserialized = JSON.parse(serialized);
        } catch (e) {
          // bad JSON, clear localStorage
          localStorage.removeItem(key);
          return null;
        }

		return deserialized;
	  	
	  },
	  
      // Returns array of instantiated widget objects
      load: function (key) {
        if (!this.useLocalStorage) {
          return null;
        }

        var serialized, deserialized, result = [];
        key = key || 'default';

        // try loading localStorage item
        if (!(serialized = localStorage.getItem('widgets.' + key))) {
          return null;
        }

        try { // to deserialize the string
          deserialized = JSON.parse(serialized);
        } catch (e) {
          // bad JSON, clear localStorage
          localStorage.removeItem('widgets.' + key);
          return null;
        }

        // instantiate widgets from stored data
        for (var i = 0; i < deserialized.length; i++) {

          // deserialized object
          var widgetObject = deserialized[i];
          // widget definition to use
          var widgetDefinition = false;

          // find definition with same name
          for (var k = this.widgetDefinitions.length - 1; k >= 0; k--) {
            var def = this.widgetDefinitions[k];
            if (def.type === widgetObject.type) {
              widgetDefinition = def;
              break;
            }
          }

          // check for no widget
          if (!widgetDefinition) {
            // no widget definition found, remove and return false
            localStorage.removeItem('widgets.' + key);
            return null;
          }

          // push instantiated widget to result array
          result.push(new WidgetModel(widgetDefinition, widgetObject));
        }

        return result;
      }
    };
    return DashboardState;
  }]);


angular.module('mean.system')
  .factory('WidgetDataModel', function () {
    function WidgetDataModel() {
    }

    WidgetDataModel.prototype = {
      setup: function (widget, scope) {
        this.dataAttrName = widget.dataAttrName;
        this.dataModelOptions = widget.dataModelOptions;
        this.widgetScope = scope;
      },

      updateScope: function (data) {
        this.widgetScope.widgetData = data;
        angular.extend(this.widgetScope.widget,data);
      },

      init: function () {
        // to be overridden by subclasses
      },

      destroy: function () {
        // to be overridden by subclasses
      }
    };

    return WidgetDataModel;
  });

angular.module('mean.system')
  .factory('WidgetDefCollection', function () {
    function WidgetDefCollection(widgetDefs) {
      this.push.apply(this, widgetDefs);

      // build (name -> widget definition) map for widget lookup by name
      var map = {};
      _.each(widgetDefs, function (widgetDef) {
      	
      	//angular.extend(widgetDef,widgetDefs.defaultconf);
        map[widgetDef.type] = widgetDef;
      });
      this.map = map;
    }

    WidgetDefCollection.prototype = Object.create(Array.prototype);

    WidgetDefCollection.prototype.getByType = function (type) {
      return this.map[type];
    };

    return WidgetDefCollection;
  });


angular.module('mean.system')
  .factory('WidgetModel', function () {
    // constructor for widget model instances
    function WidgetModel(Class, overrides) {
      overrides = overrides || {};
      angular.extend(this, {
        title: Class.title || 'Widget',
        type: Class.type,
        attrs: Class.attrs,
        editModalOptions:Class.editModalOptions,
        dataAttrName: Class.dataAttrName,
        dataTypes: Class.dataTypes,
        dataModelType: Class.dataModelType,
        dataModelOptions: Class.dataModelOptions,
        dimensions:Class.dimensions,
        config:Class.config,
        style: Class.style
      }, overrides);
      this.style = this.style || { width: '33%' };
      this.setWidth(this.style.width);

      if (Class.templateUrl) {
        this.templateUrl = Class.templateUrl;
      } else if (Class.template) {
        this.template = Class.template;
      } else {
        var directive = Class.directive || Class.type;
        this.directive = directive;
      }
    }

    WidgetModel.prototype = {

      // sets the width (and widthUnits)
      setWidth: function (width, units) {
        width = width.toString();
        units = units || width.replace(/^[-\.\d]+/, '') || '%';
        this.widthUnits = units;
        width = parseFloat(width);

        if (width < 0) {
          return false;
        }

        if (units === '%') {
          width = Math.min(100, width);
          width = Math.max(0, width);
        }
        this.style.width = width + '' + units;
        return true;
      }

    };

    return WidgetModel;
  });