angular.module('mean.system')
  .factory('WidgetDefs', function (RestTimeSeriesDataModel) {
    return [
      {
			type:'twitter',
			user:"default",
			title:'Twitter',
			directive:'metrics',
			dataAttrName: 'chart',

			dataModelType: RestTimeSeriesDataModel,
			dataModelOptions: {
         		 params:'widget/id=10' //This can be removed as it is not used.
       		},
       		editModalOptions: {
				templateUrl: 'views/widgets/configure/configure.html'

			},
			config: {
				status:false,
				accounts : { list: [],selected :''},
				chartType:{ list: ['Line','Bar'],selected:'Bar'},
				storage: { list:['Passthru'],selected:'Passthru'},
				interval:'12',
				custom: {
					username:''
				},
				metrics:{ list: ['All','Followers','Tweets','Listed','Favourites'], selected:'All'},
				storeoptions:['Dropbox']

			},
			dimensions: {
				sizeX: 2,
	       		sizeY:2,
	       		row:0,
	       		col:0
	       }

		},
		{
			type:'twitter-search',
			title:'Twitter Search',
			dataAttrName: 'chart',
			dataModelType: RestTimeSeriesDataModel,
			dataModelOptions: {
         		 params:'widget/id=10'
       		},
       		directive:'search',
       		editModalOptions: {
				templateUrl: 'views/widgets/configure/twitter-search.html'

			},
			config: {
				status:false,
				accounts : { list: [],selected :''},
				chartType:{ list: ['Line','Bar'],selected:'Bar'},
				storage: { list:['Passthru'],selected:'Passthru'},
				storeoptions:[],
				interval:'12',
				custom: {
					keyword:""
				}

			},
			dimensions: {
				sizeX: 2,
	       		sizeY:2,
	       		row:0,
	       		col:0
	       }

		},
		{
			type:'facebook',
			title:'Facebook Pages',
			dataAttrName: 'chart',
			dataModelType: RestTimeSeriesDataModel,
			directive:'metrics',
			dataModelOptions: {
         		 params:'widget/id=10'
       		},
       		editModalOptions: {
				templateUrl: 'views/widgets/configure/configure.html'

			},
			config: {
				status:false,
				accounts : { list: [],selected :''},
				chartType:{ list: ['Line','Bar'],selected:'Bar'},
				storage: { list:['Passthru'],selected:'Passthru'},
				storeoptions: [],
				interval:'12',
				custom: {
					username:"Default"
				},
				objects: {
					list:[],selected:''
				},
				metrics:{ list: ['Overview','Reach','Impressions','Likes','Views','Posts','Stories','Checkins','Clicks'], selected:'Overview'}
			},
			dimensions: {
				sizeX: 2,
	       		sizeY:2,
	       		row:0,
	       		col:0
	       }

		},
    {
      type:'youtube',
      title:'Youtube Videos',
      dataAttrName: 'chart',
      dataModelType: RestTimeSeriesDataModel,
      directive:'metrics',
      dataModelOptions: {
              params:'widget/id=10'
           },
           editModalOptions: {
        templateUrl: 'views/widgets/configure/configure.html'

      },
      config: {
        status:false,
        accounts : { list: [],selected :''},
        chartType:{ list: ['Line','Bar'],selected:'Bar'},
        storage: { list:['Passthru'],selected:'Passthru'},
        storeoptions: [],
        interval:'12',
        custom: {
          videos: {
            list:[],selected:''
          }
        },
        objects: {
          list:[],selected:''
        },
        metrics:{ list: ['Overview'], selected:'Overview'}
      },
      dimensions: {
        sizeX: 2,
             sizeY:2,
             row:0,
             col:0
         }

    },
    {
      type:'facebook-search',
      title:'Facebook Comments',
      dataAttrName: 'chart',
      dataModelType: RestTimeSeriesDataModel,
      dataModelOptions: {
              params:'widget/id=10'
           },
           directive:'search',
           editModalOptions: {
        templateUrl: 'views/widgets/configure/facebook-search.html'

      },
      config: {
        status:false,
        accounts : { list: [],selected :''},
        chartType:{ list: ['Line','Bar'],selected:'Bar'},
        storage: { list:['Passthru'],selected:'Passthru'},
        storeoptions:[],
        interval:'12',
        objects: {
          list:[],selected:''
        },

      },
      dimensions: {
        sizeX: 2,
             sizeY:2,
             row:0,
             col:0
         }

    },
    {
      type:'youtube-search',
      title:'Youtube Comments',
      dataAttrName: 'chart',
      dataModelType: RestTimeSeriesDataModel,
      dataModelOptions: {
              params:'widget/id=10'
           },
           directive:'search',
           editModalOptions: {
        templateUrl: 'views/widgets/configure/youtube-search.html'

      },
      config: {
        status:false,
        accounts : { list: [],selected :''},
        chartType:{ list: ['Line','Bar'],selected:'Bar'},
        storage: { list:['Passthru'],selected:'Passthru'},
        storeoptions:[],
        interval:'12',
        objects: {
          list:[],selected:''
        },

      },
      dimensions: {
        sizeX: 2,
             sizeY:2,
             row:0,
             col:0
         }

    },
		{
			type:'linkedin',
			user:"default",
			title:'LinkedIn',
			directive:'metrics',
			dataAttrName: 'chart',

			dataModelType: RestTimeSeriesDataModel,
			dataModelOptions: {
         		 params:'widget/id=10' //This can be removed as it is not used.
       		},
       		editModalOptions: {
				templateUrl: 'views/widgets/configure/configure.html'

			},
			config: {
				status:false,
				accounts : { list: [],selected :''},
				chartType:{ list: ['Line','Bar'],selected:'Bar'},
				storage: { list:['Passthru'],selected:'Passthru'},
				interval:'12',
				custom: {
					username:''
				},
				metrics:{ list: ['All','Connections','Recommendations','ProfileViews'], selected:'All'},
				storeoptions:['Dropbox']

			},
			dimensions: {
				sizeX: 2,
	       		sizeY:2,
	       		row:0,
	       		col:0
	       }

		}

    ];
  });
