//Global service for global variables
angular.module('mean.system').factory("Global", [
    function() {
       return {
       	
       		data: {
       			user: {},
       			authenticated: false,
       			accounts:{},
       			storageaccounts:[]
       		}
       }
    }
]);
angular.module('mean.system').factory("Shared", [
    function() {
		return {
			control : {
				showHeader : true 
			}
		};
    }
]);
angular.module('mean.system').factory("Product", ['$resource', function($resource) {
    return $resource('products/:channel/:keyword', {
       	keyword:	'@_keyword'
    }, {
        update: {
            method: 'PUT'
        },
  		save: {
  			method: 'POST'
  		},
	   findAll: {
			method: 'GET',isArray:false
	     
	   },
	   findYoutubeProducts: {
			method: 'GET', isArray:false
	     
	   },
	   findTwitterProducts: {
			method: 'GET', isArray:false
	     
	   },
	   analyse: {
	   		method: 'GET', isArray:false
	   }

	});
}]);

angular.module('mean.system').factory("Analyser", ['$resource', function($resource) {
    return $resource('analyse/:keyword', {
       	keyword:	'@_keyword',
      
       	
    }, {
        update: {
            method: 'PUT'
        },
  	   analyse: {
	   		method: 'GET',isArray:false
	   }

	});
}]);
angular.module('mean.system').factory("Trainer", ['$resource', function($resource) {
    return $resource('trainer/:analyseAction', {
       	keyword:	'@_keyword',
       	action: '@_action',
       	type: '@_type'
       	
    }, {
        update: {
            method: 'PUT'
        },
  	   updateSentiment: {
	   		method: 'GET',params: {analyseAction:"update"}, isArray:false
	   },
	   trainSentiment: {
	   		method: 'GET',params: {analyseAction:"train"}, isArray:false
	   }

	});
}]);

angular.module('mean.system').factory("Contact", ['$resource', function($resource) {
    return $resource('contact/:contactId', {
       	contactId:	'@_id'
      
       	
    }, {
        update: {
            method: 'PUT'
        }
  	   

	});
}]);

angular.module('mean.system').factory("WidgetResource", ['$resource', function($resource) {
    return $resource('widgets/:widgetId', {
       	widgetId:	'@_id'
      
       	
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            method: 'GET',
            params: {dashboardId:'@dashboardId'},
            isArray: true
            
        }
  	   

	});
}]);

angular.module('mean.system').factory("WidgetDataResource", ['$resource', function($resource) {
    return $resource('widgets/data/:widgetId', {
       	widgetId:	'@id'
      
       	
    }, {
        update: {
            method: 'PUT'
        },
        getData: {
        	method: 'GET',
        	params:{dashboardId:'@dashboardId',st:'@startTime', et:'@endTime'},
        	isArray:true        	
        }
        
  	   

	});
}]);
angular.module('mean.system').factory("WidgetObjectsResource", ['$resource', function($resource) {
    return $resource('widgets/objects/:widgetId', {
       	widgetId:	'@id'
      
       	
    }, {
        update: {
            method: 'PUT'
        },
        getObjects: {
        	method: 'GET',
        	params:{accountName:'@accountName',accountDomain:'@accountDomain'},
        	isArray:true        	
        }
        
  	   

	});
}]);

angular.module('mean.system').factory("AccountResource", ['$resource', function($resource) {
    return $resource('accounts/:accountId', {
       	accountId:	'@_id'
      
       	
    }, {
        update: {
            method: 'PUT'
        }
  	   

	});
}]);
angular.module('mean.system').factory("AccountDataResource", ['$resource', function($resource) {
    return $resource('accounts/data/:userId', {
       	userId:	'@id'
      
       	
    }, {
        update: {
            method: 'PUT'
        },
        getData: {
        	method: 'GET',
        	params:{domain:'@domain'}        	
        }
  	   

	});
}]);

angular.module('mean.system').factory("DashboardResource", ['$resource', function($resource) {
    return $resource('dashboards/:dashboardId', {
       	dashboardId:	'@_id'
      
       	
    }, {
        update: {
            method: 'PUT'
        }
  	   

	});
}]);

