angular.module('mean.system')
  .controller('WidgetViewController', function ($scope, $modalInstance,$rootScope, $timeout,widgets,Global) {
    // add widget to scope
   
    $scope.global = Global;
    $scope.widgets = widgets;

	console.log($scope.widgets);
	
    //$scope.widget.metrics
    
    // set up result object
    $scope.result = {};
    $scope.displayMetrics = {};
    $scope.buckets =[1,2,3,4];
    $scope.widgetdata=null;
    $scope.widgetTempMap={};
    $scope.selectedMetrics={};
    
    var removeClass = function(key) {
    	var element = angular.element($('#' + key));
    	console.log(element);
    	element.removeClass('btn-primary');
      	element.addClass('btn-default');
    }
    var refreshGraph = function() {
    	var finalWidgetData = {
      			min_date:'',
   	  		  	max_date:'',
  	  		  	list:[]
      	};
    	_.each($scope.widgetTempMap,function(val,key){
      		
      		console.log("Building Final",val,key);
      		//TODO:check if the val min date is less that finalData..
      		finalWidgetData.min_date = val.min_date;
      		finalWidgetData.max_date  = val.max_date;
      		finalWidgetData.list.push(val.list[0]);
      		
      		
      	});
      	$scope.widgetdata = finalWidgetData;
    	
    };
    
    $scope.removeSelectedMetric = function(key) {
    	
    	delete $scope.selectedMetrics[key];
    	delete $scope.widgetTempMap[key];
    	
    	refreshGraph();
    	//TODO: move this into model and set is via model
    	removeClass(key);
    };
    $scope.dropped = function(dragEl, dropEl) {
      // this is your application logic, do whatever makes sense
      
      var drag = angular.element(dragEl);
      var drop = angular.element(dropEl);
      if($scope.selectedMetrics[drag.attr('id')])return;
      var currentDate = new Date();
 	  var tempWidgetdata = {
 				min_date:currentDate,
   	  		  	max_date:currentDate,
  	  		  	list:[]

 		};
      	console.log("The element " + drag + " has been dropped on " + drop + "!");
      	var widgetId = drag.attr('data-widget-id');
      	var metricname = drag.attr('data-metric-name');
      	
      	drag.removeClass('btn-default');
      	drag.addClass('btn-primary');
      	
      	_.each($scope.widgets,function(widget){
      		if(widgetId === widget.wr._id) {
      			//$scope.widgetTempMap[widgetId]
      			var tempData=widget.dataModel.widgetScope.getData();
      			tempWidgetdata.min_date = (+tempWidgetdata.min_date > +tempData.min_date)? tempData.min_date : tempWidgetdata.min_date;
      			tempWidgetdata.max_date = (+tempWidgetdata.max_date < +tempData.max_date)? tempData.max_date : tempWidgetdata.max_date;
      			_.each(tempData.list,function(data){
      				if(data.key === metricname){
      					tempWidgetdata.list.push(data);
      				};
      			});
      		}
      	});
      	
      	$scope.widgetTempMap[drag.attr('id')]=tempWidgetdata;
      	refreshGraph();
    //  	drop.text(drag.text());
    	$scope.selectedMetrics[drag.attr('id')]=drag.attr('data-metric-name');
    	
      	/*
      	var tempArray =[]
      	_.map(widgetTempMap,function(val,key){
      		tempArray.push(val);
      	});
      	*/
      	$scope.$apply();
    };
	$scope.showMetrics = function(widget) {
		console.log(widget);
		
		 $scope.displayMetrics[widget.wr._id]=[];
		 _.each(widget.dataModel.widgetScope.getMetrics(),function(metric) {
		 	$scope.displayMetrics[widget.wr._id].push({title:metric,drag:true});
		 });
		//define in WidgetDataModel and implement in metrics.js
		
	};


    $scope.ok = function () {
    	
      $modalInstance.close($scope.result);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
    
   
  });


