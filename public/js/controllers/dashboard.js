angular.module('mean.system')
  .controller('DashboardAddCtrl', function ($scope, $modalInstance) {
    // add widget to scope
    
    $scope.result = {};
    $scope.result.title = "Default"
	


    $scope.ok = function () {
    	
      $modalInstance.close($scope.result);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
    
   
  })
  .controller('DashboardEditCtrl', function ($scope, $modalInstance,dashboard) {
    
    
    
	$scope.dashboard = dashboard;
	$scope.result = {};
    $scope.result.title = $scope.dashboard.title;

    $scope.ok = function () {
    	
      $modalInstance.close($scope.result);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
    
   
  })
  .controller('DashboardRemoveCtrl', function ($scope, $modalInstance,dashboard) {
    
    
    
	$scope.dashboard = dashboard;
	$scope.result = {};
    

    $scope.ok = function () {
    	
      $modalInstance.close($scope.result);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
    
   
  }); 