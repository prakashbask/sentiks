angular.module('mean.system').controller('SearchSummaryController', ['$scope','$timeout','$window','Global', function ($scope, $timeout,$window,Global,Product) {


     $scope.container={}; 
     $scope.container['twitter']=new Object(); 
     $scope.container['facebook']=new Object();
     $scope.container['youtube']=new Object();
     $scope.container['googleplus']=new Object();
     $scope.container['rss']=new Object();
     $scope.container['linkedin']=new Object();
     
     $scope.container['positive']=new Object();
     $scope.container['negative']=new Object();
     $scope.container['neutral']=new Object();
     $scope.container['highinfluence']=new Object();
     $scope.container['lowinfluence']=new Object();
     
     $scope.container['googleplus'].count=0;
     $scope.container['linkedin'].count=0;
     
     $scope.container['positive'].percent=0;
     $scope.container['negative'].percent=0;
     $scope.container['neutral'].percent=0;
     $scope.container['highinfluence'].percent=0;
     $scope.container['lowinfluence'].percent=0;
     $scope.showSource = true;
     $scope.showSentiment = false;
     $scope.showInfluence = false;
     
     $scope.container['twitter'].options={
            barColor:'#00aced',
            lineWidth:5,
            scaleColor:'#00aced'
            
     };
     $scope.container['facebook'].options={
            barColor:'#3b5998',
            lineWidth:5,
            scaleColor:'#3b5998'
            
     };
     $scope.container['youtube'].options={
            barColor:'#bb0000',
            lineWidth:5,
            scaleColor:'#bb0000'
     };
     $scope.container['googleplus'].options={
            barColor:'#dd4b39',
            lineWidth:5,
            scaleColor:'#dd4b39'
     };
     $scope.container['rss'].options={
            barColor:'#ee802f',
            lineWidth:5,
            scaleColor:'#ee802f'
            
     };
     $scope.container['linkedin'].options={
            barColor:'#007bb6',
            lineWidth:5,
            scaleColor:'#007bb6'
            
     };
     //Linkedin Blue Hex: #007bb6
     // Instagram Blue Hex: #517fa4
     //Pinterest Red Hex: #cb2027
     //Quora Burgundy Hex: #a82400
     //Flickr Pink Hex: #ff0084
     //Tmmblr Dark TurquoiseHex: #32506d
     //Vimeo GreenHex: #aad450
     //Foursquare Logo Blue Hex: #0072b1
     
     $scope.container['positive'].options={
     		colorStart: '#B3DE69',   // Colors
			colorStop: '#B3DE69'
     };
     $scope.container['negative'].options={
     		colorStart: '#FB8072',   // Colors
			colorStop: '#FB8072'
     };
     $scope.container['neutral'].options={
     		colorStart: '#7282fb',   // Colors
			colorStop: '#7282fb'
     };
      $scope.container['highinfluence'].options={
     		colorStart: '#8FBC8F',   // Colors
			colorStop: '#8FBC8F'
     };
      $scope.container['lowinfluence'].options={
     		colorStart: '#6E8B3D',   // Colors
			colorStop: '#6E8B3D'
     };
          
     
     $scope.$watch('showChartsData', function(newValue, oldValue){
 				
 				if(newValue == true) {
 				
 					$scope.updateSearchSummaryDashboard();
 				} 
 			});
 	
	$scope.updateSearchSummaryDashboard =function() {
		 var totalCount = $scope.items.length;
		 var positiveCount=0;
		 var negativeCount=0;
		 var neutralCount=0;
		 if($scope.topResults) {
		 	$scope.topResults.forEach(function(result){
		 		
		 		$scope.container[result.source].percent=Math.floor((result.count/totalCount)*100);
		 		$scope.container[result.source].count=result.count;
		 		positiveCount += result.positivecount;
		 		negativeCount += result.negativecount;
		 		neutralCount += result.neutralcount;
		 		
		 	});
		 }
		 $scope.container['positive'].percent=Math.floor((positiveCount/totalCount)*100);
		 $scope.container['negative'].percent=Math.floor((negativeCount/totalCount)*100);
		 $scope.container['neutral'].percent=Math.floor((neutralCount/totalCount)*100);
		 $scope.container['highinfluence'].percent=Math.floor(($scope.highInfluenceCount/totalCount)*100);
		  $scope.container['lowinfluence'].percent=Math.floor(($scope.lowInfluenceCount/totalCount)*100);
		
		 /*
		 $scope.twitterpercent=Math.floor((Math.random()*100)+1);
	     $scope.facebookpercent=Math.floor((Math.random()*100)+1);
		 $scope.youtubepercent=Math.floor((Math.random()*100)+1);
		 $scope.googlepluspercent=Math.floor((Math.random()*100)+1);
		 $scope.rsspercent=Math.floor((Math.random()*100)+1);*/
	}  	


}]);

