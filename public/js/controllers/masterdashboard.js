angular.module('mean.system').controller('MasterDashboardController', 
 ['$rootScope','$scope','$state','$timeout','$window','$modal','$http','$location','Global','DashboardResource','WidgetDefs','WidgetResource',
  function ($rootScope,$scope,$state, $timeout,$window,$modal,$http,$location,Global,DashboardResource, WidgetDefs,WidgetResource) {

	$scope.global = Global;
	$scope.IntroOptions = {
                   steps:[
                   	   {
                           element: '#step1',
                           intro:'Add New Widget' 
                       },
                   	   {
                           element: '#step2',
                           intro:'Add a Dashboard',
                           position:'right'
                         
                       },
                       {
                           element: '#step3',
                           intro:'Edit Dashboard',
                           
                         
                       },
                       {
                           element: '#step4',
                           intro:'Remove Dashboard',
                           position:'right'
                       },
                       {
                           element: '#step5',
                           intro:'List of Dashboards',
                           
                       },
                       {
                           element: '#step6',
                           intro:'Help/Logout',
                           position:'left'
                       }
                   ],
                    showStepNumbers: false,
                    exitOnOverlayClick: true,
                    exitOnEsc:true
                    
    };
    $scope.gridsterOpts = {
		margins: [20, 20],
		//rowHeight:'160',
		//defaultSizeY:1,
		draggable: {
			
			stop: function(event,uiWidget,$element) {
				$element.scope().saveDashboard();
			}
		},
		resize: {
        
        
         resize: function(event, uiWidget, $element) {
         	//console.log(uiWidget, $element);
         	
         }, // optional callback fired when item is resized,
         stop: function(event, uiWidget, $element) {
         //	console.log($element.scope().resize());
         	console.log("Resized",uiWidget.size);
         	$element.scope().resize(uiWidget.size);
         	$element.scope().saveDashboard();
         } // optional callback fired when item is finished resizing
        }
	};
	
	
	var defaultWidgetDef = 	{ name:'Default'};
		
    
   var defaultWidgets = [];
   $scope.dashboard = {
   	list: [],
   	selected: "Default",
   	selectedDashboard:""
   }
  /* $scope.dashboard= {
   	selected: "Default",
   	list:['Default']
   };
    */
    $scope.dashboardOptions = {
      widgetButtons: true,
      widgetDefinitions: WidgetDefs,
      defaultWidgets: defaultWidgets,
      defaultWidgetDef: defaultWidgetDef,
      dashboard:$scope.dashboard
    };
    
    $scope.addDefaultWidgets = function () {
      var dimensions = {
				sizeX: 2,
	       		sizeY:2,
	       		row:0,
	       		col:0
	  };
	  
	  $scope.dashboardOptions.addWidget({type:'linkedin'},true,true,dimensions);
	  
	  dimensions.col = 2;
	  $scope.dashboardOptions.addWidget({type:'twitter'},true,false,dimensions);
      
      dimensions.col = 4;
      $scope.dashboardOptions.addWidget({type:'facebook'},true,false,dimensions);
    };

    $scope.addDefaultDashboard = function(title) {
    	var dr = new DashboardResource({title:title || 'Default'});
    			dr.$save(function(response) {
    				$scope.dashboard.list.push(response);
    				$scope.saveDashboardRemote(); 
        			$scope.dashboard.selected = response.title;
					$scope.dashboard.selectedDashboard = response;
    				//$scope.setCurrentDashboard(response);
    				$scope.addDefaultWidgets();		
    				//$timeout($scope.dashboardOptions.saveDashboard(),500);
    					
    			});
    	
    }
    $scope.setSelectedDashboard = function() {
    	var dashboardState = $scope.dashboardOptions.getDashboardState($scope.global.data.user._id);
    	if(dashboardState) {
    		_.each($scope.dashboard.list,function(dashboard){
    			if(dashboardState.seldashboardId === dashboard._id) {
    				$scope.dashboard.selected = dashboard.title;
    				$scope.dashboard.selectedDashboard = dashboard;
    			}
    		});
    	}else {
    			$scope.dashboard.selected = $scope.dashboard.list[0].title;
    			$scope.dashboard.selectedDashboard = $scope.dashboard.list[0];
    	}
    	
    	
    };
    $scope.initDashboard = function() {
    	
    	DashboardResource.query( function(dashboards) {
    		console.log("dashboards",dashboards);
    		if(dashboards.length == 0) {
    				$scope.addDefaultDashboard();
    		} else {
    			$scope.dashboardOptions.clear();
    			$scope.dashboard.list = dashboards;
    			
    			$scope.setSelectedDashboard();
    		    	
    		 	WidgetResource.query({dashboardId:$scope.dashboard.selectedDashboard._id},function(widgets) {
    		 	 if(widgets && widgets.length == 0)$scope.CallMe();
           		 $scope.dashboardOptions.init(widgets);
        		});	
    		}
    	}, function (err) {
    		/*
    		var dr = new DashboardResource({title:'Default',user:$scope.global.data.user._id});
    			dr.$save(function(response) {
    				$scope.dashboard.list.push(response);
    				
    			});*/
    	});
       
    
    };

	$scope.addDashboard = function (event) {
		
		//event.preventDefault();
		event.stopPropagation();
		var options = {
			              templateUrl: 'views/template/dashboard-add.html',
			              controller: 'DashboardAddCtrl',
			              windowClass: 'configure-widget'
			           };
			          
			          
			          
		var modalInstance = $modal.open(options);
			
			          // Set resolve and reject callbacks for the result promise
			   modalInstance.result.then(
			            function (result) {
			              
			              console.log('result: ', result);
			              var dr = new DashboardResource({title:result.title});
			    			dr.$save(function(response) {
			    				$scope.dashboard.list.push(response);
			    				$scope.setCurrentDashboard(response);
			    			});
				          
			            },
			            function (reason) {
			              console.log('Dashboard Add dismissed: ', reason);
			
			            }
			          );
		
		
		
	};
	$scope.editDashboard = function (event) {
		
		//event.preventDefault();
		event.stopPropagation();
		var options = {
			              templateUrl: 'views/template/dashboard-edit.html',
			              resolve: {
			                dashboard: function () {
			                  return $scope.dashboard.selectedDashboard;
			                }
			              },
			              controller: 'DashboardEditCtrl',
			              windowClass: 'configure-widget'
			           };
			          
			          
			          
		var modalInstance = $modal.open(options);
			
			          // Set resolve and reject callbacks for the result promise
			   modalInstance.result.then(
			            function (result) {
			              
			              console.log('result: ', result);
			              var dr = $scope.dashboard.selectedDashboard;
			              dr.title = result.title;
			              
			                
			    			dr.$update(function(response) {
			    				$scope.setCurrentDashboard(dr);
			    			});
				          
			            },
			            function (reason) {
			              console.log('Dashboard Add dismissed: ', reason);
			
			            }
			          );
    
		
	};
	$scope.removeDashboard = function (event) {
		
		//event.preventDefault();
		event.stopPropagation();
		var selectedDashboard = $scope.dashboard.selectedDashboard;
		var options = {
			              templateUrl: 'views/template/dashboard-remove.html',
			              resolve: {
			                dashboard: function () {
			                  return selectedDashboard;
			                }
			              },
			              controller: 'DashboardRemoveCtrl',
			              windowClass: 'configure-widget'
			           };
			          
			          
			          
		var modalInstance = $modal.open(options);
			
			          // Set resolve and reject callbacks for the result promise
			   modalInstance.result.then(
			            function (result) {
			              
			              console.log('result: ', result);
			             $scope.dashboardOptions.clear()
			             //dr.title = result.title;
			              $scope.dashboard.list.splice(_.indexOf($scope.dashboard.list, selectedDashboard), 1);
			                
			    			selectedDashboard.$remove(function(response) {
			    				console.log("Remove successfully");
			    			});
			    			if($scope.dashboard.list.length > 0) {
			    				$scope.setCurrentDashboard($scope.dashboard.list[0]);
				           } else {
								$scope.addDefaultDashboard('Untitled');
								/*
				           		$scope.dashboard.selected="";
				           		$scope.dashboard.selectedDashboard="";*/
				           }
			            },
			            function (reason) {
			              console.log('Dashboard Remove dismissed: ', reason);
			
			            }
			          );
    
		
	};

	$scope.saveDashboardRemote = function() {
		var dashboardState = $scope.dashboardOptions.getDashboardState($scope.global.data.user._id);
		$http.post('/dashboardstate',dashboardState);
	};

	$scope.logout = function(event) {
		event.stopPropagation();
		 $scope.saveDashboardRemote();
		$timeout(function() {
			$location.path('/logout');
		},500);
	};

	$scope.setCurrentDashboard = function(dashboard) {
		//TODO: save existing dashboard state or dimensions of the widget
		
		$scope.saveDashboardRemote(); 
        $scope.dashboard.selected = dashboard.title;
		$scope.dashboard.selectedDashboard = dashboard;
		WidgetResource.query({dashboardId:$scope.dashboard.selectedDashboard._id},function(widgets) {
           		 $scope.dashboardOptions.init(widgets);
        });	
	};
	
	
	
	//
	

}]);

