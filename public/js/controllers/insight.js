angular.module('mean.system').controller('InsightSummaryController', ['$scope','$timeout','$window', function ($scope, $timeout,$window,Global,Product) {
	
	$scope.events = [];
	$scope.items=[]
	$scope.showKeyword=false;
	$scope.showMention=false;
	$scope.setItem = function(event){
		$scope.items.pop();
    	$scope.items.push(event);
	};
	
	$scope.destroy=function() {
		$scope.events.length=0;
		$scope.items.length=0;
		$scope.events=[];
		$scope.items=[]
		$scope.$apply();
	}
	$scope.init=function() {
		$scope.events.length=0;
		$scope.items.length=0
		 $scope.events = $scope.events.concat($scope.timesecondsDimension.top(10));
		 
		
	}
	$scope.getIconClass=function(category) {
		var category=category.toLowerCase();
		var color= category+'C';
		if(category == 'youtube') category="youtube-play"; //map it to fontawesome icon
		var iconclass="fa fa-" + category + " "+color;
		return iconclass;
	}
	$scope.showNewEvents=function(key) {
		
		
		
		//var results = $.grep($scope.items, function(e){ return e.title == key; });
		var reg = new RegExp(key,'i');
		var resultsD = $scope.titleDimension.filter(function(d,index) {
           if(d) {
              return reg.test(d);
           }else return false;
        });
        //console.log("Results",resultsD.top(Infinity));
        $scope.events=[];
        $scope.events=$scope.events.concat($scope.timesecondsDimension.top(Infinity));
        
        $scope.$apply();
        $scope.titleDimension.filter(null);
		
	}

	/*
	$scope.$watch("showInsightSummary", function(newVal, oldVal) {
		if(newVal == true)$timeout(function(){$scope.init()});
		
	});
	$scope.$watch("showChartsData", function(newVal, oldVal) {
		if((newVal == true) && ($scope.showInsightSummary == true))$timeout(function(){$scope.init()});
		
	});
     */
    $scope.$watch("showMention",function(newVal, oldVal) {
    	if(newVal == true) $timeout(function() {$scope.init()});
    	else $timeout(function() {$scope.destroy()});
    });
    $scope.$watch("showChartsData", function(newVal, oldVal) {
		if((newVal == true) && ($scope.showMention == true))$timeout(function(){$scope.init()});
		
	});

}]);

