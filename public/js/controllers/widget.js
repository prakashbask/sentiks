angular.module('mean.system')
  .controller('WidgetDialogCtrl', function ($scope, $modalInstance,$rootScope, $timeout,widget, optionsTemplateUrl,Token,Global) {
    // add widget to scope
   
    $scope.global = Global;
    $scope.widget = widget;
	console.log($scope.global);
	$scope.accountstatus = new Object();
   	$scope.accountstatus.status = false;
   	$scope.accountstatus.statusError = false;
    //$scope.widget.metrics
    
    // set up result object
    $scope.result = {};
    $scope.account = {};
	$scope.result.config = angular.copy($scope.widget.config);
	$scope.result.title = widget.title;
	$scope.result.type = widget.type;
	
	$scope.accountslist = {};
	$scope.storagelist =  angular.copy($scope.widget.config.storage.list);
	
	
	
	_.each($scope.global.data.accounts, function (account) {
		           		//wDef type can be twitter or twitter-search. 
		           		if(account.domain === ($scope.widget.type.split('-'))[0]) {
		           			$scope.accountslist[account.name]=account._id;
		           			
		           		}
		           		if($scope.widget.config.storeoptions.length > 0 && 
		           			account.category && account.category === 'storage') {
		           			$scope.storagelist.push(account.name);
		           			
		           		}
	});
	
	console.log("After copy", $scope.result);
	$scope.openAuthWindow = function(loc,popupOptions) {
      var extraParams = $scope.askApproval ? {approval_prompt: 'force'} : {};
      Token.getTokenByPopup(loc,extraParams,popupOptions)
        .then(function(params) {
          // Success getting token from popup.

          		$scope.accountstatus.statusError = false;
          		$scope.accountstatus.message = "Added Account: "+params.name;
          		$scope.accountstatus.status=true;
                
                //Following lines are comments as we don't have to write back to server on the new account added.
                /*
                $scope.account.token = params.token;
                $scope.account.user = $scope.global.data.user;
                $scope.account.accountId = params.accountId;
                
                //$scope.expiresIn = params.expires_in;
				
                //Token.set($scope.account);
                */
                //$scope.result.config.accounts.list[params.name]=params.accountId;
				
				//$scope.widget.config.custom.username = params.name;
				if($scope.widget.config.storeoptions.length > 0 &&
					params.category && params.category === 'storage') {
					//$scope.result.config.storage.list.push(params.name);
					$scope.storagelist.push(params.name);
					$scope.result.config.storage.selected = params.name;
					//$scope.widget.config.storage.list.push(params.name);
					
				} else {
					$scope.result.config.accounts.selected = params.name;
					$scope.accountslist[params.name]=params.accountId;
				}
				$rootScope.$emit('accountAdded');
				console.log("Params",params);
				 $timeout(function() { 
					$scope.accountstatus.status=false;
				},3000);

              
        }, function() {
          // Failure getting token from popup.
          //alert("Failed to get token.");
            $scope.accountstatus.statusError=true;
            $scope.accountstatus.message = "Failed to Add Account. Please try again";
            $scope.accountstatus.status=true;
            $timeout(function() { 
					$scope.accountstatus.status=false;
				},3000);
        });
        
      
    };
    $scope.label = function(choice) {
    	
        	
    	if(choice === 'Passthru' || choice  === 'Remote') return choice;
    	
    	var newLabel;
    	_.each($scope.global.data.storageaccounts,function(account) {
    		
    		if(choice === account.name) newLabel = choice + ' (' + account.domain+ ')';
    			
    		
    		
    	});
    	
    	return newLabel;
    }
	
    // look for optionsTemplateUrl on widget
    $scope.optionsTemplateUrl = optionsTemplateUrl || 'views/template/widget-default-content.html';

    $scope.ok = function () {
    	$scope.result.config.status=true;
      $modalInstance.close($scope.result);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
    
   
  });
   angular.module('mean.system')
  .controller('WidgetRemoveCtrl', function ($scope, $modalInstance, widget) {
    // add widget to scope
    $scope.widget = widget;

    //$scope.widget.metrics
    
    // set up result object
    $scope.result = {};

	
    $scope.ok = function () {
      $modalInstance.close($scope.result);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
    
   
  });
  
  angular.module('mean.system')
  .controller('WidgetConfigureCtrl', function ($scope, $modalInstance,WidgetSources) {
    // add widget to scope
   
    $scope.sources =  WidgetSources.sources;
    $scope.sourceslist = WidgetSources.sourceslist;	
    $scope.map = {};
      _.each($scope.sources, function (source) {
        $scope.map[source.category] = source;
      });
   	
    	
     $scope.result = {
      type: "Default"
    };

    // set up result object
   

    $scope.updateSelection = function(type) {
    	$scope.result.type=type;
    	$scope.destroy();
    	$modalInstance.close($scope.result);
    }
    $scope.getSourceByName = function (type) {
      return $scope.map[type];
    };
    
    $scope.ok = function () {
       $scope.destroy();
      $modalInstance.close($scope.result);
    };

    $scope.cancel = function () {
    	console.log("in scope cancel");
    	$scope.destroy();	
      $modalInstance.dismiss('cancel');
    };
    
    $scope.destroy = function() {
    	
    	sources = [];
    	map = {};
    	$scope.sourceslist = [];
    	$scope.$apply();
    };
   
     $scope.$on('$destroy', function () {
     	
		$scope.sources = null;
    	$scope.map = null;
    	$scope.sourceslist = null;
    	
	 });
   
  });
angular.module("mean.system").run(["$templateCache", function($templateCache) {
/*
  $templateCache.put("views/template/dashboard.html",
    "<div>\n" +
     "\n" +
     "  <div id=\"topnavigator\" class=\"posfixed theme1\">\n " +
	 "					<a href=\"#\" class=\"title\">\n" +
	 "					<span>sentiks </span>\n "  +
	 "						<span class=\"text-toggle\"> </span> " +
	 "					</a> \n" +
	 "					<ul class=\"actions\">\n" +
	 "						<li > <span ng-click=\"addWidgetDefault($event);\" class=\"fa fa-plus \"></span></li>\n" +
	 "						<li ng-click=\"clear();\"> <span class=\"fa fa-minus fa-1x\"></span></li> \n" +
	 "					</ul>\n" +
	 "	</div>\n" +
    "    <div gridster=\"gridsterOpts\"  ng-model=\"widgets\" class=\"dashboard-widget-area\">\n" +
    "	 <ul>" +
    "        <li gridster-item=\"item\" ng-repeat=\"widget in widgets\" ng-style=\"widget.style\" class=\"widget-container\" widget>\n" +
    "            <div class=\"widget panel panel-default\">\n" +
    "                <div class=\"widget-header panel-heading\">\n" +
    "                    <span  class=\"action fa fa-angle-down fa-1x\" ng-click=\"showKeyword = !showKeyword\"></span>\n" +
	"							    {{widget.title}}\n" +
	"					   <span ng-click=\"removeWidget(widget);\" class=\"action pull-right fa fa-times fa-1x\"></span>\n" +
	"						<span ng-click=\"openWidgetDialog(widget);\" class=\"action pull-right fa fa-cog fa-1x\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"widget-content panel-body\">\n" +
    "                    <div></div>\n" +
    "                </div>\n" +
    "                <div class=\"widget-ew-resizer\" ng-mousedown=\"grabResizer($event)\"></div>\n" +
    "            </div>\n" +
    "        </li>\n" +
    "		</ul>\n" +
    "    </div>\n" +
    "</div>"
  );
*/
  $templateCache.put("views/template/widget-default-content.html",
    "No edit template specified for this widget ({{widget.name}})."
  );

  $templateCache.put("views/template/widget-template.html",
    "<div class=\"modal-header\">\n" +
    "    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" ng-click=\"cancel()\">&times;</button>\n" +
    "  <h3>Widget Options <small>{{widget.title}}</small></h3>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"modal-body\">\n" +
    "    <form name=\"form\" novalidate class=\"form-horizontal\">\n" +
    "        <div class=\"form-group\">\n" +
    "            <label for=\"widgetTitle\" class=\"col-sm-2 control-label\">Title</label>\n" +
    "            <div class=\"col-sm-10\">\n" +
    "                <input type=\"text\" class=\"form-control\" name=\"widgetTitle\" ng-model=\"result.title\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div ng-include=\"optionsTemplateUrl\"></div>\n" +
    "    </form>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"modal-footer\">\n" +
    "    <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancel()\">Cancel</button>\n" +
    "    <button type=\"button\" class=\"btn btn-primary\" ng-click=\"ok()\">OK</button>\n" +
    "</div>"
  );
  
   $templateCache.put("views/template/widget-default-content.html",
    "No edit template specified for this widget ({{widget.name}})."
  );
  
  $templateCache.put('views/widgets/metrics/metric-graph.html',
  	"<div id=\"graph\" style=\"overflow:auto !important\" ></div>"
  );
  $templateCache.put('views/widgets/metrics/metric-details.html',
  	'<div class="counters row" >\n' + 
   	'<div class="col-md-4 col-sm-4 box" ng-repeat="(key,val) in metrics">\n' +
	'	<div class="name">{{key}}</div> \n' +
	'    	<div class="count">{{val}}</div>\n' +
    '	</div> \n'+
  '</div>'
  );


}]);