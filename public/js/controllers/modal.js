angular.module('mean.system').controller('ModalController', ['$scope', '$routeParams', '$location', '$timeout','$sce','Global', 'Product','Trainer', function ($scope, $routeParams, $location, $timeout,$sce, Global, Product,Trainer) {
    $scope.global = Global;
    $scope.items=[];
    $scope.title;
    $scope.sentiment;
    $scope.id;
    $scope.showAlert=false;
    $scope.category;
    $scope.source;
    $scope.buildUrl = function(source) {
    	  
    	  if(source) {
	  	  return '/views/'+source.toLowerCase() + '.html';
	  	  }else return "";
  
	}
    this.setModel = function(data) {
    
    	$scope.items=data;
    	//$scope.title=$scope.items[0].title;
    	$scope.id=$scope.items[0].id;
    	$scope.sentiment=$scope.items[0].sentiment;
    	$scope.category=$scope.items[0].category;
 		$scope.source=$scope.items[0].source;
    
   }
    $scope.updateSentiment = function(data) {
    
    	$scope.showLoader=true;
    	Trainer.updateSentiment({keyword:$scope.title,action:data, type:'text'}, function(results) {
    	//	$scope.alertMessage="Updated with " + data+ " sentiment";
    		$scope.showAlert=true;
    	});
    	//$scope.showTrain=true;
    	
    	$scope.showLoader=false;
    	$timeout(function() {
    		$scope.showAlert=false;
    	},2000);
    	
    };
  // $scope.setModel = this.setModel;     
 	$scope.trustSrc = function(src) {
	    return $sce.trustAsResourceUrl(src);
  	}
   
   
   

}]);