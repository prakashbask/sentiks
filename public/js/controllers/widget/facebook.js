angular.module('mean.system').controller('FacebookController',['$scope','$timeout','WidgetObjectsResource', function ($scope,$timeout,WidgetObjectsResource) {
	$scope.showLoader = false;
	$scope.init = function(newVal) {
		$scope.showLoader = true;
		
		WidgetObjectsResource.getObjects({widgetId: $scope.widget.wr._id,
										  accountName: newVal,
										  accountDomain: $scope.widget.type},
											function(data) {
												
												//TODO: check if the length of the data is zero
												console.log(data[0]);
												$scope.showLoader = false;
												$scope.result.config.objects.list = data[0];
											},function(err){
												$scope.showLoader = false;
												$scope.accountstatus.statusError = true;
												 $scope.accountstatus.message = "Failed to Load Pages. Please try again";
										            $scope.accountstatus.status=true;
										            $timeout(function() { 
															$scope.accountstatus.status=false;
														},3000);
											});
		
		
	}

	$scope.$watch('result.config.accounts.selected', function(newVal,oldVal) {
		$timeout(function() { if(newVal)$scope.init(newVal)});
	});
}]);

