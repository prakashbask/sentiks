angular.module('mean.system').controller('YoutubeController',['$scope','$timeout','WidgetObjectsResource', function ($scope,$timeout,WidgetObjectsResource) {
	$scope.showLoader = false;
	$scope.initChannels = function(newVal) {
		$scope.showLoader = true;

		WidgetObjectsResource.getObjects({widgetId: $scope.widget.wr._id,
										  accountName: newVal,
										  accountDomain: $scope.widget.type.split('-')[0]},
											function(data) {

												//TODO: check if the length of the data is zero
												console.log(data[0]);
												$scope.showLoader = false;
												$scope.result.config.objects.list = data[0];
											},function(err){
												$scope.showLoader = false;
												$scope.accountstatus.statusError = true;
												 $scope.accountstatus.message = "Failed to Load Channels. Please try again";
										            $scope.accountstatus.status=true;
										            $timeout(function() {
															$scope.accountstatus.status=false;
														},3000);
											});


	};

	$scope.initVideos = function(newVal) {
		$scope.showLoader = true;

		WidgetObjectsResource.getObjects({widgetId: $scope.widget.wr._id,
											accountName: $scope.result.config.accounts.selected,
											accountDomain: $scope.widget.type.split('-')[0],
											channelName:newVal },
											function(data) {

												//TODO: check if the length of the data is zero
												console.log(data[0]);
												$scope.showLoader = false;
												$scope.result.config.custom.videos.list = data[0];
											},function(err){
												$scope.showLoader = false;
												$scope.accountstatus.statusError = true;
												$scope.accountstatus.message = "Failed to Load Videos. Please try again";
																$scope.accountstatus.status=true;
																$timeout(function() {
															$scope.accountstatus.status=false;
														},3000);
											});


	};



	$scope.$watch('result.config.accounts.selected', function(newVal,oldVal) {
		$timeout(function() { if(newVal)$scope.initChannels(newVal)});
	});

	$scope.$watch('result.config.objects.selected', function(newVal,oldVal) {
		$timeout(function() { if(newVal)$scope.initVideos(newVal)});
	});
}]);
