
angular.module('mean.system').controller('AnalyseController', ['$scope', 'Global','Analyser','Trainer', function ($scope, Global,Analyser,Trainer) {
	$scope.global = Global;
    $scope.showResults = false;
    $scope.showLoader = false;
    $scope.showSearchBar = true;
    $scope.showTrain = false;
   
    $scope.analyse = function(){
    	console.log($scope.keyword);
    	$scope.showResults = false;
    	
    	
    	
    	$scope.showLoader=true;
    	
    	var data= $scope.keyword;
    	
    	
       
        Analyser.analyse( {keyword:data},function(results) {
          //  $scope.modalMessage= spots.count;
          	$scope.items = results;
          	        
            $scope.showLoader=false;
            $scope.showResults=true;
          
            
            
            
        });
    };
    
    $scope.updateSentiment = function(data) {
    	console.log(data);
    	$scope.showLoader=true;
    	Trainer.updateSentiment({keyword:$scope.keyword,action:data, type:'text'}, function(results) {
    		$scope.alertMessage="Updated with " + data+ " sentiment";
    	});
    	$scope.showTrain=true;
    	$scope.showLoader=false;
    };
    $scope.trainSentiment = function() {
    	$scope.showLoader=true;
    	Trainer.trainSentiment({type:'text'}, function(results) {
    		$scope.alertMessage="Trained with model";
    	});
    	$scope.showLoader=false;
    };
    
    $scope.addToModel = function(data) {
    	$scope.showLoader=true;
    	Trainer.updateSentiment({keyword:$scope.words,action:data,type:'keyword'}, function(results) {
    		$scope.alertMessage="Adding Words to Model";
    	});
    	$scope.showLoader=false;
    };
}]);
