angular.module('mean.auth')
    .controller('LoginCtrl', ['$q','$scope','$rootScope','$http','$location','$window','Token', function($q,$scope, $rootScope, $http, $location,$window,Token) {

        // This object will be filled by the form
        $scope.user = {};
	$scope.accessToken;
	$scope.login = function() {
		var extraParams = $scope.askApproval ? {approval_prompt: 'force'} : {};
		Token.getTokenByPopup('/login',extraParams)
	        .then(function(params) {
	          // Success getting token from popup.
	
	          			                
	                //Following lines are comments as we don't have to write back to server on the new account added.
	                /*
	                $scope.account.token = params.token;
	                $scope.account.user = $scope.global.data.user;
	                $scope.account.accountId = params.accountId;
	                
	                //$scope.expiresIn = params.expires_in;
					
	                //Token.set($scope.account);
	                */
	                //$scope.result.config.accounts.list[params.name]=params.accountId;
	                $http.get('/loggedin').success(function(user) {
                // Authenticated
                		if (user !== '0') {
					 		$scope.loginError = 0;
                     		$rootScope.user = user;
                     		console.log("LOGIN", $rootScope.user);
                     		$rootScope.$emit('loggedin');
                     		$location.url('login');
						}
					})
	
	              
	        }, function() {
	          // Failure getting token from popup.
	          //alert("Failed to get token.");
	           
	        });
	};
	
	$scope.login();
		
/*
        $scope.login = function(){
            $http.post('/login', {
                email: $scope.user.email,
                password: $scope.user.password
            })
                .success(function(user){
                    // authentication OK
                    $scope.loginError = 0;
                    $rootScope.user = user;
                    console.log("LOGIN", $rootScope.user);
                    $rootScope.$emit('loggedin');
                   $location.url('login');
                })
                .error(function() {
                    $scope.loginerror = 'Authentication failed.';
                });
        };
        */
       
       
    }])
    .controller('RegisterCtrl', ['$scope','$rootScope','$http','$location', function($scope, $rootScope, $http, $location) {
        $scope.user = {};

        $scope.register = function(){
            $scope.usernameError = null;
            $scope.registerError = null;
            $http.post('/register', {
                email: $scope.user.email,
                password: $scope.user.password,
                confirmPassword: $scope.user.confirmPassword,
                username: $scope.user.username,
                name: $scope.user.name
            })
                .success(function(){
                    // authentication OK
                    $scope.registerError = 0;
                    $rootScope.user = $scope.user;
                    console.log("REGISTER",$rootScope.user);
                    $rootScope.$emit('loggedin');
                   $location.url('login');
                })
                .error(function(error){
                    // Error: authentication failed
                    if (error === 'Email is already registered') {
                        $scope.usernameError = error;
                    }
                    else {
                        $scope.registerError = error;
                    }
                });
        };
    }]);
