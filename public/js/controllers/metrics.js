angular.module('mean.system').controller('MetricsSummaryController', ['$scope','$timeout','$window', function ($scope, $timeout,$window) {
	
	 $scope.container={}; 
     $scope.container['twitter']=new Object(); 
     $scope.container['twitter'].folList=[];
     $scope.container['twitter'].retweetList=[];
     $scope.container['twitter'].friendsList=[];
     $scope.container['twitter'].favouriteList=[];
     $scope.container['twitter'].statusesList=[];
     $scope.showMetric=false;
     $scope.container['twitter'].handler= function (event) {
     	
     		if(event.twitCustom) {
     			this.folList.push(event.twitCustom.followersCount);
     			this.retweetList.push(event.twitCustom.retweetCount);
     			this.friendsList.push(event.twitCustom.friendsCount);
     			this.favouriteList.push(event.twitCustom.favouriteCount);
     			this.statusesList.push(event.twitCustom.statusesCount);
     			
     		}
     		
     	
     }
     $scope.container['facebook']=new Object(); 
     $scope.container['facebook'].sharesList=[];
     
     
     $scope.container['facebook'].handler= function (event) {
     		if(event.facebookCustom) {
     			this.sharesList.push(event.facebookCustom.sharesCount);
     			
     			
     		}
     }
     $scope.container['youtube']=new Object();
     $scope.container['youtube'].likeList=[];
     $scope.container['youtube'].ratingList=[];
     $scope.container['youtube'].viewList=[];
     $scope.container['youtube'].favouriteList=[];
     $scope.container['youtube'].commentList=[];
     $scope.container['youtube'].handler= function (event) {
     		if(event.youtubeCustom) {
          			this.likeList.push(event.youtubeCustom.likesCount);
          			this.ratingList.push(event.youtubeCustom.ratingCount);
          			this.viewList.push(event.youtubeCustom.viewCount);
          			this.favouriteList.push(event.youtubeCustom.favouriteCount);
          			this.commentList.push(event.youtubeCustom.commentCount);
            }
     }
     $scope.container['RSS']=new Object(); 
     $scope.container['RSS'].handler= function (event) {
     		
     }
     
	 $scope.events;
	 $scope.redraw=false;
	
	$scope.destroy = function() {
		 $scope.container['twitter'].folList=[];
	     $scope.container['twitter'].retweetList=[];
	     $scope.container['twitter'].friendsList=[];
	     $scope.container['twitter'].favouriteList=[];
	     $scope.container['youtube'].likeList=[];
	     $scope.container['youtube'].ratingList=[];
	     $scope.container['youtube'].viewList=[];
	     $scope.container['youtube'].favouriteList=[];
	     $scope.container['youtube'].commentList=[];
	     $scope.container['facebook'].sharesList=[];
	     $scope.container['twitter'].statusesList=[];
		     
		 $scope.redraw=false;
		 $scope.$apply();
	}
	
	$scope.init=function() {
	
		 //$scope.container['twitter'] = ;
		 $scope.container['twitter'].folList=[];
	     $scope.container['twitter'].retweetList=[];
	     $scope.container['twitter'].friendsList=[];
	     $scope.container['twitter'].favouriteList=[];
	     $scope.container['youtube'].likeList=[];
	     $scope.container['youtube'].ratingList=[];
	     $scope.container['youtube'].viewList=[];
	     $scope.container['youtube'].favouriteList=[];
	     $scope.container['youtube'].commentList=[];
	     $scope.container['facebook'].sharesList=[];
	     $scope.container['twitter'].statusesList=[];
		     
		 $scope.redraw=false;
		  $scope.events=$scope.timesecondsDimension.top(Infinity);
		  for(var eventkey in $scope.events) {
		  	var event = $scope.events[eventkey];
		  	$scope.container[event.category].handler(event);
		  	
		  }
		  $scope.$apply();
		  $scope.redraw=true;
		// console.log($scope.container['twitter'].folList);
		 
	}
	/*
	$scope.$watch("showMetricsSummary", function(newVal, oldVal) {
		if(newVal == true)$timeout(function(){$scope.init()});
		
	});
	$scope.$watch("showChartsData", function(newVal, oldVal) {
		if((newVal == true) && ($scope.showMetricsSummary == true))$timeout(function(){$scope.init()});
		
	});
     */
     $scope.$watch("showMetric", function(newVal, oldVal) {
		if(newVal == true)$timeout(function(){$scope.init()});
		else $timeout(function() {$scope.destroy()});
		
	});
    $scope.$watch("showChartsData", function(newVal, oldVal) {
		if((newVal == true) && ($scope.showMetric == true))$timeout(function(){$scope.init()});
		
	});

}]);

