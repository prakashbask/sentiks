angular.module('mean.system')
.controller('IndexCtrl', ['$scope','$rootScope','$state','AccountResource','Global', function($scope,$rootScope,$state,AccountResource, Global) {
	$scope.global = Global;
	
	var checkIE = function() {
		
		    var ms_ie = false;
		    var ua = window.navigator.userAgent;
		    var old_ie = ua.indexOf('MSIE ');
		    var new_ie = ua.indexOf('Trident/');
		
		    if ((old_ie > -1) || (new_ie > -1)) {
		        return true;
		    }
		
		    return false;

		
    	
	};
	
	
	
	
	if(checkIE()) {
			$scope.statusBrowserMessage = 'Currently we have limited support for IE. This site works best with Chrome or Firefox.';
    		$scope.showBrowserStatus=true;
			
		
	};
 $rootScope.$on('loggedin', function() {

  			AccountResource.query(function(accounts){
		          	 $scope.global.data.accounts = accounts;
		          	 _.each(accounts,function(account){
		          	 	if(account.category === 'storage') $scope.global.data.storageaccounts.push(account);
		          	 });
		          	
		          });

            $scope.global.data.user = $rootScope.user;
            $scope.global.data.authenticated = !! $rootScope.user;
            
            $state.go('dashboard');
            
  });

 $rootScope.$on('accountAdded', function() {
 	AccountResource.query(function(accounts){
		          	 $scope.global.data.accounts = accounts;
		          	
		          	
		          });
 });
$rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
  console.log('$stateChangeStart to '+toState.to+'- fired when the transition begins. toState,toParams : \n',toState, toParams);
});
$rootScope.$on('$stateChangeError',function(event, toState, toParams, fromState, fromParams){
  console.log('$stateChangeError - fired when an error occurs during transition.');
  console.log(arguments);
});
$rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
  console.log('$stateChangeSuccess to '+toState.name+'- fired once the state transition is complete.');
});
// $rootScope.$on('$viewContentLoading',function(event, viewConfig){
//   // runs on individual scopes, so putting it in "run" doesn't work.
//   console.log('$viewContentLoading - view begins loading - dom not rendered',viewConfig);
// });
$rootScope.$on('$viewContentLoaded',function(event){
  console.log('$viewContentLoaded - fired after dom rendered',event);
});
$rootScope.$on('$stateNotFound',function(event, unfoundState, fromState, fromParams){
  console.log('$stateNotFound '+unfoundState.to+'  - fired when a state cannot be found by its name.');
  console.log(unfoundState, fromState, fromParams);
});

}])

.controller('IndexController', ['$scope','$timeout','$window','$location','$anchorScroll','smoothScroll','Global','Product','Shared', function ($scope, $timeout,$window,$location,$anchorScroll,Global,smoothScroll,Product,Shared) {

$scope.global = Global;
    $scope.showResults = false;
    $scope.showSearchSummary=false;
    $scope.showProfileSummary=false;
    $scope.showInsightSummary=false;
    $scope.showMetricsSummary=false;
    $scope.showDiceSummary=false;
    $scope.showCharts = false;
    $scope.showChartsData = false;
    $scope.reinitChartsData = false;
    $scope.showLoader = false;
    $scope.showData = true;
    $scope.showTimeline = true;
    $scope.showSentiment = true;
    $scope.showDice=false;
    $scope.showSource = true;
    $scope.showSearchBar=true;
    $scope.showSpanData=false;
    $scope.showModal = false;  
    $scope.triggerContainer="all";
    $scope.busy=false;
    $scope.after='';
    $scope.items=[];
    $scope.numOfTimesLoaded=0;
    $scope.pageInited=0;
    $scope.showStatus=false;
    $scope.statusMessage="";
    $scope.topResults=[];  
    $scope.highInfluenceCount=0;
    $scope.lowInfluenceCount=0;  
    $scope.timesecondsDimension; 
    $scope.wordgroup;
    $scope.titleDimension;
    $scope.sharedControl = Shared.control; 
    $scope.IntroOptions = {
                   steps:[
                   	   {
                           element: document.querySelector('#step1'),
                           intro: '<div class="intro-text" style="font-family:FontAwesome;font-size:0.7em"><span style="display:block"> <span class="fa fa-bolt hue"> </span>' + 
					        		' Search for a destination/celebrity/product (e.g iphone)</span>' +
					        		'<span style="display:block"> <span class="fa fa-bolt  hue"></span>'+
		 	 		        		' Connects with social media sites.</span>' +
		 	 		        		'<span style="display:block"><span class="fa fa-bolt  hue"></span>'+
						        	 ' Twitter, Facebook, Youtube, News, Blogs, Forums</span>'+
					               	'<span style="display:block"><span class="fa fa-bolt  hue"> </span>'+ 
					        		' Used by Digital Marketers, Corporations, News Agencies	<span>'+
					        		'<span style="display:block"><span class="fa fa-bolt  hue"> </span>'+
					        		' Monitor, Analyse and Extract trends and sentiment</span>'+
					        		'<span style="display:block"><span class="fa fa-bolt  hue"> </span>'+ 
					        		' Improve Reputation of Brand and Marketing</span></div>'
                       }
                   	   
                       
                       
                   ],
                    showStepNumbers: false,
                    exitOnOverlayClick: true,
                    exitOnEsc:true
                    
    };
    //check IE lt 9 version
	if (document.all && !document.addEventListener) {
	
		$scope.statusMessage = 'Your browser version is not supported. Please upgrade your browser to latest version';
    	$scope.showStatus=true;
	}
	
	$scope.buildUrl = function(source) {
		  
	  	  return '/views/'+source.toLowerCase() + '.html';
  
	}
		
	$scope.okClicked = function(){
        
         $scope.showModal = false; 
    };
    
    // function called when negative button (Cancel) is clicked
    $scope.closed = function(){
       
         $scope.showModal = false; 
    };
    
	$scope.getResult = function(obj) {
		return $.grep($scope.items, function(e){ return e.id == obj; });
	}
   
    $scope.applyFilter = function(filterType) {
    	
    	$scope.filterText=filterType;
    }
    $scope.loadMore = function() {
    		if(!$scope.showResults)return;
      		 if ($scope.busy) return;
      		 if($scope.numOfTimesLoaded > 0 ) return;
      		 $scope.numOfTimesLoaded = $scope.numOfTimesLoaded+1;
   			$scope.busy=true;
            var data=$scope.keyword;
        	console.log("Loading more data");
            Product.findTwitterProducts( {channel:"twitter",keyword:data},function(products) {
          
	          	$scope.twitteritems = $scope.twitteritems.concat(products.items);
	          	$scope.items = $scope.items.concat(products.items);
	      
        	});
            $scope.busy=false;
    };
    
    $scope.showChartsFn = function(){
    	$scope.showResults=false;
    	//$scope.showSearchBar=false;
    	$scope.showCharts=true;
    	
    };
    
    $scope.showResultsFn = function(){
    	$scope.showCharts=false;
    	//$scope.showSearchBar=true;
    	$scope.showResults=true;
    	
    };
   // $scope.loadMore();
   $scope.expand = function(obj) {
   	
   		var result = $.grep($scope.items, function(e){ return e.id == obj; });
   	
   		$scope.showModal = true;
   		$scope.modalTitle=result[0].title;
   		$scope.obj = obj;
   	
   };
   $scope.exportToCSV = function() {
   	/*
   		var product = new Product({items:$scope.items,count:$scope.items.length});
   		product.$save(function(success) {
   			 window.open('http://localhost:3000/public/downloads/active.csv');
   		},function(error){
   			$scope.statusMessage = 'Error: ' +error.status + ' while downloading csv';
            $scope.showStatus=true;
            $timeout(function() { $scope.showStatus=false},3000);
   		});
   	*/
   		//TODO Check if Downloadify can be used as it prompts a dialog for saving. However, there is a d
   		// dependency of Flash 10 to use Downloadify
   	    var json = JSON.stringify($scope.items);
   	    var csv = 'Source,Title,Link,Date,Sentiment\r\n';
   	    var array = typeof json != 'object' ? JSON.parse(json) : json;
        
        for (var i = 0; i < array.length; i++) {
        		
				var line = array[i].source+',' + array[i].title.replace(/\r|\n|,/g," ") + ','+array[i].link+',' + array[i].date+','+array[i].sentiment;
				
                csv += line + '\r\n';
        }
		var blob = new Blob([csv], {type: "application/csv"});
		$scope.url  = URL.createObjectURL(blob);
		$scope.fileName  = $scope.keyword+'.csv';

   };
   
    			function reduceAdd(p, v) {
                                if(v.emotions) {
                                        v.emotions.forEach (function(val, idx) {
                                                p.emotions[val] = (p.emotions[val] || 0) + 1; //increment counts
                                        });
                                }
                                if(v.usermentions) {

                                                v.usermentions.forEach (function(val, idx) {
                                                p.usermentions[val] = (p.usermentions[val] || 0) + 1; //increment counts
                                        });

                                }
                                       if(v.hashtagmentions) {

                                                v.hashtagmentions.forEach (function(val, idx) {
                                                p.hashtags[val] = (p.hashtags[val] || 0) + 1; //increment counts
                                        });}
/*
                                        if(v.keywords) {

                                                v.keywords.forEach (function(val, idx) {
                                                p.keywords[val] = (p.keywords[val] || 0) + 1; //increment counts
                                        });}
*/

                                return p;
                        }

                        function reduceRemove(p, v) {
                        //omitted. not useful for demonstration
		                                if(v.emotions) {
		                                        v.emotions.forEach (function(val, idx) {
		                                                p.emotions[val] = (p.emotions[val] || 0) - 1; //reduce counts
		                                        });
		                                }
                        				if(v.usermentions) {

                                                v.usermentions.forEach (function(val, idx) {
                                                	p.usermentions[val] = (p.usermentions[val] || 0) - 1; //increment counts
                                        		});

                               			 }
                                         if(v.hashtagmentions) {

                                                v.hashtagmentions.forEach (function(val, idx) {
                                                	p.hashtags[val] = (p.hashtags[val] || 0) - 1; //increment counts
                                         		});


                               			 }
/*
                                        if(v.keywords) {

                                                v.keywords.forEach (function(val, idx) {
                                                p.keywords[val] = (p.keywords[val] || 0) - 1; //increment counts
                                        });}
*/

                                 return p;
                                }
						function reduceInitial() {
			                         /* this is how our reduce function is seeded. similar to how inject or fold
			   							works in functional languages. this map will contain the final counts
			  							by the time we are done reducing our entire data set.*/
			                       return {emotions:{},usermentions:{},hashtags:{},keywords:{}};
			
			            }

 
    $scope.resetDC = function() {
    	dc.filterAll();
    	dc.renderAll();
    };
    
    $scope.submit = function(){
    	console.log($scope.keyword);
    	$scope.showResults = false;
    	$scope.showLoader=true;
    	var data= $scope.keyword;
        $scope.items=[];
        $scope.topResults=[];
         $scope.highInfluenceCount=0;
    	$scope.lowInfluenceCount=0; 
        //$scope.facts = crossfilter($scope.items);
        $scope.showChartsData=false;
        $scope.reinitChartsData=false;
       
        Product.findAll( {channel:"all",keyword:data},function(products) {
        	
            if(products.items.length > 0) {         
	            var format = d3.time.format("%d/%m/%Y");
	            $scope.items = $scope.items.concat(products.items);
	            $scope.facts = crossfilter($scope.items);
	            $scope.topResults=$scope.topResults.concat(products.topResults);
	            
	            var influenceDimension = $scope.facts.dimension(function(d) {
					
					return d.influence_count;
				});
				
				var influenceArray = influenceDimension.filterRange([1,Infinity]);
				var influenceArraySorted = influenceArray.bottom(Infinity);
				
				var medianInfluence = influenceArraySorted[Math.round(influenceArraySorted.length/2)];
				$scope.medianNumber = medianInfluence.influence_count;
				
				
				influenceDimension.filterAll(null);
				influenceDimension.dispose();
	           	
				var data = products.items;
				
				data.forEach(function(d) {
											
							d.dtg = format.parse(d.date);
							
							if(d.influence_count > $scope.medianNumber){
								 $scope.highInfluenceCount +=1;
								 d.influence_rank="High";
							} else {
								 $scope.lowInfluenceCount+=1;
								 d.influence_rank="Low";
							}
							if(d.sentiment =='negative')
		            			d.color= "#FB8072";
		            		else if(d.sentiment == 'positive')
		            			d.color = "#B3DE69";
		            		else if(d.sentiment == 'neutral')
			            		d.color = "#7282fb";
		            		else
		            			d.color="grey";
							
				});
				$scope.timesecondsDimension=$scope.facts.dimension(function(d) {
	            	return d.timeseconds;
	            });
	           // $scope.facts.add(data);
	            $scope.wordgroup = $scope.facts.groupAll().reduce(reduceAdd,reduceRemove,reduceInitial).value();
	            $scope.titleDimension = $scope.facts.dimension(function(d) {
                                return d.title;
                    });

                    
                    

				
	            $scope.showLoader=false;
	            $scope.showSearchBar=false;
	            $scope.sharedControl.showHeader=false;
	            $scope.showChartsData=true;
	            $scope.showCharts=true;
	            $scope.showSearchSummary=true;
            } else {
            	$scope.showLoader=false;
            	$scope.statusMessage = 'No mentions found for '+$scope.keyword;		
            	$scope.showStatus=true;
            	$timeout(function() { $scope.showStatus=false},3000);
            }
            
            
        }, function(err){
        	console.log("In Controller Error" + err.status);
        	
        	switch (err.status) {
                        case 401:
                            $scope.statusMessage = 'Error 401: You have exceeded the quota for the day. Please get in touch with support if you would like to get more access';
                            break;
                        case 403:
                            $scope.statusMessage = 'Error 403: You don\'t have the permission to do this';
                            break;
                        case 500:
                            $scope.statusMessage ='Error 500: Server internal error: ';
                            break;
                        default:
                            $scope.statusMessage = 'Error ' +err.status + ': Requested Resource Not Found';
            }
            $scope.showLoader=false;
            $scope.showStatus=true;
            $timeout(function() { $scope.showStatus=false},3000);
            
        	
        });
        
        
        	
        };
        
}]);


