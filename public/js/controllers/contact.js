
angular.module('mean.system').controller('ContactController', ['$scope', '$location','$timeout','Global','Contact', function ($scope, $location,$timeout,Global,Contact) {
	$scope.global = Global;
 
   
		$scope.contactStatus='';
    	
    	
    	
    
    	
    	
       
        $scope.create = function() {
	        var article = new Contact({
	            name: this.name,
	            email: this.email,
	            phone: this.phone,
	            message: this.message
	        });
	        article.$save(function(response) {
	            //$location.path('contact/' + response._id);
	            $scope.contactStatus='Thanks for your message. We will get back to you as soon as possible';
	            $scope.showContactStatus=true;
		        $timeout(function() { $scope.showContactStatus=false},3000);
		        },function(err){
	        	
	        		switch (err.status) {
                        case 401:
                            $scope.contactStatus = 'Error 401: You have exceeded the quota for the day. Please get in touch with support if you would like to get more access';
                            break;
                        case 403:
                            $scope.contactStatus = 'Error 403: You don\'t have the permission to do this';
                            break;
                        case 500:
                            $scope.contactStatus ='Error 500: Server internal error: ';
                            break;
                        default:
                            $scope.contactStatus = 'Error ' +err.status + ': Requested Resource Not Found';
            		}
            
		           $scope.showContactStatus=true;
		        $timeout(function() { $scope.showContactStatus=false},3000);
	        	
		        }
		        
	        	 
	        
	        );
				
	       
   	   };

    
    
}]);
