
angular.module('mean.system').config(function($stateProvider, $urlRouterProvider) {
        	var dashboard = {
        		name: 'dashboard',
                url: 'dashboard',
                templateUrl: 'views/masterdashboard.html'
                
                      		
        		
        	};
            // For unmatched routes:
           // $urlRouterProvider.otherwise('/');

			$urlRouterProvider.when('/logout',function($http,$state) {
				$http.get('/logout')
				 .success(function(user){
                    // authentication OK
                    $state.go('auth');
                })
                .error(function() {
                    //ToDO
                });
			});
			$urlRouterProvider.when('/dashboard','dashboard');
			$urlRouterProvider.when('/login','auth');
			$urlRouterProvider.when('', '/');
            // states for my app
            $stateProvider.state('auth', {
            	 url:'/',
	             templateUrl: 'views/index.html',
	             controller: function($state,$timeout){
	             	$timeout(function() { 
	             		
	             		
	             		console.log($state);console.log("Changing state to login")

						$state.go('auth.login').then(
							function(result) {
								
							},function(err) {
								//it means it is authenticated.
								$state.go('dashboard');
							}
						)
						;});
	             }
	           });
	          $stateProvider.state(dashboard);
            
               
        }
    );
  