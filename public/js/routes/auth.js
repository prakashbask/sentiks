//Setting up route
angular.module('mean.auth').config(['$stateProvider',
    function($stateProvider) {
        //================================================
        // Check if the user is not conntect
        //================================================
        var checkLoggedOut = function($rootScope,$q, $timeout, $http, $location,$state) {
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user) {
                // Authenticated
                if (user !== '0') {
                	
                    $timeout(function() {
                        deferred.reject();
                    }, 0);
                    $rootScope.user = user;
                    console.log("LOGGEDIN", $rootScope.user);
                    $rootScope.$emit('loggedin');
                   
                    $location.url('login');
                    
					//$state.go('dashboard');
                }

                // Not Authenticated
                else {
                    $timeout(deferred.resolve, 0);

                }
            });

            return deferred.promise;
        };
        //================================================
		var login = {
                name: 'auth.login',
                url: 'login',
                templateUrl: 'views/auth/login.html',
                resolve: {
                    loggedin: checkLoggedOut
                }
             },
             register = {
             	
             	name: 'auth.register',
                url: 'register',
                templateUrl: 'views/auth/register.html',
                resolve: {
                    loggedin: checkLoggedOut
                }
             	
             };

        // states for my app
        $stateProvider
            .state(login)
            .state(register);
    }
]);

