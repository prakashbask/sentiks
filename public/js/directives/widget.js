angular.module('mean.system')
  .directive('widget', ['$compile','$timeout', function ($compile,$timeout) {
    function findWidgetPlaceholder(element) {
      // widget placeholder is the first (and only) child of .widget-content
      return angular.element(element.find('.widget-content').children()[0]);
    }

    return {

      link: function (scope, element) {
        // first child of .widget-content
        var elm = findWidgetPlaceholder(element);
        var newScope;
		var compiledElement;
		var ds;

        // the widget model/definition object
        var widget = scope.widget;

        // set up data source
        if (widget.dataModelType) {
          //var ds = widget.ds;
           ds = new widget.dataModelType();
          widget.dataModel = ds;
          ds.setup(widget, scope);
          if(widget.config.status)ds.init();
          //scope.$on('$destroy', ds.destroy.bind(ds));
        }

        // .widget element (element is .widget-container)
        var widgetElm = element.find('.widget');

        // check for a template in widget def
        if (widget.templateUrl) {
          var includeTemplate = '<div ng-include="\'' + widget.templateUrl + '\'"></div>';
          var templateElm = angular.element(includeTemplate);
          elm.replaceWith(templateElm);
          elm = templateElm;
        } else if (widget.template) {
          elm.replaceWith(widget.template);
          elm = findWidgetPlaceholder(element);
        } else {
          elm.attr(widget.directive, '');

          if (widget.attrs) {
            _.each(widget.attrs, function (value, attr) {
              elm.attr(attr, value);
            });
          }

          if (widget.dataAttrName) {
            elm.attr(widget.dataAttrName, 'widgetData');
          }
        }

       

        // replaces widget title with input
        scope.editTitle = function (widget) {
          widget.editingTitle = true;
          // HACK: get the input to focus after being displayed.
          setTimeout(function () {
            widgetElm.find('form.widget-title input:eq(0)').focus()[0].setSelectionRange(0, 9999);
          }, 0);
        };

        // saves whatever is in the title input as the new title
        scope.saveTitleEdit = function (widget) {

          widget.editingTitle = false;
        };
        
        scope.resize = function(size) {
        	console.log("element",elm);
        	
        	angular.element(elm[0].children[0]).scope().resize(size);
        	
        
        	
        };
        
        scope.getMetrics = function() {
        	return angular.element(elm[0].children[0]).scope().getMetrics();
        	
        };
        
        scope.getData = function() {
        	return angular.element(elm[0].children[0]).scope().getData();
        };
        scope.saveDashboard = function() {
        	
        	scope.$parent.saveDashboard();
        	
        };
        
 		newScope = scope.$new();
       compiledElement = $compile(elm)(newScope);

        scope.$emit('widgetAdded', widget);
        
        scope.$on('$destroy', function() {
        	if(ds)ds.destroy.bind(ds);
	        console.log('scope on destroy');
	        newScope.$destroy(); 
	 		newScope = null; 
	 		compiledElement.off();
	 		compiledElement.remove();
	 		compiledElement=null;
	 		elm=null;
	 		ds=null;
      	});
      }
    };
  }]);