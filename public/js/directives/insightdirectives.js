



angular.module('mean.system')
.directive("packedCircle",function($timeout) {
	return {

	link:function(scope,elem,attrs) {
			
			var reduceAdd = function(p, v) {
                                if(v.emotions) {
                                        v.emotions.forEach (function(val, idx) {
                                                p.emotions[val] = (p.emotions[val] || 0) + 1; //increment counts
                                        });
                                }
                                if(v.usermentions) {

                                                v.usermentions.forEach (function(val, idx) {
                                                p.usermentions[val] = (p.usermentions[val] || 0) + 1; //increment counts
                                        });

                                }
                                       if(v.hashtagmentions) {

                                                v.hashtagmentions.forEach (function(val, idx) {
                                                p.hashtags[val] = (p.hashtags[val] || 0) + 1; //increment counts
                                        });}
/*
                                        if(v.keywords) {

                                                v.keywords.forEach (function(val, idx) {
                                                p.keywords[val] = (p.keywords[val] || 0) + 1; //increment counts
                                        });}
*/

                                return p;
                        }

                var reduceRemove = function(p, v) {
                        //omitted. not useful for demonstration
		                                if(v.emotions) {
		                                        v.emotions.forEach (function(val, idx) {
		                                                p.emotions[val] = (p.emotions[val] || 0) - 1; //reduce counts
		                                        });
		                                }
                        				if(v.usermentions) {

                                                v.usermentions.forEach (function(val, idx) {
                                                	p.usermentions[val] = (p.usermentions[val] || 0) - 1; //increment counts
                                        		});

                               			 }
                                         if(v.hashtagmentions) {

                                                v.hashtagmentions.forEach (function(val, idx) {
                                                	p.hashtags[val] = (p.hashtags[val] || 0) - 1; //increment counts
                                         		});


                               			 }
/*
                                        if(v.keywords) {

                                                v.keywords.forEach (function(val, idx) {
                                                p.keywords[val] = (p.keywords[val] || 0) - 1; //increment counts
                                        });}
*/

                                 return p;
                                }
						var  reduceInitial = function() {
			                         /* this is how our reduce function is seeded. similar to how inject or fold
			   							works in functional languages. this map will contain the final counts
			  							by the time we are done reducing our entire data set.*/
			                       return {emotions:{},usermentions:{},hashtags:{},keywords:{}};
			
			            }
	 var remove = function() {
	 	var svg = d3.select(elem[0].firstElementChild);
		if(svg)svg.remove();
	 }
	 var init = function(size) {
	 	   
		   scope.wordgroup = scope.facts.groupAll().reduce(reduceAdd,reduceRemove,reduceInitial).value();
			var data=[];
			for(var type in scope.wordgroup.emotions){
	    			data.push({'key':type, 'count': scope.wordgroup.emotions[type],'category':'Emotion'})
			}
			for(var type in scope.wordgroup.usermentions){
	    			data.push({'key':type, 'count': scope.wordgroup.usermentions[type],'category':'UserMentions'})
			}
			for(var type in scope.wordgroup.hashtags){
	    			data.push({'key':type, 'count': scope.wordgroup.hashtags[type],'category':'Hashtags'})
			}
			/*
			                var keywordcount=0;
					for(var type in scope.wordgroup.keywords){
						if(scope.wordgroup.keywords[type] > 1) {
							keywordcount +=1;
						if(keywordcount < 50) {
			    				data.push({'key':type, 'count': scope.wordgroup.keywords[type],'category':'keyword'})
						}
						}
					}
			*/
				
				 var width = scope.size.width || elem[0].clientWidth || 200	, height = scope.size.height || elem[0].clientHeight || 200;
			        var fill = d3.scale.ordinal()
			          .domain(["Emotion", "Keyword", "Hashtags","UserMentions"])
			           .range(["#CA4D3F","#7282fb","#B3DE69","#3878BD"]);
					var target = elem[0];
					
					var svg = d3.select(target.firstElementChild);
					if(svg)svg.remove();
			        var svg = d3.select(target).append("svg")
			            .attr("width", width)
			            .attr("height", height);
			
			        var maxcount = d3.max(data, function (d) { return parseInt(d.count)})
			        var radius_scale = d3.scale.pow().exponent(0.5).domain([0, maxcount]).range([2, 85])
			
			        _.each(data, function (elem) {
			          elem.radius = radius_scale(elem.count)*.3;
				  //elem.radius = +elem.count/2;
			          elem.all = 'All';
			          elem.x = _.random(0, 1000);
			          elem.y = _.random(0, 1000);
			        })
			
			        var padding = 4;
			        var maxRadius = d3.max(_.pluck(data, 'radius'));
			        //var maxRadius = d3.max($.map(data, function(o) { return o["radius"]; }));
				 function getCenters(vname, w, h) {
			          var v = _.uniq(_.pluck(data, vname)), c =[];
			          var l = d3.layout.pack().size([w, h]);
			          _.each(v, function (k, i) { c.push({name: k, value: 1}); });
			          return _.object(v,l.nodes({children: c})[0].children);
			        }
			
			       // var all_center = { "all": {name:"All Keywords", x: 500, y: 300}};
			
			        var nodes = svg.selectAll("circle")
			          .data(data);
			
			        nodes.enter().append("circle")
			          .attr("class", "node")
			          .attr("cx", function (d) { return d.x; })
			          .attr("cy", function (d) { return d.y; })
			          .attr("r", 10)
			          .style("fill", function (d) { return fill(d.category); })
			          .on("mouseover", function (d) { showPopover.call(this, d); })
			          .on("mouseout", function (d) { removePopovers(); })
			          .on("click", function (d, i) { scope.showNewEvents(d.key); })
			
			        nodes.transition().delay(500).duration(1000)
			          .attr("r", function (d) { return d.radius; })
			
			        var force = d3.layout.force()
			          .charge(0)
			          .gravity(0)
			          .size([width, height])
			
			        draw('all');
			
			        $( ".bubble" ).click(function() {
			          draw(this.id);
			        });
			
			        function draw (varname) {
			          //var foci = varname === "all" ? all_center: year_centers;
				  var foci = getCenters(varname, width, height);
			          force.on("tick", tick(foci, varname, .85));
			          labels(foci)
			          force.start();
			        }
			
			        function tick (foci, varname, k) {
			          return function (e) {
			            data.forEach(function(o, i) {
			              var f = foci[o[varname]];
			              o.y += (f.y - o.y) * k * e.alpha;
			              o.x += (f.x - o.x) * k * e.alpha;
			            });
			            nodes
			              .each(collide(.2))
			              .attr("cx", function (d) { return d.x; })
			              .attr("cy", function (d) { return d.y; });
			          }
			        }
			
			        function labels (foci) {
			          svg.selectAll(".label").remove();
			
			          svg.selectAll(".label")
			          .data(_.toArray(foci)).enter().append("text")
			          .attr("class", "label")
			          .text(function (d) { return d.name })
			          .attr("transform", function (d) {
			            //return "translate(" + (d.x - ((d.name.length)*3)) + ", " + (d.y - 275) + ")";
			            return "translate(" + (d.x - ((d.name.length)*3)) + ", " + (d.y - d.r) + ")";
			          });
			        }
			
			        function removePopovers () {
			          $('.popover').each(function() {
			            $(this).remove();
			          }); 
			        }
			
			        function showPopover (d) {
			          $(this).popover({
			            placement: 'auto top',
			            container: 'body',
			            trigger: 'manual',
			            html : true,
			            content: function() { 
			              return "Word: " + d.key; }
			               //+ "<br/>Count: " + d3.format(",")(+d.count); 
			          });
			          $(this).popover('show')
			        }
			
			        function collide(alpha) {
			          var quadtree = d3.geom.quadtree(data);
			          return function(d) {
			            var r = d.radius + maxRadius + padding,
			                nx1 = d.x - r,
			                nx2 = d.x + r,
			                ny1 = d.y - r,
			                ny2 = d.y + r;
			            quadtree.visit(function(quad, x1, y1, x2, y2) {
			              if (quad.point && (quad.point !== d)) {
			                var x = d.x - quad.point.x,
			                    y = d.y - quad.point.y,
			                    l = Math.sqrt(x * x + y * y),
			                    r = d.radius + quad.point.radius + padding;
			                if (l < r) {
			                  l = (l - r) / l * alpha;
			                  d.x -= x *= l;
			                  d.y -= y *= l;
			                  quad.point.x += x;
			                  quad.point.y += y;
			                }
			              }
			              return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
			            });
			          };
			        }
			
			    
				  }
				 
					 scope.$watch('showKeyword', function(newValue, oldValue){

                                if(newValue) {
                                        
                                        $timeout(function() {init(scope.size)});
                                } else {
                                	    $timeout(function() {remove()});
                                }
                        });
                        
                   
 						scope.$on('widgetResized', function(event,size){
									   event.stopPropagation();
                              		   remove();
                                        $timeout(function() {init(size)});
                                 
                        });
                        scope.$on('widgetInited', function(){
									   
                              		   remove();
                                        $timeout(function() {init(scope.size)});
                                 
                        });
					
					

	
	 	}
	}

});
