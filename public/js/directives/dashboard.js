



angular.module('mean.system')
.directive('dashboard', ['WidgetModel', 'WidgetDefCollection', '$modal','$timeout', 'DashboardState','WidgetResource','AccountResource','Global', function (WidgetModel, WidgetDefCollection, $modal, $timeout,DashboardState,WidgetResource, AccountResource,Global) {
        return {
              restrict: 'A',
			  templateUrl:'views/template/dashboard.html',
              scope: true,
		      controller: function ($scope) {
		        $scope.sortableOptions = {
		          stop: function () {
		            //TODO store active widgets in local storage on add/remove/reorder
		            $scope.dashboardState.save($scope.widgets);
		          },
		          handle: '.widget-header'
		        };
		        
		        $scope.launchView = function() {
		        	var options = {
			              templateUrl: 'views/template/widget-view.html',
			              resolve: {
			                widgets: function () {
			                  return $scope.widgets;
			                },
			                Global: function() {
		                	return $scope.global;
		                	}
			              },
			              controller: 'WidgetViewController',
			              windowClass: 'widget-view'
			           };
			          
			          
			          
					var modalInstance = $modal.open(options);
						
						          // Set resolve and reject callbacks for the result promise
						   modalInstance.result.then(
						            function (result) {
						             
						
						            },
						            function (reason) {
						
						
						            }
						        );
		        	
		        };
		        
		      },
		      link: function (scope, element, attrs) {
		      	scope.global = Global;
		      	scope.showDashIcons = true;
		      	scope.widgets=[];
		      	
		        scope.options = scope.$eval(attrs.dashboard);
		        scope.defaultWidgets = scope.options.defaultWidgets; // save widgets for reset
		       // scope.widgetDefs = scope.options.widgetDefinitions;
		        scope.widgetDefs = new WidgetDefCollection(scope.options.widgetDefinitions);
		        scope.dashboard = scope.options.dashboard;
		
		        var count = 1;
		        var dashboardState = scope.dashboardState = new DashboardState(
		          !!scope.options.useLocalStorage,
		          scope.defaultWidgets
		        );
		
				scope.setDashIcon = function() {
					scope.showDashIcons = !scope.showDashIcons;
				}
		
		        scope.addWidget = function (widgetDef,initMode,selfLoad,dimensions) {
		          
		
		          var wDef = scope.widgetDefs.getByType(widgetDef.type);
		          if (!wDef) {
		            throw 'Widget ' + widgetDef.name + ' is not found.';
		          }
				  var title = wDef.title ? wDef.title : ('Widget ' + count++);
				 
				  //hack
				 // wDef.config.accounts.list = [];
		          //wDef.config.storage.list=['Passthru','Remote'];
		          
		          var w = angular.copy(wDef);
		          
		          if(dimensions) {
		          	angular.extend(w.dimensions,dimensions);
		          }
		          angular.extend(w, widgetDef); //TODO deep extend
				  
		          var widget = new WidgetModel(w, {
		            title: title
		          });
		         
		          
		          
		          var wr = new WidgetResource({
		          	dashboardId:scope.dashboard.selectedDashboard._id,
		          	//id:Math.floor((Math.random()*1000)+1),
		          	type:wDef.type,
		          	title:title,
		          	config:angular.copy(wDef.config),
		          	dimensions:dimensions || wDef.dimensions
		          	
		          });
		       
		           
		           wr.$save(function(response) {
			            //$location.path('contact/' + response._id);
			            console.log("Saved successfully",response);
			            widget.wr = response;
			            scope.saveDashboard();
				        },function(err){
			        	
			        		
		            	}
		            	
				    );
				    
				  console.log("widget added", widget);
		          scope.widgets.push(widget);
		          if(!initMode)
		          scope.openWidgetDialog(widget);
		          if(selfLoad) {
		          	  var conf = {};
    				  conf.config = angular.copy(widget.config);
					  conf.title = widget.title;
					  conf.type = widget.type;
		          	  conf.config.accounts.selected = scope.global.data.user.name;
		          	  conf.config.status=true; 
		          	 
		             $timeout(function() {
		             	widget.dataModel.updateConfig(conf);
		             },500);
		          }
		          
		        };
		
		        scope.removeWidget = function (widget) {
		        	
			        var options = {
			              templateUrl: 'views/template/widget-remove-confirmation.html',
			              resolve: {
			                widget: function () {
			                  return widget;
			                }
			              },
			              controller: 'WidgetRemoveCtrl',
			              windowClass: 'configure-widget'
			           };
			          
			          
			          
			          var modalInstance = $modal.open(options);
			
			          // Set resolve and reject callbacks for the result promise
			          modalInstance.result.then(
			            function (result) {
			              
			              console.log('result: ', result);
			               scope.widgets.splice(_.indexOf(scope.widgets, widget), 1);
			          
				           var wr = widget.wr;
				           wr.$remove(function(response){
				           		console.log("Deleted Successfully");
				           		},function(err) {
				           			
				           			
				           		}
				           );
				           scope.saveDashboard();
			            },
			            function (reason) {
			              console.log('widget dialog dismissed: ', reason);
			
			            }
		          );
		          
		        };
		
				 scope.refreshWidget = function (widget) {
		        	
		        	console.log("Refresh",widget,element);
		        	if(widget.config.status)
			        	widget.dataModel.init();
		        };
		        scope.openWidgetDialog = function (widget) {
		          var defaultOptions = widget.editModalOptions;
		
		          // use default options when none are supplied by widget
		       	   
		           var options = {
		              templateUrl: 'views/template/widget-template.html',
		              resolve: {
		                widget: function () {
		                  return widget;
		                },
		                optionsTemplateUrl: function () {
		                  return scope.options.optionsTemplateUrl;
		                },
		                Global: function() {
		                	return scope.global;
		                }
		              },
		              controller: 'WidgetDialogCtrl',
		              windowClass: 'configure-widget'
		           };
		          angular.extend(options,defaultOptions);
		          /*
		          _.each(scope.global.data.storageaccounts,function(account){
		          		widget.config.storage.list.push(account.name);
		          });*/
		          
		          
		          var modalInstance = $modal.open(options);
		
		          // Set resolve and reject callbacks for the result promise
		          modalInstance.result.then(
		            function (result) {
		              console.log('widget dialog closed');
		              console.log('result: ', result);
		              widget.title = result.title;
		              widget.dataModel.updateConfig(result);
		            },
		            function (reason) {
		              console.log('widget dialog dismissed: ', reason);
		
		            }
		          );
		
		        };
		
		        scope.clear = function () {
		          scope.widgets = [];
		          scope.defaultWidget = []
		        };
		
		        scope.addWidgetInternal = function (event, widgetDef) {
		          event.preventDefault();
		          scope.addWidget(widgetDef);
		        };
		         scope.addWidgetDefault = function (event) {
		         	
		
		          // use default options when none are supplied by widget
		          
		            options = {
		              templateUrl: 'views/template/widget-configure-template.html',
		              
		              controller: 'WidgetConfigureCtrl'
		            };
		          
		          var modalInstance = $modal.open(options);
		
		          // Set resolve and reject callbacks for the result promise
		          modalInstance.result.then(
		            function (result) {
		              console.log('widget dialog closed');
		              console.log('result: ', result);
		             
		              scope.addWidget(result);
		            
		            },
		            function (reason) {
		              console.log('widget dialog dismissed: ', reason);
		
		            }
		          );
		         	/*
		          event.preventDefault();
		          scope.addWidget(scope.options.defaultWidgetDef);
		          */
		        };
		
		        scope.saveDashboard = function () {
		          dashboardState.save(scope.options.dashboard.selectedDashboard,scope.widgets);
		         
		        };
		        
				scope.getDashboardState = function(key) {
					return dashboardState.loadState(key);
				};
		        scope.loadWidgets = function (widgets) {
			          scope.defaultWidgets = widgets; // save widgets for reset
			          scope.clear();
			          
			          
				          _.each(widgets, function (widgetDef) {
				            scope.addWidget(widgetDef);
				      	
		          			});
		         	
		        };
		         scope.init = function (widgets) {
		         	 
			         
			         		 scope.clear();
			         		 
			         		var dashboardState = scope.getDashboardState(scope.global.data.user._id);
			         		//this is a temp map so that widget can be laid out as per the row number 
			         		var initWidgetMap={};
			          		$timeout(function() {	          
					          _.each(widgets, function (widgetDef) {
					            	var wDef = scope.widgetDefs.getByType(widgetDef.type);
							          if (!wDef) {
							            throw 'Widget ' + widgetDef.type + ' is not found.';
							          }
									  
							          var w = angular.copy(wDef);
							          angular.extend(w, widgetDef); //TODO deep extend
									  
									 
									  if(dashboardState) {
									  	widgetDef._id
									  	var widgetState = dashboardState.widgets[widgetDef._id];
									  	if(widgetState && widgetState.dimensions)
									  		angular.extend(w.dimensions,widgetState.dimensions);
									  }
									  
							          var widget = new WidgetModel(w);
							          
							          widget.wr=widgetDef;
							          
							          if(!initWidgetMap[widget.dimensions.row])
							          	initWidgetMap[widget.dimensions.row]=[];
							          
							          initWidgetMap[widget.dimensions.row].push(widget);
							          //scope.widgets.push(widget);
							         
							          
					      	
			          			});
			          			var keys = _.keys(initWidgetMap);
			          			_.each(keys,function(key){
			          					_.each(initWidgetMap[key],function(widget){
			          				   		scope.widgets.push(widget);
			          				 	});
			          			});
		         				scope.defaultWidgets = scope.widgets;
		         				scope.saveDashboard();
		         			},500);
		         			 
		        };
		
		/*
		        scope.resetWidgetsToDefault = function () {
		          scope.loadWidgets(scope.defaultWidgets);
		        };
		
		        // Set default widgets array
		        var savedWidgets = dashboardState.load();
		
		        if (savedWidgets) {
		          scope.widgets = savedWidgets;
		        } else if (scope.defaultWidgets) {
		          scope.resetWidgetsToDefault();
		        }
		*/
		        // allow adding widgets externally
		        scope.options.addWidget = scope.addWidget;
		        scope.options.loadWidgets = scope.loadWidgets;
		        scope.options.init = scope.init;
		        scope.options.clear = scope.clear;
		        scope.options.saveDashboard = scope.saveDashboard;
		        scope.options.getDashboardState = scope.getDashboardState;
		 		/*if(scope.widgets.length == 0) {
			          	 $('#AddWidgetAction').popover('show');
			     } */
		        // save state
		        scope.$on('widgetChanged', function (event) {
		          event.stopPropagation();
		          scope.saveDashboard();
			         
		        });

		      }
		    };
  }]);










