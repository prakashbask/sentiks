angular.module('mean.system')
  .directive('search', function ($interval,$timeout,WidgetDataResource) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        chart: '='
      },
      templateUrl: 'views/widgets/search/search.html',
      controller: function($scope,$element,$attrs){

      		$scope.showSentiment = false;
		    $scope.container={
					positive: {
						options: {
							colorStart: '#B3DE69',   // Colors
							colorStop: '#B3DE69'
						},
						percent:0
				   },
				   negative: {
						options: {
							colorStart: '#FB8072',   // Colors
							colorStop: '#FB8072'
						},
						percent:0
				   },
				   neutral: {
						options: {
							colorStart: '#7282fb',   // Colors
							colorStop: '#7282fb'
						},
						percent:0
				   },
				   metrics: {}

			    };






		   $scope.destroySentPanel = function() {

		   };
		     $scope.destroyMetricPanel = function() {

		   };

			$scope.initSentPanel =function() {
				 var totalCount = $scope.events.length;
				 var positiveCount=0;
				 var negativeCount=0;
				 var neutralCount=0;
				 console.log("Initing Sentiment Panel");
				 if($scope.topResults) {
				 	$scope.topResults.forEach(function(result){

				 		positiveCount += result.positivecount;
				 		negativeCount += result.negativecount;
				 		neutralCount += result.neutralcount;

				 	});
				 }

				 $scope.container.positive.percent=Math.floor((positiveCount/totalCount)*100);
				 $scope.container.negative.percent=Math.floor((negativeCount/totalCount)*100);
				 $scope.container.neutral.percent=Math.floor((neutralCount/totalCount)*100);


			};
			var populateMetricEvent = function(metricEvent) {
				_.each(metricEvent.metrics,function(val,key) {

					//console.log(val,key);
					$scope.container.metrics[key].push(val);
				});




			}
			$scope.initMetricPanel =function() {
					$scope.timesecondsDimension=$scope.facts.dimension(function(d) {
		            	return d.timeseconds;
		            });
				  $scope.metricEvents=$scope.timesecondsDimension.top(Infinity);
				  $scope.redraw=false;
				  $scope.container.metrics={};
				  if($scope.metricEvents.length > 0) {
				  	var firstEvent  = $scope.metricEvents[0].metrics;
				  	var keys = _.keys(firstEvent);
				  	_.each(keys, function(key){
				  		$scope.container.metrics[key]=[];
				  	});

					 _.each($scope.metricEvents,function(metricEvent) {
					  		populateMetricEvent(metricEvent);
					 });
					//$scope.$apply();
				  }
		  		 $scope.redraw=true;
			}

			 $scope.$watch('showSentiment', function(newValue, oldValue){

 				if(newValue)
 					$timeout( function() {$scope.initSentPanel()},1000);

		   	 	else
		   	 		$scope.destroySentPanel();

 			});
 			 $scope.$watch('showMetric', function(newValue, oldValue){

 				if(newValue)
 					$timeout( function() {$scope.initMetricPanel()},100);

		   	 	else
		   	 		$scope.destroyMetricPanel();

 			});
 				$scope.$on('widgetInited',function() {

 				if($scope.showSentiment)$scope.initSentPanel();
 				if($scope.showMetric)$scope.initMetricPanel();

 			});


      },
      link: function postLink(scope,elem) {
      	  scope.showConfigure = false;
      	  scope.showWidgetError = false;
      	  scope.showLoader = false;
      	  scope.size = {width: 350, height: 250}
      	  var retryCount = 0;
      	  var data=[];
      	  var svg;
      	  scope.events = [];
      	  scope.metricEvents = [];
		  scope.items=[];
		  scope.topResults=[];
		    scope.setItem = function(event){
				scope.items.pop();
		    	scope.items.push(event);
			};

			scope.destroy=function() {
				scope.events.length=0;
				scope.items.length=0;
				scope.events=[];
				scope.items=[]
				scope.metricEvents = [];
				 scope.items=[];
				scope.$apply();
			}
			 scope.buildUrl = function(source) {

                  return '/views/widgets/search/'+source.toLowerCase() + '.html';

      		 }

			scope.getIconClass=function(category) {
				var category=category.toLowerCase();
				var color= category+'C';
				if(category == 'youtube') category="youtube-play"; //map it to fontawesome icon
				var iconclass="fa fa-" + category + " "+color;
				return iconclass;
			}
			scope.showNewEvents=function(key) {



				//var results = $.grep($scope.items, function(e){ return e.title == key; });
				var reg = new RegExp(key,'i');
				var resultsD = scope.titleDimension.filter(function(d,index) {
		           if(d) {
		              return reg.test(d);
		           }else return false;
		        });
		        //console.log("Results",resultsD.top(Infinity));
		        scope.events=[];
		        scope.events=scope.events.concat(scope.timesecondsDimension.top(Infinity));

		        scope.$apply();
		        scope.titleDimension.filter(null);

			}
			scope.openWidgetDialog = function(widget) {
      	  	scope.$parent.openWidgetDialog(scope.$parent.widget);
      	  	}

      	  scope.init = function(chart) {
      	  		  console.log("In search chart inint")
                scope.showWidgetError=false;
      	  		  scope.showLoader=true;

      	  		  scope.showNoData = false;

      	  		  scope.events.length=0;
					scope.items.length=0
					scope.events=[];
			        scope.topResults=[];
			        scope.metricEvents = [];
		  			scope.items=[];
		      	  //scope.time = new Date().toLocaleTimeString();

		      	  //scope.$parent.widget.title=chart.title;
		      	  /*
		      	  scope.$parent.widget.config.accounts.selected=chart.config.accounts.selected;
		      	  scope.$parent.widget.config.metrics.selected=chart.config.metrics.selected;
		      	  scope.$parent.widget.config.chartType.selected=chart.config.chartType.selected;
		      	  */
		      	 // angular.extend(scope.$parent.widget.config, chart.config);
		      	 // console.log("Parent Modified", scope.$parent.widget);
		      	 var today = Date.now();
		      	 var before = today - 604800000;


		      	  WidgetDataResource.getData({widgetId: scope.$parent.widget.wr._id,
		      	  							  dashboardId: scope.$parent.widget.wr.dashboardId,
		      	  							  st:today,
		      	  							  et:before

		      	  	},function(data) {
            			//console.log("Widget Data successfull");

            			if(data && data.length == 0 && retryCount < 1) {
            				//set interval
            				retryCount ++;
            				$timeout(function() {scope.init(chart);},5000);
            			} else {
            				//cancel interval
            				retryCount = 0;
            				scope.showLoader = false;
            				if(data && data.length > 0 && data[0].data && data[0].data.items && data[0].data.items.length > 0) {
            					scope.showNoData = false;
            					initData(data);
            					//scope.$apply();
            					scope.$emit('widgetInited');
            				} else
            					scope.showNoData = true;
            			}

        		  },function(err) {
        		  		//console.log("Widget Data unsuccessfull");
                  scope.showLoader = false;
                  scope.widgetErrorMsg = "Error in fetching data. Please try again";
                  scope.showWidgetError=true;
        		  });

        		   function initData(widgetdata) {


			         // console.log(elem);

			         processData(widgetdata);


					}

			}
			var processData = function(data) {

				_.each(data, function (item) {
					_.each (item.data.items,function(mention) {
			        	scope.events.push(mention);
			      	});
			        scope.topResults.push(item.data.topResults[0]);

			      });
			       scope.facts = crossfilter(scope.events);



			};
			scope.update = function(result) {
				if(data.length === 0) {
					scope.init();
				} else {

				}

			}

			scope.getMetrics = function() {
				return [];
			};

			scope.getData = function() {
				return {};
			};

			scope.resize = function(size) {
				//chart.update();
				//console.log(elem);
				//elem.slimScroll({distance: '0px',
 				//			size: '3px'});
 				var scrollableElem = angular.element(elem.find('.scrollcontainer'));
	        	//var scrollableElem = $('.timeline');

	        	var newHeight = (size.height < 200)? '110px':Math.floor(size.height*0.8) + 'px';
	        	var parentHeight = size.height  + 'px';
	        	//var newHeight = '80%';

	        	if(scrollableElem){
	        		scrollableElem.css({height:newHeight});
	 				scrollableElem.parent().css({height:parentHeight});
	        	}
	        	scope.size.width = size.width;
	        	scope.size.height = size.height


 				scope.$emit('widgetResized',scope.size);
			}
			 scope.$watch('chart', function (chart) {


		          if (chart) {
		          	scope.showConfigure = false;
		          	chart.state = chart.state || "init";
		            if(chart.state== "init")$timeout(function() {scope.init(chart);},100);
		            else if(chart.state == "update") $timeout(function() {scope.update(chart);},100);

		          } else {
		          	scope.showConfigure = true;
		          }
		     });

		     scope.$on('$destroy', function() {
	        	//console.log("Twitter directive destory")

		       if(svg) {
		       	 //console.log("Removing svg");
		       	 svg.remove();
		         svg=null;
		        }
		      // console.log(nvchart);
		       data=[];
		       data=null;

		    	scope.destroy();



	      	});
      }
    };
  });
