angular.module('mean.system')
  .directive('viewmetrics', function ($interval,$timeout,WidgetDataResource,ChartFactory) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        data: '='
      },
           
      link: function (scope,elem) {
      	
      	  scope.handlerMap={};
      	 
      	  
      	  
      	  
      	  scope.chartInstance = ChartFactory.chart();
      	 
      	  
      	  
      	  
      	       	  
      	  
      	  var initLine = function(data) {
      	  	scope.includeGraph = true;
      	  	$timeout(function() {
      	  		var target = elem.find('#graph')[0];
	      	  	scope.chartInstance.initLine(data,target);	
      	  	});
      	  };
      	  
      	  var initBar = function(data) {
      	  	scope.includeGraph = true;
      	  	$timeout(function() {
      	  		var target = elem.find('#graph')[0];
      	  		scope.chartInstance.initBar(data,target);
      	  	});
      	  };
      	  
      	  var initDetails = function(widgetdata) {
      	  	scope.includeDetails = true;
      	  
      	  	if(widgetdata.length > 0 ) {
      	  		scope.metrics = widgetdata[widgetdata.length - 1].data;
			}
      	  
      	  };
      	  
      	  var initList = function() {
      	  	scope.includeList = true;
      	  };
      	  
      	  var initPie = function() {
      	  	scope.includeGraph = true;
      	  };
      	  
      	  var initMap = function() {
      	  	scope.includeGraph = true;
      	  }
      	  
      	  scope.handlerMap['Line']=initLine;
      	  scope.handlerMap['Bar']=initBar;
      	  scope.handlerMap['Details']=initDetails;
      	  scope.handlerMap['Map']=initMap;
      	  scope.handlerMap['List']=initList;
      	  scope.handlerMap['Pie']=initPie;
      	  
      	  
      	  
      	  scope.init = function(widgetdata) {
      	  
      	  		  
		    	
		    	scope.data=widgetdata;
		    	//processData(data);
		    	var chartType='Line';
		      	scope.handlerMap[chartType](scope.data);
		      	  	
		      	  	
            		
			        
            		
		  }
			

						
		
			
					
		
			 scope.$watch('data', function (data) {
		          
		
		          if (data && data.list) {
		          	console.log(data);
		            scope.init(data);
		            
		          }
		     });
		     scope.$on('$destroy', function() {
	        	
	           scope.chartInstance.destroy();  
	           if(scope.data) {
		       	scope.data.list=[]
		       	scope.data=null;
		       }
		       
		       scope.chartInstance = null;
		       //scope.$apply();
		 		
	      	});
      }
    };
  });
