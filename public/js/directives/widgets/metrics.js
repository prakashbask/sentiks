angular.module('mean.system')
  .directive('metrics', function ($interval,$timeout,WidgetDataResource,ChartFactory) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        chart: '='
      },
      templateUrl: 'views/widgets/metrics/metrics.html',

      link: function postLink(scope,elem) {

      	  scope.handlerMap={};

      	  scope.showConfigure = false;
      	  scope.showLoader = false;
          scope.showWidgetError = false;
      	  //scope.includeGraph = true;
      	 // scope.includeDetails = true;
      	  scope.includeList = false;
      	  scope.chartInstance = ChartFactory.chart();
      	  scope.data={
      	  	min_date:'',
      	  	max_date:'',
      	  	list:[]
      	  };
      	  var svg;


      	  var retryCount = 0;

      	  scope.openWidgetDialog = function(widget) {
      	  	scope.$parent.openWidgetDialog(scope.$parent.widget);
      	  }


      	  var initLine = function(data) {
      	  	scope.includeGraph = true;
      	  	$timeout(function() {
      	  		var target = elem.find('#graph')[0];
	      	  	scope.chartInstance.initLine(data,target);
      	  	});
      	  };

      	  var initBar = function(data) {
      	  	scope.includeGraph = true;
      	  	$timeout(function() {
      	  		var target = elem.find('#graph')[0];
      	  		scope.chartInstance.initBar(data,target);
      	  	});
      	  };

      	  var initDetails = function(widgetdata) {
      	  	scope.includeDetails = true;

      	  	if(widgetdata.length > 0 ) {
      	  		scope.metrics = widgetdata[widgetdata.length - 1].data;
			}

      	  };

      	  var initList = function() {
      	  	scope.includeList = true;
      	  };

      	  var initPie = function() {
      	  	scope.includeGraph = true;
      	  };

      	  var initMap = function() {
      	  	scope.includeGraph = true;
      	  }

      	  scope.handlerMap['Line']=initLine;
      	  scope.handlerMap['Bar']=initBar;
      	  scope.handlerMap['Details']=initDetails;
      	  scope.handlerMap['Map']=initMap;
      	  scope.handlerMap['List']=initList;
      	  scope.handlerMap['Pie']=initPie;



      	  scope.init = function(chart) {
                scope.showWidgetError=false;
      	  		  scope.showLoader=true;
      	  		  scope.data={
      	  		  	min_date:'',
      	  		  	max_date:'',
      	  		  	list:[]
      	  		  }
		      	  var today = Date.now();
		      	  var before = today - 604800000;


		      	  WidgetDataResource.getData({widgetId: scope.$parent.widget.wr._id,
		      	  							  dashboardId: scope.$parent.widget.wr.dashboardId,
		      	  							  st:today,
		      	  							  et:before

		      	  	},function(data) {

            			if(data.length == 0 && retryCount < 3) {
            				//set interval
            				$timeout(function() {scope.init(chart);},5000);
            				retryCount++;
            				//$interval(function() {scope.init(chart),5000,5})
            			} else {
            				//cancel interval
            				retryCount = 0;
            				scope.showLoader = false;
            				processData(data);
			                var chartType = scope.$parent.widget.wr.config.chartType.selected;
			            	scope.handlerMap[chartType](scope.data);
			            	//TODO:initialize details only when it is a time series graph
			            	if(isTimeSeriesChart(chartType))scope.handlerMap['Details'](data);

            			}

        		  },function(err) {
        		  		//console.log("Widget Data unsuccessfull");
                  scope.showLoader = false;
                  scope.widgetErrorMsg = "Error in fetching data. Please contact support.";
                  scope.showWidgetError = true;
        		  });

			}

			scope.update = function(result) {
				//TODO: detect exact change in the config
				if(scope.data.list.length >= 0) {
					scope.init();
				} else {
					scope.handlerMap[result.config.chartType.selected](scope.data);

				}

			}

			var processData = function(widgetdata) {

				var metricMap = {};
				_.each(widgetdata, function (item) {

					_.each(item.data,function(val,key){
						if(!metricMap[key]) metricMap[key]=[];
						metricMap[key].push({x:item.createdAt,y:val});
					});

			      });

				var format = d3.time.format("%Y-%m-%dT%H:%M:%S.%LZ");

			      var maxDate,minDate;

			      if(widgetdata.length > 0) {
			       maxDate = new Date(widgetdata[widgetdata.length - 1].createdAt);
			       minDate = d3.time.day.offset(maxDate,-30); //TODO set it based on filter

			      }

			  	scope.data.min_date=minDate;
			  	scope.data.max_date=maxDate
			  		_.each(metricMap,function(val,key){
			  			scope.data.list.push({
			  				"key":key,
			  				"values":val,
			  				"area":true
			  			});
			  		});


			};

			var isTimeSeriesChart = function(chartType) {
				if(chartType === 'Line' ||
					chartType === 'Bar')
					return true;
			};
			scope.resize = function() {
				//chart.update();
			}

			scope.getMetrics = function() {
				var keys = [];
				_.each(scope.data.list,function(item){
					keys.push(item.key);
				})
				return keys;

			};

			scope.getData= function() {
				return scope.data;
			};
			 scope.$watch('chart', function (chart) {


		          if (chart) {
		          	scope.showConfigure = false;
		          	chart.state = chart.state || "init";
		            if(chart.state== "init")$timeout(function() {scope.init(chart);},100);
		            else if(chart.state == "update") $timeout(function() {scope.update(chart);},100);
		          } else {
		          	scope.showConfigure = true;
		          }
		     });
		     scope.$on('$destroy', function() {

	           scope.chartInstance.destroy();
		       scope.data.list=[]
		       scope.data=null;
		       scope.metrics = [];
		       scope.metrics=null;
		       scope.chartInstance = null;
		       //scope.$apply();

	      	});
      }
    };
  });
