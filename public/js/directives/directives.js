



angular.module('mean.system')
 .directive("resultsContainer", function() {
 	
 	return {
 		templateUrl:'views/results.html'
 		 		
 	}
 })
  .directive("includeConfiguretemplate", function() {
 	
 	return {
        require: 'ngInclude',
        restrict: 'A', /* optional */
        link: function (scope, el, attrs) {
            el.replaceWith(el.children());
        }
    };
 })
 .directive("dashboardContainer", function() {
 	
 	return {
 		templateUrl:'views/dashboard.html'
 		 		
 	}
 })
 .directive("chartsContainer", function() {
 	
 	return {
 		templateUrl:'views/charts.html'
 		
 	}
 })
 .directive("searchSummary", function() {
 	
 	return {
 		templateUrl:'views/searchsummary.html'
 		
 	}
 })
 .directive("profileSummary", function() {
 	
 	return {
 		templateUrl:'views/profilesummary.html'
 		
 	}
 })
 .directive("insightSummary", function() {
 	
 	return {
 		templateUrl:'views/insight.html'
 		
 	}
 })
 .directive("metricsSummary", function() {
 	
 	return {
 		templateUrl:'views/metrics.html'
 		
 	}
 })
.directive('lvlDraggable', function($rootScope) {
        return {
            restrict: 'A',
            link: function(scope, el, attrs, controller) {
                angular.element(el).attr("draggable", "true");
 
 				
                var id = angular.element(el).attr("id");
                if (!id) {
                    id = Math.floor((Math.random()*10000)+1);
                    angular.element(el).attr("id", id);
                }
                console.log(id);
                
                el.bind("dragstart", function(e) {
                    e.dataTransfer.setData('text', id);
                    $rootScope.$emit("LVL-DRAG-START");
                });
                 
                el.bind("dragend", function(e) {
                    $rootScope.$emit("LVL-DRAG-END");
                });
            }
        }
   })
.directive('lvlDropTarget',  function($rootScope) {
        return {
            restrict: 'A',
            scope: {
                onDrop: '&'
            },
            link: function(scope, el, attrs, controller) {
            	
                var id = angular.element(el).attr("id");
                if (!id) {
                    id = Math.floor((Math.random()*10000)+1);
                    angular.element(el).attr("id", id);
                }
                          
                el.bind("dragover", function(e) {
                    if (e.preventDefault) {
                      e.preventDefault(); // Necessary. Allows us to drop.
                  }
                   
                  if(e.stopPropagation) { 
                    e.stopPropagation(); 
                  }
 
                  e.dataTransfer.dropEffect = 'move';
                  return false;
                });
                 
                el.bind("dragenter", function(e) {
                  angular.element(e.target).addClass('lvl-over');
                });
                 
                el.bind("dragleave", function(e) {
                  angular.element(e.target).removeClass('lvl-over');  // this / e.target is previous target element.
                });
 
                el.bind("drop", function(e) {
                  if (e.preventDefault) {
                    e.preventDefault(); // Necessary. Allows us to drop.
                  }
 
                  if (e.stopPropogation) {
                    e.stopPropogation(); // Necessary. Allows us to drop.
                  }
 
                  var data = e.dataTransfer.getData("text");
                  var dest = document.getElementById(id);
                  var src = document.getElementById(data);
                     
                  scope.onDrop({dragEl: src, dropEl: dest});
                });
 
                $rootScope.$on("LVL-DRAG-START", function() {
                  var el = document.getElementById(id);
                  angular.element(el).addClass("lvl-target");
                });
                 
                $rootScope.$on("LVL-DRAG-END", function() {
                  var el = document.getElementById(id);
                  angular.element(el).removeClass("lvl-target");
                  angular.element(el).removeClass("lvl-over");
                });
            }
        }
    })
 .directive('draggable',function() {
 	
 	
 	return {
 		restrict: 'A',
 		link: function(scope,elem,attr) {
 			elem.draggable({containment: 'parent', stop:dragStop});
	 			function dragStop(event, ui){
	    			convert_to_percentage($(this));
				}
				function convert_to_percentage(el){
				    var parent = el.parent();
				    el.css({
				        left:parseInt(el.css('left'))/parent.width()*100+"%",
				        top: parseInt(el.css('top'))/parent.height()*100+"%",
				        width: el.width()/parent.width()*100+"%",
				        height: el.height()/parent.height()*100+"%"
				    });
 				}
		}
 	}
 })
  .directive('resizable',function() {
 	
 	
 	return {
 		restrict: 'A',
 		link: function(scope,elem,attr) {
 			elem.resizable({containment: 'parent', handles: "se", create: setContainerResizer, stop:resizeStop});
 			function resizeStop(event, ui){
    			convert_to_percentage($(this));
			}
			function setContainerResizer(event, ui) {
			    console.log($(this)[0]);
			    $($(this)[0]).children('.ui-resizable-handle').mouseover(setContainerSize);
			    $($(this)[0]).children('.ui-resizable-handle').mouseout(resetContainerSize);
			}
			function convert_to_percentage(el){
			    var parent = el.parent();
			    el.css({
			        left:parseInt(el.css('left'))/parent.width()*100+"%",
			        top: parseInt(el.css('top'))/parent.height()*100+"%",
			        width: el.width()/parent.width()*100+"%",
			        height: el.height()/parent.height()*100+"%"
			    });
			}

			function setContainerSize(el) {
			    var parent = $(el.target).parent().parent();
			    parent.css('height', parent.height() + "px");
			}

			function resetContainerSize(el) {
			    parent.css('height', 'auto');
			}
 		}
 	}
 })
.directive('sortable',function() {
 	
 	
 	return {
 		restrict: 'A',
 		link: function(scope,elem,attr) {
 	
 			elem.sortable();
 			elem.disableSelection();
 		}
 	}
 })
 .directive('scrollable',function($timeout) {
 	
 	
 	return {
 		restrict: 'A',
 		link: function(scope,elem,attr) {
 	
 			elem.slimScroll({distance: '0px',
 							size: '3px'
 							//height: '300px'
 					});
 							
 		
 		}
 	}
 })
.directive('showSlide', function() {
   return {
     //restrict it's use to attribute only.
     restrict: 'A',

     //set up the directive.
     link: function(scope, elem, attr) {

        //get the field to watch from the directive attribute.
        var watchField = attr.showSlide;

        //set up the watch to toggle the element.
        scope.$watch(attr.showSlide, function(v) {
           if(v && !elem.is(':visible')) {
              elem.slideDown();
           }else {
              elem.slideUp();
           }
        });
     }
   }
})
 .directive("backtop", function() {
 	
 	
 		return {
 			link: function() {
 					console.log('backtop');
	 				jQuery(document).ready(function() {
	 					console.log('backtop');
				    var offset = 220;
				    var duration = 500;
				    jQuery.event.props.push('dataTransfer');
				    jQuery(window).scroll(function() {
				        if (jQuery(this).scrollTop() > offset) {
				            jQuery('.back-to-top').fadeIn(duration);
				        } else {
				            jQuery('.back-to-top').fadeOut(duration);
				        }
				    });
				    
				    jQuery('.back-to-top').click(function(event) {
				        event.preventDefault();
				        jQuery('html, body').animate({scrollTop: 0}, duration);
				        return false;
				    })
				    
				    
				}); 
 			}
 		}
 		
 	
 })
.directive("metricschart", function($timeout) {
        return {
                                restrict: 'A',
                                require: '?ngModel',
                                scope: {
                                        val: '='
                                },
                                link: function (scope, element, attrs) {
									var options = {
	                                                type:'line',
	                                                width:'40%',
	                                                height:'20px',
	                                                chartRangeMin:0
	
	                                }
	
									 var init = function() { 
	                                                                             //scope.options = angular.extend(options, scope.options);
	                                        //console.log(element);
	                                        $(element).sparkline(scope.val,options);
	                                        /*
	                                        $(element).bind('sparklineClick', function(ev) {
    											var sparkline = ev.sparklines[0],
        										region = sparkline.getCurrentRegionFields();
    												console.log("Clicked on x="+region.x+" y="+region.y);
    												ev.stopPropagation();
    												
											});
	                                        */
	                               
	                               	 };
	                               	 
	                               	 init();
	                                
	                                
	                                scope.$watch('val', function(newVal, oldVal) {
	                                	
	                                		init();
	                                	//	$.sparkline_display_visible();
	                                	
	                                });
                                }
                }


})
.directive("guagechart", function($timeout) {
 	return {
				restrict: 'A',
				require: '?ngModel',
				scope: {
					percent: '=',
					options: '='
				},
				link: function (scope, element, attrs) {

					
						scope.percent = scope.percent || 0;

					/**
					 * default easy pie chart options
					 * @type {Object}
					 */
					
						var options = {
							
							lines: 12, // The number of lines to draw
							  angle: 0, // The length of each line
							  lineWidth: 0.25, // The line thickness
							  pointer: {
							    length: 0.57, // The radius of the inner circle
							    strokeWidth: 0.035, // The rotation offset
							    color: '#000000' // Fill color
							  },
							  colorStart: '#6FADCF',   // Colors
							  colorStop: '#8FC0DA',    // just experiment with them
							  strokeColor: '#E0E0E0',   // to see which ones work best for you
							  generateGradient: true
							 
							
							};
							
							scope.options = angular.extend(options, scope.options);
							var gauge = new Gauge(element[0].nextElementSibling).setOptions(options); // create  gauge!
							gauge.maxValue = 100; // set max gauge value
							gauge.animationSpeed = 32; // set animation speed (32 is default value)
							gauge.set(scope.percent); // set actual value
					
					
					
											
						

					

					scope.$watch('percent', function(newVal, oldVal) {
						gauge.set(newVal);
					});
				}
		}
 	
 })
.directive("wordCloud", function ($timeout) {
	

  	return {
		 scope: true,    	
         
	     link: function (scope, element, attr) {
          
          var init = function() {
          	
			  var fill = d3.scale.category20();
			
			  d3.layout.cloud().size([300, 300])
			      .words([
			        "kejriwal","olympics", "america", "obama", "narendra modi", "iphone", "whatsapp",
			        "blackberry", "bangalore"].map(function(d) {
			        		var fontSize = 3 + Math.random() * 40;
			        		if (fontSize < 10) fontSize = 10;
			        		return {text: d, size: fontSize};
			      }))
			      .padding(5)
			      .rotate(function() { return ~~(Math.random() * 3) * 45; })
			      .font("Impact")
			      .fontSize(function(d) { return d.size; })
			      .on("end", draw)
			      .start();
			
			  function draw(words) {
			    d3.select("#wordcloud").append("svg")
			        .attr("width", 300)
			        .attr("height", 300)
			      .append("g")
			        .attr("transform", "translate(150,150)")
			      .selectAll("text")
			        .data(words)
			      .enter().append("text")
			        .style("font-size", function(d) { return d.size + "px"; })
			        .style("font-family", "Impact")
			        .style("fill", function(d, i) { return fill(i); })
			        .attr("text-anchor", "middle")
			        .attr("transform", function(d) {
			          return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
			        })
			        .text(function(d) { return d.text; });
			  }
          	
          }
         
          $timeout(function() {init()},100);
        }
  	}
})
.directive("modalWindow", function () {
	var baseId = 'md'+ new Date().getTime() + Math.floor((Math.random()*10000)+1);

  	return {
	     restrict: 'E',
	     replace: false,
         scope:{
             trigger:'=',
            items:'@',
            obj:'@',
            getresult:"&",
            positiveclick:"&",
            negativeclick:"&",
            positivelabel:'@',
            negativelabel:'@'
            
         },
         templateUrl:'/views/modal.html',
	     link: function (scope, element, attr) {
          
          // create a unique id. Needed for various elements 
            
            
                       
            // update elements with unqiue Ids		
            
			//element.attr('id',baseId);
			element.children('div.modal').attr('id',baseId);
			//element.attr('aria-labelledby',baseId +'Label');
			element.children('div.modal').attr('aria-labelledby',baseId +'Label');
			element.children('div.modal-header > h3').attr('id',baseId +'Label');
			var modalElem = $('#' + baseId);
			//modalElem = $('#myModal');
			 scope.handlepositiveclick = function(){
              // disable all the buttons in the dialog  
              modalElem.find('button').addClass('disabled');
              // call the positive click function  
              scope.positiveclick();            
              // hide the dialog  
              modalElem.modal('hide');  
            };
            
            // function called when negative buttons are clicked
            scope.handlenegativeclick = function(){
              // disable all the buttons in the dialog  
              modalElem.find('button').addClass('disabled');
              // call the negative click function  
              scope.negativeclick();
              // hide the dialog  
              modalElem.modal('hide');
            };
          scope.$watch('trigger',function(newValue, oldValue){               
                
                if (newValue)
                {
                    // enable any disabled buttons 
                    //$element.find('button').removeClass('disabled');
                    // show the dialog
                   // element.modal('show');
                   modalElem.find('button').removeClass('disabled');
                   var ctrl = modalElem.controller();
                   var result = scope.getresult(scope.obj);
                   ctrl.setModel(result);
                   
                   modalElem.modal('show');
                   $('.clickcursor').tooltip();
                   
                }
                else
                {
                    // hide the dialog
                  //  element.modal('hide');
                     modalElem.modal('hide');
                // scope.$apply(scope.trigger = false);
                }
            });
            
            
            
            // listen to 'hidden' event and set 'trigger' to false, updating parent scope
            modalElem.on('hidden', function () {
              scope.$apply(scope.trigger = false);
            });
        }
  	}
});

