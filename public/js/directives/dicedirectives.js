angular.module('mean.system')
.directive("chartsload", function($timeout,$compile) {
 	
 	return {
 		scope:true,
 		link: function (scope, elem, attrs) {
 			
			
			//var followersChart = dc.rowChart("#dc-followers-chart");
			
				// Run the data through crossfilter and load our 'facts'
			var newScope;
			var compiledElement;
			
			
			var reinit = function() {
				dc.redrawAll();
				
			}
			
			var remove = function() {
				dc.deregisterAllCharts();
				//TODO: Remove the DOM nodes
			}
 			var init = function() {
 				
 				dc.deregisterAllCharts();
 				
 				
 				
 				
 				var sourceChart = dc.rowChart("#dc-source-chart");
 				var sentimentChart = dc.pieChart('#dc-sentiment-chart');
 				var tableChart = dc.dataTable("#dc-data-table")
				var postsChart = dc.barChart("#dc-posts-chart");
				var postsChartSmall = dc.barChart("#dc-posts-chart-small");
				var datacount = dc.dataCount("#datacount");
				
				

 				//negative,neutral,positive
 				//var colorPallete=["#FB8072","#7282fb","#B3DE69","#CCEBC5","#FCCDE5","#8DD3C7","#BEBADA","#FB8072","#80B1D3","#FDB462"];
 				//var dataTable = dc.dataTable("#dc-table-graph");
 				var colorScale = d3.scale.ordinal().domain(["negative", "neutral", "positive"])
 													.range(["#FB8072", "#7282fb", "#B3DE69"]);
 			    
 				
 				
			//	var postsChartSmall = dc.barChart("#dc-posts-chart-small");

			
				
				
				
				
				// Create dataTable dimension
				var timeDimension = scope.facts.dimension(function (d) {
					return d.dtg;
				});
					
				
				var sentDimension = scope.facts.dimension(function (d) {
					return d.sentiment;
				});
				var sourceDimension = scope.facts.dimension(function (d) {
					return d.category;
				});
				
				
				
				var sentvalue = sentDimension.group();
				var sourceValue = sourceDimension.group();
				
				
				var postsValue = timeDimension.group().reduceSum(function(d) {return 1;});
				var minDate = timeDimension.bottom(1)[0].dtg;
				var maxDate = timeDimension.top(1)[0].dtg;
				var maxDateDisplay = d3.time.day.offset(maxDate,2);
				var maxDateFilter =  d3.time.day.offset(maxDate,-5);
				var minDateDisplay = d3.time.day.offset(minDate, -1);
				
				datacount.dimension(scope.facts)
				.group(scope.facts.groupAll());						
				
			

				sourceChart.width(180)
			            .height(150)
			            .dimension(sourceDimension)
			            .group(sourceValue)
			            .renderLabel(true)
			            .elasticX(true)
			           
			            // .colors(["#8DD3C7","#BEBADA"])
			       		.xAxis().ticks(4);
			            
			        
			            
							
				// Render the Charts
				
				sentimentChart.width(180)
					.height(150)
					.radius(75)
					.innerRadius(15)
					.dimension(sentDimension)
					.group(sentvalue)
					//.colors(d3.scale.category20b());
					//.colors(d3.scale.ordinal().range(colorPallete))
					.colors(function(d){return colorScale(d); })
					.title(function(d){return d.value;});
				
			/*
				followersChart.width(230)
			            .height(150)
			            .dimension(followersDimension)
			            .group(followersValue)
			            .renderLabel(true)
			            .elasticX(true)
			         	.xAxis().ticks(4); */					
					//.colors(d3.scale.ordinal().range(colorPallete))
					
					//.linearColors(["#a60000","#ff0000", "#ff4040","#ff7373"]);
					//.colors(["#a60000","#ff0000", "#ff4040","#ff7373","#67e667","#39e639","#00cc00"])
					//.colorDomain([0, 5]);
					
					//.colors(["#FB8072","#80B1D3","#FDB462","#B3DE69","#FCCDE5","#CCEBC5","#FFED6F"]);
					//.colors(["#a60000","#ff0000", "#ff4040","#ff7373","#67e667","#39e639","#00cc00"])
					
				  
			            
					
					postsChartSmall.width(400)
					.height(50)
					.margins({top: 10, right: 10, bottom: 20, left: 40})
					.dimension(timeDimension)
					.group(postsValue)
					.centerBar(true)
					.gap(1)
					.x(d3.time.scale().domain([minDateDisplay,maxDateDisplay])) 
					.renderlet(
		             function(chart) {
		             		if( !(chart.select("svg").attr("width") == "100%")) {
		                    	chart.select("svg").attr("width","100%").attr("height","50px").attr("viewBox",
		                            "0 0 " + 400 + " " + 50).attr("preserveAspectRatio", "xMaxYMax");
		                    }
		             })
				//	.filter([minDate, maxDate])
				//	.round(d3.time.day.round)
					.round(d3.time.day.month)
      				.alwaysUseRounding(true)
      				.xUnits(d3.time.months);
      				//.xUnits(d3.time.days);
					
					//postsChartSmall.xAxis().ticks(6);
					//postsChartSmall.xAxis().tickFormat(function (x) {
       					// return x.getMonth()+1;
    				//});
    				postsChartSmall.xAxis().ticks(6)
    					.tickFormat(d3.time.format("%d-%b"));	
					
			
			  
		        tableChart.dimension(timeDimension)
		        // data table does not use crossfilter group but rather a closure
		        // as a grouping function
		        .group(function (d) {
		        	if(d.category == 'RSS')
		            	return 'z' + d.category;
		            else return d.category;
		        })
		        .size(100) // (optional) max number of records to be shown, :default = 25
		       // .size(7) // (optional) max number of records to be shown, :default = 25
		        // dynamic columns creation using an array of closures
		        .columns([
		        	function (d) {
		        		 
		                //return	"<span class='glyphicon glyphicon-eye-open' ng-click='expand(" + d.id + ")' ></span>";
		                return "<span class='glyphicon glyphicon-eye-open' id='" + d.id +"'></span>";
		                		
		                		
		               
		                
		                
		            },
		            function (d) {
		                return	d.date; 
		                
		            },
		            function (d) {
		                return		                "<img src='../img/icons/" + d.category + ".ico'/>";
		            },
		            function (d) {
		                return d.title;
		            },
		            function (d) {
		            		            	
		            	//if(d.influence_count > scope.medianNumber){ scope.highInfluenceCount +=1;return "High";}
		            	//else { scope.lowInfluenceCount+=1;return "Low"};
		            	return d.influence_rank;
		            	
		            },
		            function (d) {
		            	/*
		            	var color;
		            	if(d.sentiment =='negative')
		            		color= "#FB8072";
		            	else if(d.sentiment == 'positive')
		            		color = "#B3DE69";
		            	else if(d.sentiment == 'neutral')
		            		color = "#7282fb";
		            	else
		            		color="grey";
		            	*/	
		            		
		                return "<svg width='10' height='10'> <circle cx='5' cy='5' r='5' fill='"+ d.color +"' /></svg>";
		            }
		            
		            
		        ])
		        // (optional) sort using the given field, :default = function(d){return d;}
		       
		        .sortBy(function (d) {
		           return d.dtg;
		        })
		        .order(d3.ascending)
		       	
		        // (optional) sort order, :default ascending
		        
		        // (optional) custom renderlet to post-process chart using D3
		        .renderlet(function (table) {
		        	
		         // table.selectAll("#dc-data-table").classed("info", true);
		         
		           $('#dc-data-table > tbody  > tr').each(function() {
		           	var id = $(this).children(':first').children(':first').attr("id");
					$(this).attr("ng-click","expand(" + id + ")");
		           });
		            if(newScope) {
	 					newScope.$destroy(); 
	 					newScope = null; 
	 					compiledElement.off();
	 					compiledElement.remove();
 					}
 				
 					newScope = scope.$new();
		            
		           	var dataTableElem = $('#dc-data-table > tbody');//JQuery request for the app pane element.
					compiledElement=$compile(dataTableElem.contents())(newScope);
					dataTableElem = null;
				 						           
		        });	
		        
		        
		        
		        postsChart.width(400)
					.height(150)
					.margins({top: 10, right: 10, bottom: 20, left: 40})
					.dimension(timeDimension)
					.group(postsValue)
					.transitionDuration(500)
					.centerBar(true)
					.barWidth(10)
					.gap(60)
					.x(d3.time.scale().domain([minDateDisplay,maxDateDisplay]))
					.alwaysUseRounding(true)
					.round(d3.time.day.round)
					//.filter([maxDateFilter, maxDateDisplay])
					 
					.renderlet(
						
		             function(chart) {
		             	
		             	  if( !(chart.select("svg").attr("width") == "100%")) {
		             	  		
		             	  		
		             		
		                    	chart.select("svg").attr("width","100%").attr("height","150px").attr("viewBox",
		                            "0 0 " + 400 + " " + 150).attr("preserveAspectRatio", "xMaxYMax");
		                		
		                 
		                  } 
		                
		             })
		            
					//.elasticY(true)
					//.mouseZoomable(true)
					.rangeChart(postsChartSmall)
					//.xUnits(d3.time.months)
					.xUnits(d3.time.days)
					
					.yAxisLabel("Posts per day")
					//.xAxisLabel("Number of Posts")
					.yAxis().tickFormat(d3.format(".0f"));
				
				//postsChart.xAxis().ticks(6);	
				postsChart.xAxis().ticks(6)
   					.tickFormat(d3.time.format("%d-%b"));	
   					
				
				
				
				//postsChartSmall.filter([minDateDisplay,maxDateDisplay]);
				
				
				//$('#containershape').shapeshift({handle:'.dragger', minColumns:2}); 
				
				var dragOptions = { handle:'.dragger'};
				
				//$('#dc-posts-chart').draggable();
				//$('#dc-posts-chart-small').draggable();
				//$('#dc-sentiment-chart').draggable();	
				//$('#dc-source-chart').draggable();
				//$('#dc-sentiment-chart').resizable();	
				//$('#dc-source-chart').resizable();
				$('#dc-posts-chart').resizable();
				$('#dc-posts-chart-small').resizable();
				//$('#dc-data-table-wrapper').resizable();
		
 		
				//$('#dc-posts-chart-small').resizable();
				//$('#dc-posts-chart').draggable();
				
				//$('#dc-data-table-wrapper').draggable();
				//$('#dc-sentiment-chart').draggable();	
				//$('#dc-source-chart').draggable();
				/*
				$(".bigchart").on( "resizestop", function( event, ui ) {
					console.log($(this).width());
					if ($(this).width() < 600) {
						$(this).attr("data-ss-colspan",1);
						$('#containershape').shapeshift('ss-rearrage');
						console.log("trigger rearrage");
						
					}
				} );
				*/
			
			var dataTableElem = $('#dc-data-table > tbody');//JQuery request for the app pane element.
			
			if(newScope) {
	 					newScope.$destroy(); 
	 					newScope = null; 
	 					compiledElement.off();
	 					compiledElement.remove();
 			}
 				
 			newScope = scope.$new();
			
			compiledElement= $compile(dataTableElem.contents())(newScope);
			dataTableElem = null;
			
			dc.renderAll();
			postsChartSmall.filter([minDateDisplay,maxDateDisplay]);
			
			//$timeout(function() {$(".extent").click(); }, 300);
			//
				//
				//$timeout(function() {$("#dc-posts-chart" ).trigger( "click" ); }, 100);
			//$("#dc-data-table").children('tbody').slimScroll({height: '400px'});
			
			$timeout(function() {	
				//$("#dc-data-table").children('tbody').attr("id","dc-table-tbody");
				
				$("#dc-data-table-wrapper-2").slimScroll({
								height: '540px',
								distance: '0px',
								size: '3px'
								
								});
				scope.exportToCSV();
								/*
				var json = JSON.stringify(scope.items);
				var blob = new Blob([json], {type: "application/json"});
				var url  = URL.createObjectURL(blob);
				$('#exportToCSV').attr("href",url).attr("download",scope.keyword+'.csv');
				*/
				/*
				var a = document.createElement('a');
				a.download    = scope.keyword+'.csv';
				a.href        = url;
				a.textContent = "Download " + a.download;
				$('#exportToCSV').append(a);
				*/
				
				
			
				
			},100);
				
					
	 		}
	 		/*
 			scope.$watch('showChartsData', function(newValue, oldValue){
 				
 				if(newValue == true) {
 					
 					init();
 				} 
 			});
 			
 			scope.$watch('reinitChartsData', function(newValue, oldValue){
 				
 				if(newValue == true) {
 					
 					reinit();
 				} 
 			});
 			
 			*/
 			scope.$watch('showDice', function(newValue, oldValue){
 				
 				if(newValue == true) {
 					
 					$timeout(function() {init()});
 				} else {
               	    $timeout(function() {remove()});
                }
 			});
 			scope.$watch('showChartsData', function(newValue, oldValue){
 				
 				if((newValue == true) && (scope.showDice == true))$timeout(function() {init()});
 			});
 			
 		}
 		
 	}
 });